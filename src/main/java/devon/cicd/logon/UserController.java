package devon.cicd.logon;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/user/")
public class UserController {
	@Autowired
	UserManager userManager;
	
    @GetMapping(value = "profile", produces = "application/json")
    public Object getUserProfile() throws Exception {
    	return userManager.getUserProfile();
    }

}