package devon.cicd.logon;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devon.cicd.ctx.ThreadContext;
import devon.cicd.factory.InterfaceFactory;
import devon.cicd.resource.SourceRepository;

@Service
public class UserManager {
	@Autowired
	InterfaceFactory interfaceFactory;

	public Map<String, Object> getUserProfile() throws Exception {
		SourceRepository sourceRepository = interfaceFactory.getSourceRepository();
		if(sourceRepository == null) {
			throw new RuntimeException("SourceRepository type do not exist!");
		}
		return sourceRepository.getUserByUserName(ThreadContext.getUserContext().getUserName());
	}
}
