package devon.cicd.logon;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devon.cicd.config.CicdConfig;
import devon.cicd.config.CicdConfigManager;
import devon.cicd.config.SourceRepoConfig;
import devon.cicd.ctx.CicdContext;
import devon.cicd.ctx.ThreadContext;
import devon.cicd.ctx.UserContext;
import devon.cicd.data.LogonUser;
import devon.cicd.factory.InterfaceFactory;

@Service
public class LogonManager {
	@Autowired
	InterfaceFactory interfaceFactory;
	@Autowired
	CicdConfigManager cicdConfigManager;

	public Map<String, Object> httpLogin(Map<String, Object> data) throws Exception {
		CicdConfig config = cicdConfigManager.readAuthConfigById((String)data.get("authId"), "ALL");

		UserContext userContext = ThreadContext.getUserContext();
		@SuppressWarnings("unchecked")
		Map<String, Object> session = (Map<String, Object>)userContext.getSolutionHttpSession(config.getSolutionCode());
		if(session != null) {
			return session;
		}
		HttpLogon logon = interfaceFactory.getHttpLogon(config);
		if(logon == null) {
			throw new RuntimeException("HttpLogon do not exist!");
		}
		
		session = logon.login();
		userContext.addSolutionHttpSession(config.getSolutionCode(), session);
		ThreadContext.setUserContext(userContext);
		
		return session;
	}
	
	public Map<String, Object> login(Map<String, Object> data) throws Exception {
		SourceRepoConfig repoConfig = CicdContext.instance().getSourceRepoConfig((String)data.get("repositoryId"));
		if(repoConfig == null) {
			throw new RuntimeException("The configuration information for the selected source repository does not exist.");
		}
		Logon logon = interfaceFactory.getLogon(repoConfig.getSolutionCode());
		
		if(logon == null) {
			throw new RuntimeException("Logon type do not exist!");
		}
		
		Map<String, Object> returnMap = logon.login(data);
		
		UserContext userContext = new UserContext();
		userContext.setUserName((String)data.get("userId"));
		userContext.setPassword((String)data.get("password"));
		userContext.setUserPersonalAccessToken((String)returnMap.get("personalToken"));
		userContext.setSourceRepositoryId((String)data.get("repositoryId"));
		userContext.setAthority(logon.getAuthority((String)data.get("userId")));
		userContext.setId(((LogonUser)(returnMap.get("logonUser"))).getId());
		ThreadContext.setUserContext(userContext);
		
		returnMap.put("solutionCode", repoConfig.getSolutionCode());
		returnMap.put("repositoryId", data.get("repositoryId"));
		return returnMap;
	}
	
    public Object tokenInfo(String token) throws Exception {
    	SourceRepoConfig repoConfig = CicdContext.instance().getSourceRepoConfig(ThreadContext.getUserContext().getSourceRepositoryId());
    	Logon logon = interfaceFactory.getLogon(repoConfig.getSolutionCode());
    	return logon.tokenInfo(token); 
    }
}
