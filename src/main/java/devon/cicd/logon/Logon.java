package devon.cicd.logon;

import java.util.Map;

public interface Logon {
	public Map<String, Object> login(Map<String, Object> input) throws Exception;
    public Map<String, Object> tokenInfo(String token) throws Exception;
    public String getAuthority(String userId) throws Exception;
}
