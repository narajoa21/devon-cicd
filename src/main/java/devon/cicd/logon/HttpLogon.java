package devon.cicd.logon;

import java.util.Map;

public interface HttpLogon {
	public Map<String, Object> login() throws Exception;
}
