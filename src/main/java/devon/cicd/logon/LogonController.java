package devon.cicd.logon;

import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/auth/")
public class LogonController {
	@Autowired
	LogonManager logonManager;
	
    @PostMapping(value = "login", produces = "application/json")
    public Object login(@RequestBody Map<String, Object> body) throws Exception {
    	return logonManager.login(body);
    }

    @RequestMapping(value = "/token_info", method = RequestMethod.GET, produces = "application/json")
    public Object tokenInfo(@RequestParam("token") String token) throws Exception {
    	return logonManager.tokenInfo(token); 
    }

    @PostMapping(value = "httpLogin", produces = "application/json")
    public Object httpLogin(@RequestBody Map<String, Object> body) throws Exception {
    	return logonManager.httpLogin(body);
    }   
}