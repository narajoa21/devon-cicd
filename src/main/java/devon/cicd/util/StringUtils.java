package devon.cicd.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.codec.binary.Base32;

import devon.cicd.config.Constants;


public class StringUtils {
	static public String getIndent(int depth){
		StringBuilder builder = new StringBuilder();
		for(int i=0; i < depth; i++){
			builder.append('\t');
		}
		return builder.toString();
	}
	
	static public void toLower(List<String> list){
		if(list == null || list.size() == 0){
			return;
		}
		
		for(int i=0; i < list.size(); i++){
			list.set(i, list.get(i).toLowerCase());
		}		
	}
	
	static public String encodeBase32Bytes(byte [] data) {
		if(data == null) {
			return null;
		}
		Base32 base = new Base32();
		return base.encodeToString(data);		
	}
	
	static public String encodeBase32(String message) throws UnsupportedEncodingException {
		if(message == null) {
			return null;
		}
		return encodeBase32Bytes(message.getBytes(Constants.CHARSET));
	}

	static public byte [] decodeBase32Bytes(String message) {
		if(message == null) {
			return null;
		}

		Base32 base = new Base32();
		return base.decode(message);
	}
	
	static public String decodeBase32(String message) throws UnsupportedEncodingException {
		if(message == null) {
			return null;
		}
		return new String(decodeBase32Bytes(message), Constants.CHARSET);
	}
	
	static public String getDate(int days) {
		Calendar cal = Calendar.getInstance();
		cal.set (cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE) + days);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return  sdf.format (cal.getTime());
	}
	
	static public boolean isContains(String source, List<String> searchs) {
		for(String search : searchs) {
			if(source.indexOf(search) >= 0) {
				return true;
			}
		}
		return false;
	}
	
	static public String getFullStackMessage(Exception ex) {
		if (ex==null){
			return null;
		}
		StringWriter message = null;
		PrintWriter writer = null;
		try
		{
			message = new StringWriter();
			writer = new PrintWriter(message);
			ex.printStackTrace(writer);
			return message.getBuffer().toString();
		}catch(Exception e) {
		}finally{
			if(message != null) { try { message.close(); }catch(Exception e) {}}
			if(writer != null) { try { writer.close(); }catch(Exception e) {}}
		}
		return null;
	}
	
	static public String getValueFromHTML(String html, String start, String end, int index) {
		if(html == null || start == null || end == null) {
			return null;
		}
		
		if("{all}".equals(start)) {
			return html.trim();
		}
		
		int currentInx = 0;
		int currentPos = 0;
		int endPos;
		int size;
		while(currentInx <= index) {
			currentPos = html.indexOf(start, currentPos);
			if(currentPos == -1) {
				return null;
			}
			size = start.length();
			if(currentPos < index) {
				currentPos+= size;
				continue;
			}
			
			if((endPos = html.indexOf(end, currentPos + size)) == -1){
				return null;
			}
			
			return html.substring(currentPos + size, endPos);
		}
		return null;
	}
	
	static public List<String> parseString(String line, String token){
		List<String> list = new ArrayList<String>();
		int size = token.length();
		
		int end=0;
		int start = line.indexOf(token);
		if(start == -1) {
			list.add(line.trim());
			return list;
		}
		if(start >= 0) {
			list.add(line.substring(0, start).trim());			
		}
		start = start + size;
		while(start >=0) {
			end = line.indexOf(token, start);
			if(end == -1) {
				list.add(line.substring(start).trim());
				return list;
			}
			list.add(line.substring(start, end).trim());
			start = end + size;
		}
		
		return list;
	}
	
	static public String replace(String str, String source, String target) {
		if(str == null || source == null || target == null) {
			return str;
		}
		
		int start = str.indexOf(source);
		if(start == -1) {
			return str;
		}
		
		int size = str.length();
		StringBuilder buffer = new StringBuilder(str.length());
		if(start > 0) {
			buffer.append(str.substring(0, start));
		}
		buffer.append(target);
		if((size - (start + source.length())) > 0) {
			buffer.append(str.substring(start + source.length()));
		}
		return buffer.toString();
	}

	static public String replace(String str, char source, char target) {
		if(str == null || source == 0x0 || target == 0x0) {
			return str;
		}

		StringBuffer buffer = new StringBuffer(str.length());
		int size = str.length();
		char ch;
		for(int i = 0; i < size; i++) {
			ch = str.charAt(i);
			if(ch == source) {
				buffer.append(target);
			}else{
				buffer.append(ch);
			}
		}
		return buffer.toString();
	}
	
	static public List<String> toList(Iterator<String> itor){
		if(itor == null) {
			return null;
		}
		
		List<String> list = new ArrayList<String>();
		while(itor.hasNext()) {
			list.add(itor.next());
		}
		return list;
	}
}
