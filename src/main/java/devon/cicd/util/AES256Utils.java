package devon.cicd.util;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import devon.cicd.config.Constants;

public class AES256Utils {
	private Key keySpec;
	final static byte [] keyBytes = {1,23,111,121,34,112,3,56,53,73,91,123,105,123,34,55};
	
	static public void setKey(String key) {
		if(key == null) {
			throw new RuntimeException("key is invalid!");
		}
		String [] keys = key.split(",");
		if(keys.length != 16) {
			throw new RuntimeException("key is invalid!");			
		}

		for(int index = 0; index < keys.length; index++) {
			keyBytes[index] = (byte)(Integer.parseInt(keys[index].trim()));
		}
	}
	
	public AES256Utils() throws UnsupportedEncodingException {
		this.keySpec = (Key)new SecretKeySpec(keyBytes, "AES");
	}

	public byte [] encrypt(byte [] data) throws NoSuchAlgorithmException, GeneralSecurityException, UnsupportedEncodingException {
		Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		c.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(keyBytes));
		return c.doFinal(data);
	}

	public byte [] decrypt(byte [] data) throws NoSuchAlgorithmException, GeneralSecurityException, UnsupportedEncodingException {
		Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		c.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(keyBytes));
		return c.doFinal(data);
	}
	
	static public String decodePassword(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException, GeneralSecurityException {
		if(password == null) {
			return null;
		}
		
		byte [] data = StringUtils.decodeBase32Bytes(password);
		AES256Utils aes = new AES256Utils();
		data = aes.decrypt(data);
		return new String(data, Constants.CHARSET);		
	}
	
	static public String checkEncryptedText(String text) throws Exception {
		if(text != null && text.startsWith("encrypted:")) {
			String decryptedText = text.substring(10);
			decryptedText = AES256Utils.decodePassword(decryptedText);
			return decryptedText;
		}else {
			return text;
		}
	}
	
	static public void main(String [] args) {
		try {
			if(args == null || args.length != 1) {
				System.out.println("java AES256Utils [text]");
				return;
			}
			byte [] data = args[0].getBytes(Constants.CHARSET);
			AES256Utils aes = new AES256Utils();
			data = aes.encrypt(data);
			String encrypted = StringUtils.encodeBase32Bytes(data);
			System.out.println(args[0] + "=>" + encrypted);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}