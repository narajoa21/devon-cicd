package devon.cicd.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	static public String getYYYYMMDDHHMMSS(long time) {
		return getYYYYMMDDHHMMSS(new Date(time));
	}

	static public String getYYYYMMDDHHMMSS(Date time) {
		if(time == null) {
			return null;
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(time);
	}

	static public String getYYYYMMDD(long time) {
		return getYYYYMMDD(new Date(time));
	}
	
	static public String getYYYYMMDD(Date time) {
		if(time == null) {
			return null;
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(time);
	}
	
	static public String getISO8601Format(Date time) {
		if(time == null) {
			return null;
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		return format.format(time);
	}
	
	static public String arangeGitlabDate(String date) {
		if(date == null) {
			return null;
		}
		String arrangeDate = date.trim();
		arrangeDate = arrangeDate.replace('T', ' ');
		int index;
		if((index = arrangeDate.indexOf('.')) >= 0) {
			arrangeDate = arrangeDate.substring(0, index);
		}
		return arrangeDate;
	}
	
	static public String getHHMM(long time) {
		return getHHMM(new Date(time));
	}

	static public String getHHMM(Date time) {
		if(time == null) {
			return null;
		}
		
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		return format.format(time);
	}
}
