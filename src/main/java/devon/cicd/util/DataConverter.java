package devon.cicd.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import devon.cicd.config.Constants;

public class DataConverter {
	static public String convertSameSolutionCode(String solutionCode) {
		if(Constants.RESOURCE_GITLAB_CI.equals(solutionCode) || Constants.RESOURCE_GITLAB_RUNNER.equals(solutionCode) || Constants.RESOURCE_GITLAB_AUTH.equals(solutionCode)){
			return Constants.RESOURCE_GITLAB_AUTH;
		}
		return solutionCode;
	}

	static public Map<String, Object> convertListToMap(List<Map<String, Object>> list, String key, String value){
		Map<String, Object> configs = new HashMap<String, Object>(list.size());
		for(Map<String, Object> map : list) {
			configs.put((String)map.get(key), map.get(value));
		}
		return configs;
	}
	
	static public Map<String, Object> makeApiMapClone(Map<String, Object> input, String solutionCode, String apiSolutionCode){
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		returnMap.put("solutionCode", solutionCode);
		returnMap.put("apiSolutionCode", apiSolutionCode);
		returnMap.put("stageCode", input.get("stageCode"));
		returnMap.put("region", input.get("region"));
		
		return returnMap;
	}	
}
