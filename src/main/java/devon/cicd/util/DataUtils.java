package devon.cicd.util;

import devon.cicd.data.KeyValue;

import java.util.HashMap;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class DataUtils {
	static public List<Map<String, Object>> list(){
		return new ArrayList<Map<String, Object>>();
	}
	static public List<Map<String, Object>> list(int size){
		return new ArrayList<Map<String, Object>>(size);		
	}
	static public Map<String, Object> map(){
		return new HashMap<String, Object>();
	}
	static public Map<String, Object> map(int size){
		return new HashMap<String, Object>(size);		
	}
	
	static public List<KeyValue> makeDays(long lastTime, String dateFormat, int days){
		List<KeyValue> dates = new ArrayList<KeyValue>(days);
		SimpleDateFormat sf = new SimpleDateFormat(dateFormat);
		
		KeyValue data;
		for(long i = 0; i < days; i++) {
			data = new KeyValue();
			data.setKey(sf.format(new Date(lastTime - (i * 86400000L))));
			dates.add(data);
		}
		return dates;
	}

	static public List<KeyValue> makeDaysByZero(long lastTime, String dateFormat, int days){
		List<KeyValue> dates = makeDays(lastTime, dateFormat, days);
		for(KeyValue data: dates) {
			data.setValue(0);
		}
		return dates;
	}
	
	static public KeyValue makeKeyValue(String key, Object value) {
		KeyValue keyValue = new KeyValue();
		keyValue.setKey(key);
		keyValue.setValue(value);
		return keyValue;
	}
}
