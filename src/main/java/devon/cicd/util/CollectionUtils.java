package devon.cicd.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import devon.cicd.data.KeyValue;

public class CollectionUtils {
	static public List<Map<String, Object>> sort(String name, boolean isAscending, List<Map<String, Object>> inputs){
		if(inputs ==  null || inputs.size() == 0 || name == null || name.length() == 0) {
			return inputs;
		}
		
		Collections.sort(inputs, new Comparator<Map<String, Object>>(){
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				Object cmp1 = o1.get(name);
				Object cmp2 = o2.get(name);
				
				if(cmp1 == null) {
					return -1;
				}else if(cmp2 == null) {
					return 1;
				}
				
				if(cmp1 instanceof String){
					if(isAscending) {
						return ((String) cmp1).compareTo((String)cmp2);
					}
					return ((String) cmp2).compareTo((String)cmp1);
				}else if(cmp1 instanceof Integer){
					if(isAscending) {
						return (Integer)cmp1 - (Integer)cmp2;
					}
					return (Integer)cmp2 - (Integer)cmp1;
				}else if(cmp1 instanceof Long){
					if(isAscending) {
						return (int)((Long)cmp1 - (Long)cmp2);
					}
					return (int)((Long)cmp2 - (Long)cmp1);				
				}else {
					if(isAscending) {
						return cmp1.toString().compareTo(cmp2.toString());
					}
					return cmp2.toString().compareTo(cmp1.toString());
				}
			}
		});
		return inputs;
	}
	
	static public List<Map<String, Object>> filter(String name, String value, boolean isEqual, List<Map<String, Object>> inputs){
		if(inputs ==  null || inputs.size() == 0 || name == null || name.length() == 0) {
			return inputs;
		}
		
		if(!isEqual && (value == null || value.length() == 0)){
			return inputs;
		}
		
		List<Map<String, Object>> reLists = new ArrayList<Map<String, Object>>();
		Object object;
		String cmp;
		for(Map<String, Object> data : inputs) {
			object = data.get(name);
		
			if(object == null) {
				cmp = null;
			}else if(object instanceof String){
				cmp = (String)object;
			}else{
				cmp = object.toString();
			}
			
			if(cmp == null || cmp.length() == 0) {
				if(value == null || value.length() == 0) {
					reLists.add(data);
				}
				continue;
			}else if(value == null || value.length() == 0) {
				continue;
			}
			
			if(isEqual) {
				if(cmp.equals(value)) {
					reLists.add(data);
				}
			}else {
				if(cmp.indexOf(value) >= 0) {
					reLists.add(data);
				}
			}
		}
		return reLists;
	}
	
	static public Map<String, KeyValue> convertListToMapByKeyValue(List<KeyValue> list){
		if(list == null || list.size() == 0) {
			return null;
		}
		
		Map<String, KeyValue> map = new HashMap<String, KeyValue>(list.size());
		for(KeyValue keyvalue : list) {
			map.put(keyvalue.getKey(), keyvalue);
		}
		return map;
	}
}
