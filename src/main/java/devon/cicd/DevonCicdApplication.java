package devon.cicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableAutoConfiguration
@RestController
@ComponentScan
@Configuration
@EnableScheduling
@EnableAsync(proxyTargetClass = true)
@EnableCaching
public class DevonCicdApplication {
    @RequestMapping("/health")
    public String health() {
        return "OK! I am a Devon CICD";
    }

    public static ConfigurableApplicationContext applicationContext;

	public static void main(String[] args) {
		applicationContext = SpringApplication.run(DevonCicdApplication.class, args);
	}

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }	
}
