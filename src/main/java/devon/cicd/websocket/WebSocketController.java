package devon.cicd.websocket;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebSocketController {
	private static int wsId = 0;
	
    @PostMapping(value = "/sshinfo", produces = "application/json;charset=UTF-8")
    public Object setSSHInfo(@RequestBody Map<String, Object> body) throws Exception {
        int id = (++wsId);
        
        Map<String, Object> map = new HashMap<String, Object>(3);
        map.put("status", 0);
        map.put("id", id);
        map.put("encoding", "utf-8");
        return map;
    }
}