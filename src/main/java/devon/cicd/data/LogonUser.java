package devon.cicd.data;

import lombok.Data;

@Data
public class LogonUser {
    private String id;
    private String email;
    private String name;
    private String userName;
}
