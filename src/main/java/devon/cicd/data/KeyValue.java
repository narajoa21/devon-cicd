package devon.cicd.data;

import java.io.Serializable;

import lombok.Data;

@Data
public class KeyValue implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String key;
	private Object value;
	
	public KeyValue() {
		
	}
	public KeyValue(String key, Object value) {
		this.key = key;
		this.value = value;
	}
}
