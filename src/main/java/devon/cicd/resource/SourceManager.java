package devon.cicd.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.Commit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devon.cicd.config.CicdConfig;
import devon.cicd.config.CicdConfigManager;
import devon.cicd.config.Constants;
import devon.cicd.config.SourceRepoConfig;
import devon.cicd.ctx.CicdContext;
import devon.cicd.ctx.ThreadContext;
import devon.cicd.ctx.UserContext;
import devon.cicd.db.dao.ConfigWrapper;
import devon.cicd.factory.InterfaceFactory;

@Service
public class SourceManager {
	@Autowired
	InterfaceFactory interfaceFactory;
	@Autowired
	CicdConfigManager cicdConfigManager;
	@Autowired
	ConfigWrapper configWrapper;
	@Autowired
	RuntimeManager runtimeManager;
	
	private SourceRepository getSourceRepository() throws Exception{
		SourceRepository sourceRepository = interfaceFactory.getSourceRepository();
		if(sourceRepository == null) {
			throw new RuntimeException("SourceRepository type do not exist!");
		}
		return sourceRepository;
	}
	
	public List<Map<String, Object>> getProjects() throws Exception {
		return getSourceRepository().getProjects();
	}
	
	public List<Map<String, Object>> getProjectMembers() throws Exception {
		return getSourceRepository().getProjectMembers();
	}	
	
	public Map<String, Object> getProjectCurrentUser() throws Exception {
		return getSourceRepository().getProjectCurrentUser();
	}		
	
	public Map<String,List<Map<String, Object>>> postCurrentProject(Map<String, Object> input) throws Exception {
		UserContext userContext = ThreadContext.getUserContext();
		userContext.setSystemId((String)input.get("systemId"));
		userContext.setSubSystemId((String)input.get("subSystemId"));
		userContext.setSourceRepository((String)input.get("sourceRepository"));
		userContext.setSourceRepositoryUrl((String)input.get("sourceRepositoryUrl"));
		userContext.setProjectId("" + (int)input.get("projectId"));
		
		List<Map<String, Object>> cicds = configWrapper.selectCicdList(input);
		if(cicds == null) {
			return null;
		}
		
		List<Map<String, Object>> returnCicds = new ArrayList<Map<String, Object>>(cicds.size());
		Map<String, Map<String, Object>> checks = new HashMap<String, Map<String, Object>>(cicds.size());
		Map<String, Object> item;
		String authId;
		String cicdStep;
		
		CicdConfig cicdConfig = null;
		for(Map<String, Object> cicd : cicds) {
			item = checks.get((String)cicd.get("solutionCode"));
			
			authId = (String)cicd.get("authId");
			if(item == null) {
				checks.put((String)cicd.get("solutionCode"), cicd);	
				cicdStep = (String)cicd.get("cicdStep");
				if(authId == null && "S".equals(cicdStep) ) {
					SourceRepoConfig repoConfig = CicdContext.instance().getSourceRepoConfig(userContext.getSourceRepositoryId());
					if(repoConfig != null) {
						cicd.put("host", repoConfig.getConfigs().get("host"));
						cicd.put("authId", repoConfig.getRepositoryId());
					}
				}else {
					cicdConfig = cicdConfigManager.readAuthConfigById(authId, "ALL");
					if(cicdConfig != null) {
						cicd.put("host", cicdConfig.getConfigs().get("host"));
					}
				}
				returnCicds.add(cicd);
			}else {
				if(authId != null && item.get("host") == null) {
					cicdConfig = cicdConfigManager.readAuthConfigById(authId, "ALL");
					if(cicdConfig != null) {
						item.put("host", cicdConfig.getConfigs().get("host"));
					}
				}
				item.put("cicdStep",((String)item.get("cicdStep")) + ((String)cicd.get("cicdStep"))); 
			};
		}
		
		Map<String, List<Map<String, Object>>> returnMap = new HashMap<String, List<Map<String, Object>>>(2);
		List<Map<String, Object>> stages = configWrapper.selectStageList((String)input.get("subSystemId"));
		returnMap.put("cicds", returnCicds);
		returnMap.put("stages", stages);
		
		ThreadContext.setUserContext(userContext);
		if(stages != null) {
			Map<String, Object> data;
			for(Map<String, Object> stage: stages) {
				data = runtimeManager.getRegion((String)stage.get("region"));
				if(data != null) {
					stage.put("regionName", data.get("name"));
				}
			}
		}
		return returnMap;
	}
	
	public List<Map<String, Object>> getCommitsByBranch(String branchName) throws Exception {
		return getSourceRepository().getCommitsByBranch(branchName);
	}
	
	public List<Map<String, Object>> getFilesByCommit(String id) throws Exception {
		return getSourceRepository().getFilesByCommit(id);
	}

	public Map<String, Object> getFilesContents(Map<String, Object> input) throws Exception {		
		return getSourceRepository().getFilesContents(input);	
	}
	
	
	public List<Map<String, Object>> getApprovalUsers(Map<String, Object> input) throws Exception {	
		List<Map<String, Object>> reUsers = cicdConfigManager.readMergeApprovalUsers(input);
		
		Map<String, Object> user, reUser;
		for(int inx = 0; inx < reUsers.size(); inx++) {
			reUser = reUsers.get(inx);
			user = getUserByUserName((String)reUser.get("userName"));
			if(!"active".equals((String)user.get("state"))) {
				reUsers.remove(inx);
				inx--;
				continue;
			}
			reUser.put("userId", user.get("userId"));
			reUser.put("name", user.get("name"));
			reUser.put("email", user.get("email"));
		}
		return reUsers;
	}

	public Map<String, Object> getUserByUserName(String userName) throws Exception{
		CicdContext context = CicdContext.instance();
		Map<String, Object> user = context.getSourceUser(userName);
		if(user != null) {
			if((System.currentTimeMillis() - (long)user.get("createTime")) < Constants.MAX_SOURCEUSER_KEEP_TIME){
				return user;
			}
		}

		user = getSourceRepository().getUserByUserName(userName);
		if(user != null) {
			context.addSourceUser(user);
		}
		return user;
	}
	
	public List<Map<String, Object>> getMergeRequests(Map<String, Object> input) throws Exception {	
		return getSourceRepository().getMergeRequests(input);
	}
	
	public Map<String, Object> createMergeRequest(Map<String, Object> input) throws Exception {
		return getSourceRepository().createMergeRequest(input);
	}
	
	public Map<String, Object> addMergeRequestComment(Map<String, Object> input) throws Exception {
		return getSourceRepository().addMergeRequestComment(input);
	}
	
	public Map<String, Object> acceptMergeRequest(Map<String, Object> input) throws Exception {
		return getSourceRepository().acceptMergeRequest(input);
	}
	
	
	public Map<String,Object> cherryPickRequest(Map<String,Object> input) throws Exception{
		Branch branch = getSourceRepository().createBranchRequest(input);
		Commit commit = getSourceRepository().createCommitRequest(input, branch);
		input.put("sourceBranchName", branch.getName());
		return getSourceRepository().createMergeRequest(input);
	}
}