package devon.cicd.resource;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devon.cicd.factory.InterfaceFactory;

@Service
public class ImageManager {
	@Autowired
	InterfaceFactory interfaceFactory;
	
	public List<Map<String,Object>> getImages(Map<String, Object> input) throws Exception{
		ImageRepository container = interfaceFactory.getImageRepository(input);
		return container.getImages(input);
	}
}
