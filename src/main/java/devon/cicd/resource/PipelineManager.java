package devon.cicd.resource;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devon.cicd.factory.InterfaceFactory;

@Service
public class PipelineManager {
	@Autowired
	InterfaceFactory interfaceFactory;
	
	public List<Map<String, Object>> getPipelineLogs(String branchName) throws Exception {
		PipelineJob pipelineJob = interfaceFactory.getPipelineJob(null);
		if(pipelineJob == null) {
			throw new RuntimeException("PipelineJob type do not exist!");
		}
		return pipelineJob.getPipeLineLogs(branchName);
	}

	public Map<String, Object> getDefine(Map<String, Object> input) throws Exception {
		PipelineJob pipelineJob = interfaceFactory.getPipelineJob((String)input.get("regionName"));
		if(pipelineJob == null) {
			throw new RuntimeException("PipelineJob type do not exist!");
		}
		return pipelineJob.getDefine(input);
	}

	public Map<String, Object> runPipeline(Map<String, Object> input) throws Exception {
		PipelineJob pipelineJob = interfaceFactory.getPipelineJob((String)input.get("regionName"));
		if(pipelineJob == null) {
			throw new RuntimeException("PipelineJob type do not exist!");
		}
		return pipelineJob.runPipeline(input);
	}
	
	public Map<String, Object> isRunningPipeline(Map<String, Object> input) throws Exception {
		PipelineJob pipelineJob = interfaceFactory.getPipelineJob((String)input.get("regionName"));
		if(pipelineJob == null) {
			throw new RuntimeException("PipelineJob type do not exist!");
		}
		return pipelineJob.isRunningPipeline(input);
	}	
}