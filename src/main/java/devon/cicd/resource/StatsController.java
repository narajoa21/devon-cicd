package devon.cicd.resource;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stats/")
public class StatsController {
	@Autowired
	StatsManager statsManager;
    
    @PostMapping(value = "all/simpleStats", produces = "application/json;charset=UTF-8")
    public Object getSimpleStats(@RequestBody Map<String, Object> body) throws Exception {
    	return statsManager.getSimpleStats(body);
    }
    
    @PostMapping(value = "source/commit/days", produces = "application/json;charset=UTF-8")
    public Object getSourceCommitStatsByDay(@RequestBody Map<String, Object> body) throws Exception {
    	return statsManager.getSourceCommitStatsByDay(body);
    }
 
    @PostMapping(value = "source/pipeline/days", produces = "application/json;charset=UTF-8")
    public Object getSourcePipelineStatsByDay(@RequestBody Map<String, Object> body) throws Exception {
    	return statsManager.getSourcePipelineStatsByDay(body);
    }

    @PostMapping(value = "source/mergeRequest/days", produces = "application/json;charset=UTF-8")
    public Object getSourceMergeRequestStatsByDay(@RequestBody Map<String, Object> body) throws Exception {
    	return statsManager.getSourceMergeRequestStatsByDay(body);
    }    
    
    @PostMapping(value = "runtime/instances/stats", produces = "application/json;charset=UTF-8")
    public Object getInstancesStats(@RequestBody Map<String, Object> body) throws Exception {
    	return statsManager.getInstancesStats(body);
    }
    
    @PostMapping(value = "runtime/instances/usage", produces = "application/json;charset=UTF-8")
    public Object getInstancesUsage(@RequestBody Map<String, Object> body) throws Exception {
    	return statsManager.getInstancesUsage(body);
    } 
    
    @PostMapping(value = "runtime/loadbalances/stats", produces = "application/json;charset=UTF-8")
    public Object getLoadBalancesStats(@RequestBody Map<String, Object> body) throws Exception {
    	return statsManager.getLoadBalancesStats(body);
    }
    
    @PostMapping(value = "runtime/loadbalances/usage", produces = "application/json;charset=UTF-8")
    public Object getLoadBalancesUsage(@RequestBody Map<String, Object> body) throws Exception {
    	return statsManager.getLoadBalancesUsage(body);
    }      
}