package devon.cicd.resource;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devon.cicd.db.dao.ConfigWrapper;
import devon.cicd.factory.InterfaceFactory;

@Service
public class RuntimeManager {
	@Autowired
	InterfaceFactory interfaceFactory;
	@Autowired
	ConfigWrapper configWrapper;
	
	public Map<String, Object> getRegion(String region) throws Exception{
		CloudRegions cloudRegions = interfaceFactory.getCloudRegions();
		return cloudRegions.getRegion(region);
	}
	
	public List<Map<String,Object>> getClusters(Map<String, Object> input) throws Exception{
		Runtime runtime = interfaceFactory.getRuntime(input);
		return runtime.getClusters(input);
	}

	public Map<String,Object> getService(Map<String, Object> input) throws Exception{
		Runtime runtime = interfaceFactory.getRuntime(input);
		return runtime.getService(input);
	}	

	public List<Map<String,Object>> getTasks(Map<String, Object> input) throws Exception{
		Runtime runtime = interfaceFactory.getRuntime(input);
		return runtime.getTasks(input);
	}	

	public List<Map<String,Object>> getServiceEvents(Map<String, Object> input) throws Exception{
		Runtime runtime = interfaceFactory.getRuntime(input);
		return runtime.getServiceEvents(input);
	}	

	public Map<String,Object> scaleService(Map<String, Object> input) throws Exception{
		Runtime runtime = interfaceFactory.getRuntime(input);
		return runtime.scaleService(input);
	}
	
	public List<Map<String,Object>> stopTask(Map<String, Object> input) throws Exception{
		Runtime runtime = interfaceFactory.getRuntime(input);
		return runtime.stopTask(input);
	}
	
	public Map<String,Object> replaceImage(Map<String, Object> input) throws Exception{
		Runtime runtime = interfaceFactory.getRuntime(input);
		return runtime.replaceImage(input);
	}
	
	public Map<String,Object> isRunningDeployment(Map<String, Object> input) throws Exception{
		Runtime runtime = interfaceFactory.getRuntime(input);
		return runtime.isRunningDeployment(input);
	}	
}