package devon.cicd.resource;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devon.cicd.db.dao.ConfigWrapper;
import devon.cicd.factory.InterfaceFactory;

@Service
public class DeploymentManager {
	@Autowired
	InterfaceFactory interfaceFactory;
	@Autowired
	ConfigWrapper configWrapper;
	
	public List<Map<String,Object>> getDeployments(Map<String, Object> input) throws Exception{
		DeploymentJob deploymentJob = interfaceFactory.getDeployment(input);
		return deploymentJob.getDeployments(input);
	}	

	public Map<String,Object> cancelDeployment(Map<String, Object> input) throws Exception{
		DeploymentJob deploymentJob = interfaceFactory.getDeployment(input);
		return deploymentJob.cancelDeployment(input);
	}	

	public Map<String,Object> changeService(Map<String, Object> input) throws Exception{
		DeploymentJob deploymentJob = interfaceFactory.getDeployment(input);
		return deploymentJob.changeService(input);
	}
}
