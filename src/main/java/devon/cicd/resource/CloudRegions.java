package devon.cicd.resource;

import java.util.List;
import java.util.Map;

public interface CloudRegions {
	public List<Map<String, Object>> getRegions();
	public Map<String, Object> getRegion(String region);
}
