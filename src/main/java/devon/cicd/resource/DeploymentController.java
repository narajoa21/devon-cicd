package devon.cicd.resource;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/deployment/")
public class DeploymentController {
	@Autowired
	DeploymentManager deployManager;

    @PostMapping(value = "deployments", produces = "application/json;charset=UTF-8")
    public Object getDeployments(@RequestBody Map<String, Object> body) throws Exception {
    	return deployManager.getDeployments(body);
    }
    
    @PutMapping(value = "cancelDeployment", produces = "application/json;charset=UTF-8")
    public Object cancelDeployment(@RequestBody Map<String, Object> body) throws Exception {
    	return deployManager.cancelDeployment(body);
    }

    @PutMapping(value = "changeService", produces = "application/json;charset=UTF-8")
    public Object changeService(@RequestBody Map<String, Object> body) throws Exception {
    	return deployManager.changeService(body);
    }    
}