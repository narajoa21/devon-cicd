package devon.cicd.resource;

import java.util.List;
import java.util.Map;

public interface DeploymentJob {
	public List<Map<String, Object>> getDeployments(Map<String, Object> input) throws Exception;
	public Map<String, Object> cancelDeployment(Map<String, Object> input) throws Exception;
	public Map<String, Object> changeService(Map<String, Object> input) throws Exception;
	public String getRunningDeploymentId(Map<String, Object> input) throws Exception;
	public Map<String, Object> getDeployment(Map<String, Object> input) throws Exception;
	public Map<String, Object> createDeployment(Map<String, Object> input) throws Exception;
}
