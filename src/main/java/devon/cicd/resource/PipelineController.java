package devon.cicd.resource;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pipeline/")
public class PipelineController {
	@Autowired
	PipelineManager pipelineManager;
	
    @GetMapping(value = "/log/{branchName}", produces = "application/json;charset=UTF-8")
    public Object getPipelineLogs( @PathVariable("branchName") String branchName) throws Exception {
    	return pipelineManager.getPipelineLogs(branchName);
    }
    
    @PostMapping(value = "define", produces = "application/json;charset=UTF-8")
    public Object getDefine(@RequestBody Map<String, Object> body)throws Exception {
        return pipelineManager.getDefine(body);
    }
    @PostMapping(value = "run", produces = "application/json;charset=UTF-8")
    public Object runPipeline(@RequestBody Map<String, Object> body)throws Exception {
        return pipelineManager.runPipeline(body);
    }
    
    @PostMapping(value = "isRunning", produces = "application/json;charset=UTF-8")
    public Object isRunningPipeline(@RequestBody Map<String, Object> body)throws Exception {
        return pipelineManager.isRunningPipeline(body);
    }          
}