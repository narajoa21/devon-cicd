package devon.cicd.resource;

public class CachedResourceApi {
	private long lastAccessTime;
	private Object api;
	
	public long getLastAccessTime() {
		return lastAccessTime;
	}
	public void setLastAccessTime(long lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}
	public Object getApi() {
		return api;
	}
	public void setApi(Object api) {
		this.api = api;
	}
}
