package devon.cicd.resource;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/container/")
public class ImageController {
	@Autowired
	ImageManager containerManager;
	
    @PostMapping(value = "images", produces = "application/json;charset=UTF-8")
    public Object getImages(@RequestBody Map<String, Object> body) throws Exception {
    	return containerManager.getImages(body);
    }
}