package devon.cicd.resource;

import java.util.List;
import java.util.Map;

import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.Commit;

import devon.cicd.data.KeyValue;

public interface SourceRepository {
	public Map<String, Object> getUserByUserName(String userName)  throws Exception;
	public List<Map<String, Object>> getProjects() throws Exception;
	public List<Map<String, Object>> getProjectMembers() throws Exception;
	public Map<String, Object> getProjectCurrentUser() throws Exception;
	public List<Map<String, Object>> getCommitsByBranch(String branchName) throws Exception;
	public List<KeyValue> getCommitsStatsByBranch(String branchName) throws Exception;
	public List<Map<String, Object>> getFilesByCommit(String id) throws Exception;
	public Map<String, Object> getFilesContents(Map<String, Object> input) throws Exception;
	public List<Map<String, Object>> getMergeRequests(Map<String, Object> input) throws Exception;
	public List<KeyValue> getMerteRequestStatsByBranch(String branchName) throws Exception;
	public Map<String, Object> createMergeRequest(Map<String, Object> input) throws Exception;
	public Map<String, Object> addMergeRequestComment(Map<String, Object> input) throws Exception;
	public Map<String, Object> acceptMergeRequest(Map<String, Object> input) throws Exception;
	public Map<String, Object> getMergeRequestSimpleStats(Map<String, Object> input) throws Exception;
	public Branch createBranchRequest(Map<String, Object> input) throws Exception;
	public Commit createCommitRequest(Map<String, Object> input, Branch branch) throws Exception;
}
