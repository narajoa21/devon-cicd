package devon.cicd.resource;

import java.util.List;
import java.util.Map;

public interface ImageRepository {
	public List<Map<String, Object>> getImages(Map<String, Object> input) throws Exception;
}
