package devon.cicd.resource.aws;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import software.amazon.awssdk.services.cloudwatch.model.StandardUnit;

public class AwsUtils {
	static public void makeUsageName(Map<String, List<Object>> map, String name, String awsName, StandardUnit unit, String namespace) {
		List<Object> objects = new ArrayList<Object>(3);
	
		objects.add(awsName);
		objects.add(unit);
		objects.add(namespace);
		
		map.put(name, objects);
	}
	static public String getTargetImageName(Map<String, Object> input) {
		String imageName = (String)input.get("targetImage");
		if(imageName == null) {
			return null;
		}
		String branchName = (String)input.get("branchName");
		int index = imageName.indexOf(branchName);
		if(index == -1) {
			return null;
		}
		int end = imageName.indexOf(',', index);
		if(end == -1) {
			end = imageName.indexOf(']', index);
		}
		
		if(end == -1) {
			return imageName.substring(index);			
		}
		return imageName.substring(index, end);
	}
	
	static public String getCurrentImageNameHeader(Map<String, Object> input) {
		String currentImage = (String)input.get("currentImage");
		int index = currentImage.lastIndexOf(':');
		if(index == -1) {
			return currentImage;
		}
		return currentImage.substring(0, index + 1);
	}
	
	static public void setStatsTime(Map<String, Object> input, Map<String, Object> condition) throws ParseException {
		String endTimeStr = (String)input.get("date") + " " + (String)input.get("endTime");
		String startTimeStr = (String)input.get("date") + " " + (String)input.get("startTime");
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = df.parse(endTimeStr);                                      
		Long endTime = date.getTime();
		date = df.parse(startTimeStr);                                      
		Long startTime = date.getTime();
		
		condition.put("endTime", endTime);
		condition.put("duration", endTime - startTime);				
	}
}
