package devon.cicd.resource.aws;

import java.util.HashMap;
import java.util.Map;

import devon.cicd.util.DateUtils;
import software.amazon.awssdk.services.codedeploy.model.DeploymentInfo;

public class AwsCdpDataConverter {
	static Map<String, Object> convertDeploymentInfoToMap(DeploymentInfo info){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("deploymentId", info.deploymentId());
		map.put("status", info.statusAsString());
		map.put("startTime", DateUtils.getYYYYMMDDHHMMSS(info.createTime().toEpochMilli()));
		if(info.completeTime() != null) {
			map.put("completeTime", DateUtils.getYYYYMMDDHHMMSS(info.completeTime().toEpochMilli()));
		}
		map.put("computePlatform", info.computePlatformAsString());
		map.put("application", info.applicationName());
		map.put("deploymentGroup", info.deploymentGroupName());
		map.put("creator", info.creatorAsString());
		map.put("description", info.description());
		if(info.deploymentStyle() != null) {
			map.put("deploymentType",  info.deploymentStyle().deploymentType());
		}
		if(info.errorInformation() != null) {
			map.put("errorMessage", info.errorInformation().message());
		}
		
		return map;		
	}
}
