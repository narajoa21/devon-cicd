package devon.cicd.resource.aws.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;
import software.amazon.awssdk.services.elasticloadbalancingv2.ElasticLoadBalancingV2Client;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.Action;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.DescribeListenersRequest;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.DescribeListenersResponse;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.DescribeLoadBalancersRequest;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.DescribeLoadBalancersResponse;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.DescribeRulesRequest;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.DescribeRulesResponse;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.DescribeTargetGroupsRequest;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.DescribeTargetGroupsResponse;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.DescribeTargetHealthRequest;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.DescribeTargetHealthResponse;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.Listener;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.LoadBalancer;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.ModifyListenerRequest;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.Rule;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetGroup;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetHealthDescription;

public class ElbApi extends AwsApi {
	ElasticLoadBalancingV2Client client;
	public ElbApi(String cicdStep, Map<String, Object> input) {
		super(cicdStep);
		client = getElbClient(input);
	}
	
    public List<TargetHealthDescription> getTargetGroupHealth(String targetGroupArn) {
    	DescribeTargetHealthRequest req = DescribeTargetHealthRequest.builder().targetGroupArn(targetGroupArn).build();
        DescribeTargetHealthResponse targetGroupsRes = client.describeTargetHealth(req);
        return targetGroupsRes.targetHealthDescriptions();
    }

    public TargetGroup getTargetGroup(String targetGroupArn) {
    	DescribeTargetGroupsRequest req = DescribeTargetGroupsRequest.builder().targetGroupArns(targetGroupArn).build();
    	
    	DescribeTargetGroupsResponse res = client.describeTargetGroups(req);
        return res.targetGroups().get(0);
    }
    
    public LoadBalancer getLoadBalancers(String loadBalancerName) {
    	DescribeLoadBalancersRequest req = DescribeLoadBalancersRequest.builder().names(loadBalancerName).build();
    	
    	DescribeLoadBalancersResponse res = client.describeLoadBalancers(req);
    	List<LoadBalancer> list  =res.loadBalancers();
    	return list.get(0);
    }   
    
    
    public List<Listener> getListeners(String loadBalancerArn) {
        DescribeListenersRequest describeListenersReq = DescribeListenersRequest.builder().loadBalancerArn(loadBalancerArn).build();
        DescribeListenersResponse describeListenersRes= client.describeListeners(describeListenersReq);
        return describeListenersRes.listeners();
    }
    
    public List<Rule> getRules(String listenerArn) {
        DescribeRulesRequest describeRulesReq = DescribeRulesRequest.builder().listenerArn(listenerArn).build();
        DescribeRulesResponse describeRulesRes= client.describeRules(describeRulesReq);
        return describeRulesRes.rules();
    }

    public Map<String, String> getCoudWatchInfo(String loadBalancerName){
    	software.amazon.awssdk.services.elasticloadbalancingv2.model.LoadBalancer elb = getLoadBalancers(loadBalancerName);
    	if(elb == null) {
    		return null;
    	}
    	List<Listener> listeners  = getListeners(elb.loadBalancerArn());
    	if(listeners == null || listeners.size() == 0) {
    		return null;
    	}
  
      	Map<String, String> reCloudWatchInfo = new HashMap<String, String>(5);
        String arn = elb.loadBalancerArn();
        
        reCloudWatchInfo.put("loadBalancer", arn.substring(arn.indexOf('/') + 1));
        
        List<Rule> rules;
       	List<Action> actions;
    	for(Listener listener : listeners) {
    		rules = getRules(listener.listenerArn());
    		arn = null;
    		for(Rule rule : rules) {
    			actions = rule.actions();
    			if(actions == null || actions.size() == 0) {
    				continue;
    			}
    			for(Action action : actions) {
    				arn = action.targetGroupArn();
    				if(arn != null) {
    					arn = "targetgroup" + arn.substring(arn.indexOf('/'));
    					break;
    				}
    			}
    			if(arn != null) {
    				break;
    			}
    		}
    		if(arn != null) {
    			reCloudWatchInfo.put("" + listener.port(), arn);
    		}
    	}
    	return reCloudWatchInfo;
    }
    
    public Map<String, String> getTargetsGroupByLoadBalancer(String loadBalancerName){
    	software.amazon.awssdk.services.elasticloadbalancingv2.model.LoadBalancer elb = getLoadBalancers(loadBalancerName);
    	if(elb == null) {
    		return null;
    	}
    	
    	List<Listener> listeners  = getListeners(elb.loadBalancerArn());
    	if(listeners == null || listeners.size() == 0) {
    		return null;
    	}
    	
    	List<Rule> rules;
       	List<Action> actions;
    	TargetGroup targetGroup;
    	Map<String, String> reTargetGroups = new HashMap<String, String>(listeners.size());
    	String targetGroupName;
    	for(Listener listener : listeners) {
    		rules = getRules(listener.listenerArn());
    		targetGroupName = null;
    		for(Rule rule : rules) {
    			actions = rule.actions();
    			if(actions == null || actions.size() == 0) {
    				continue;
    			}
    			for(Action action : actions) {
    				targetGroup = getTargetGroup(action.targetGroupArn());
    				if(targetGroup != null) {
    					targetGroupName = targetGroup.targetGroupName();
    					break;
    				}
    			}
    			if(targetGroupName != null) {
    				break;
    			}
    		}
    		if(targetGroupName != null) {
    			reTargetGroups.put("" + listener.port(), targetGroupName);
    		}
    	}
    	return reTargetGroups;
    }
		

    public List<Listener> resetListenerTargets(String loadBalancerArn, List<TargetMapping> targetMappings) {
    	DescribeListenersRequest describeListenersReq = DescribeListenersRequest.builder().loadBalancerArn(loadBalancerArn).build();
        DescribeListenersResponse describeListenersResult = client.describeListeners(describeListenersReq);
        List<Listener> listeners = describeListenersResult.listeners();

        //Each targetMappings, modify listenerArn - targetGroupArn relation
        for (int i = 0; i < targetMappings.size(); i++) {
            TargetMapping targetMapping = targetMappings.get(i);
            String listenerArn = targetMapping.getListenerArn();
            String targetGroupArn = targetMapping.getTargetGroupArn();

            for (int i1 = 0; i1 < listeners.size(); i1++) {
                Listener listener = listeners.get(i1);
                if (listener.listenerArn().equals(listenerArn)) {
                    //Action action = listener.defaultActions().get(0);
                    Action action = Action.builder().targetGroupArn(targetGroupArn).build();
                    
                    ModifyListenerRequest modifyListenerRequest = ModifyListenerRequest.builder()
                            .listenerArn(listener.listenerArn())
                            .port(listener.port())
                            .defaultActions(action)
                            .build();

                    client.modifyListener(modifyListenerRequest);
                }
            }
        }

        describeListenersReq = DescribeListenersRequest.builder().loadBalancerArn(loadBalancerArn).build();
        DescribeListenersResponse describeListenersRes = client.describeListeners(describeListenersReq);
        return describeListenersRes.listeners();
    }

    @Data
    public static class TargetMapping {
        private String listenerArn;
        private String targetGroupArn;

        public TargetMapping() {
        }

        public TargetMapping(String listenerArn, String targetGroupArn) {
            this.listenerArn = listenerArn;
            this.targetGroupArn = targetGroupArn;
        }
    }	
}
