package devon.cicd.resource.aws.api;

import java.util.Map;

import devon.cicd.DevonCicdApplication;
import devon.cicd.config.CicdConfigManager;
import devon.cicd.config.Constants;
import devon.cicd.factory.InterfaceFactory;
import devon.cicd.util.DataConverter;
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient;
import software.amazon.awssdk.services.cloudwatchlogs.CloudWatchLogsClient;
import software.amazon.awssdk.services.codedeploy.CodeDeployClient;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ecr.EcrClient;
import software.amazon.awssdk.services.ecs.EcsClient;
import software.amazon.awssdk.services.elasticloadbalancingv2.ElasticLoadBalancingV2Client;

public abstract class AwsApi {
	private InterfaceFactory interfaceFactory;
	private CicdConfigManager cicdConfigManager;
	private String solutionCode;	
	
	public AwsApi(String cicdStep) {
		interfaceFactory = DevonCicdApplication.applicationContext.getBean(InterfaceFactory.class);
		cicdConfigManager = DevonCicdApplication.applicationContext.getBean(CicdConfigManager.class);
		Map<String, Object> cicd = cicdConfigManager.readCicd(cicdStep);
		if(cicd == null) {
			throw new RuntimeException("Cicd Step(" + cicdStep + ") does not exist!");
		}
		solutionCode = (String)cicd.get("solutionCode");		
	}
	
	public InterfaceFactory getInterfaceFactory() {
		return this.interfaceFactory;
	}
	
	public CicdConfigManager getCicdConfigManager() {
		return this.cicdConfigManager;
	}
	
	public String solutionCode() {
		return this.solutionCode;
	}
	
	protected Object getApi(Map<String, Object> input) {
		return interfaceFactory.getResourceApi(input);
	}
	
	protected EcsClient getEcsClient(Map<String, Object> input) {
		return (EcsClient)getApi(DataConverter.makeApiMapClone(input, solutionCode, Constants.RESOURCE_AWS_ECS));
	}
	
	protected Ec2Client getEc2Client(Map<String, Object> input) {
		return (Ec2Client)getApi(DataConverter.makeApiMapClone(input, solutionCode, Constants.RESOURCE_AWS_EC2));
	}
	
	protected EcrClient getEcrClient(Map<String, Object> input) {
		return (EcrClient)getApi(DataConverter.makeApiMapClone(input, solutionCode, Constants.RESOURCE_AWS_ECR));
	}

	protected ElasticLoadBalancingV2Client getElbClient(Map<String, Object> input) {
		return (ElasticLoadBalancingV2Client)getApi(DataConverter.makeApiMapClone(input, solutionCode, Constants.RESOURCE_AWS_ELB));
	}


	protected CloudWatchClient getCloudWatchClient(Map<String, Object> input) {
		return (CloudWatchClient)getApi(DataConverter.makeApiMapClone(input, solutionCode, Constants.RESOURCE_AWS_CLOUDWATCH));
	}

	protected CloudWatchLogsClient getCloudWatchLogClient(Map<String, Object> input) {
		return (CloudWatchLogsClient)getApi(DataConverter.makeApiMapClone(input, solutionCode, Constants.RESOURCE_AWS_LOG));
	}
	
	protected CodeDeployClient getCodeDeployClient(Map<String, Object> input) {
		return (CodeDeployClient)getApi(DataConverter.makeApiMapClone(input, solutionCode, Constants.RESOURCE_AWS_CODEDEPLOY));
	}
	
}
