package devon.cicd.resource.aws.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import software.amazon.awssdk.services.ecs.EcsClient;
import software.amazon.awssdk.services.ecs.model.Cluster;
import software.amazon.awssdk.services.ecs.model.ContainerDefinition;
import software.amazon.awssdk.services.ecs.model.ContainerInstance;
import software.amazon.awssdk.services.ecs.model.CreateServiceRequest;
import software.amazon.awssdk.services.ecs.model.CreateServiceResponse;
import software.amazon.awssdk.services.ecs.model.CreateTaskSetRequest;
import software.amazon.awssdk.services.ecs.model.CreateTaskSetResponse;
import software.amazon.awssdk.services.ecs.model.DeleteServiceRequest;
import software.amazon.awssdk.services.ecs.model.DeleteServiceResponse;
import software.amazon.awssdk.services.ecs.model.DescribeClustersRequest;
import software.amazon.awssdk.services.ecs.model.DescribeClustersResponse;
import software.amazon.awssdk.services.ecs.model.DescribeContainerInstancesRequest;
import software.amazon.awssdk.services.ecs.model.DescribeContainerInstancesResponse;
import software.amazon.awssdk.services.ecs.model.DescribeServicesRequest;
import software.amazon.awssdk.services.ecs.model.DescribeServicesResponse;
import software.amazon.awssdk.services.ecs.model.DescribeTaskDefinitionRequest;
import software.amazon.awssdk.services.ecs.model.DescribeTaskDefinitionResponse;
import software.amazon.awssdk.services.ecs.model.DescribeTasksRequest;
import software.amazon.awssdk.services.ecs.model.DescribeTasksResponse;
import software.amazon.awssdk.services.ecs.model.DesiredStatus;
import software.amazon.awssdk.services.ecs.model.LaunchType;
import software.amazon.awssdk.services.ecs.model.ListClustersResponse;
import software.amazon.awssdk.services.ecs.model.ListTaskDefinitionsRequest;
import software.amazon.awssdk.services.ecs.model.ListTaskDefinitionsResponse;
import software.amazon.awssdk.services.ecs.model.ListTasksRequest;
import software.amazon.awssdk.services.ecs.model.ListTasksResponse;
import software.amazon.awssdk.services.ecs.model.LoadBalancer;
import software.amazon.awssdk.services.ecs.model.RegisterTaskDefinitionRequest;
import software.amazon.awssdk.services.ecs.model.RegisterTaskDefinitionResponse;
import software.amazon.awssdk.services.ecs.model.Service;
import software.amazon.awssdk.services.ecs.model.SortOrder;
import software.amazon.awssdk.services.ecs.model.StopTaskRequest;
import software.amazon.awssdk.services.ecs.model.StopTaskResponse;
import software.amazon.awssdk.services.ecs.model.Task;
import software.amazon.awssdk.services.ecs.model.TaskDefinition;
import software.amazon.awssdk.services.ecs.model.TaskSet;
import software.amazon.awssdk.services.ecs.model.UpdateServiceRequest;
import software.amazon.awssdk.services.ecs.model.UpdateServiceResponse;

public class EcsApi extends AwsApi {
	private EcsClient client;
	public EcsApi(String cicdStep, Map<String, Object> input) {
		super(cicdStep);
      	client = getEcsClient(input); // stageCode, region
	}

    public List<Cluster> listClusters() {
        ListClustersResponse listClustersRes = client.listClusters();
        List<String> clusterArns = listClustersRes.clusterArns();
        
        DescribeClustersRequest describeClustersReq = DescribeClustersRequest.builder().clusters(clusterArns).build();
        DescribeClustersResponse describeClustersRes = client.describeClusters(describeClustersReq);

        return describeClustersRes.clusters();
    }

    public TaskDefinition getTaskDefinition(String taskDefinition) { // (String)input.get("taskDefinition")
       	DescribeTaskDefinitionRequest describeTaskDefinitionReq = DescribeTaskDefinitionRequest.builder().taskDefinition(taskDefinition).build();
        DescribeTaskDefinitionResponse describeTaskDefinitionRes = client.describeTaskDefinition(describeTaskDefinitionReq);
        return describeTaskDefinitionRes.taskDefinition();
    }
    
    public TaskDefinition getLastTaskDefinitionVersion(String familyName) {  // (String)input.get("familyName")
    	ListTaskDefinitionsRequest listTaskDefinitionsReq = ListTaskDefinitionsRequest.builder()
                .familyPrefix(familyName)
                .sort(SortOrder.DESC)
                .maxResults(1)
				.build();
       	
        ListTaskDefinitionsResponse listTaskDefinitionsRes = client.listTaskDefinitions(listTaskDefinitionsReq);
        String taskDefinitionArn = listTaskDefinitionsRes.taskDefinitionArns().get(0);

        String[] split = taskDefinitionArn.split("/");
        String nameAndRevision = split[split.length - 1];
        DescribeTaskDefinitionRequest describeTaskDefinitionReq = DescribeTaskDefinitionRequest.builder().taskDefinition(nameAndRevision).build();
        DescribeTaskDefinitionResponse describeTaskDefinitionRes = client.describeTaskDefinition(describeTaskDefinitionReq);
        return describeTaskDefinitionRes.taskDefinition();
    }
    
    private  RegisterTaskDefinitionRequest makeRegisterTaskDefinitionRequest(TaskDefinition taskDef, List<ContainerDefinition> containerDefs) {
        return RegisterTaskDefinitionRequest.builder()
                .family(taskDef.family())
                .taskRoleArn(taskDef.taskRoleArn())
                .containerDefinitions(((containerDefs==null)? taskDef.containerDefinitions(): containerDefs))
                .cpu(taskDef.cpu())
                .executionRoleArn(taskDef.executionRoleArn())
                .memory(taskDef.memory())
                .networkMode(taskDef.networkMode())
                .placementConstraints(taskDef.placementConstraints())
                .requiresCompatibilities(taskDef.requiresCompatibilities())
                .volumes(taskDef.volumes())
                .ipcMode(taskDef.ipcMode())
                .build();
    }

    private RegisterTaskDefinitionRequest replaceImageInTaskDefinition(TaskDefinition taskDef,  String image, String containerName) {
        List<ContainerDefinition> containerDefs = taskDef.containerDefinitions();
        if(containerDefs == null | containerDefs.size() == 0) {
        	return null;
        }
        
        ContainerDefinition containerDef;
        List<ContainerDefinition> newContainerDefs = new ArrayList<ContainerDefinition>(containerDefs.size());
        for (int i = 0; i < containerDefs.size(); i++) {
        	containerDef = containerDefs.get(i);
        	if(containerName == null || containerDef.name().equals(containerName)) {
        		newContainerDefs.add(containerDef.toBuilder().image(image).build());        		
        	}else {
        		newContainerDefs.add(containerDef);         
        	}
        }
        
        return  makeRegisterTaskDefinitionRequest(taskDef, newContainerDefs);
    }
    
    private TaskDefinition registerTaskDefinition(RegisterTaskDefinitionRequest req) {
    	if(req == null) {
    		return null;
    	}
    	RegisterTaskDefinitionResponse res = client.registerTaskDefinition(req);
        return res.taskDefinition();    	
    }
   
    public TaskDefinition updateTaskDefinitionVersionWithImage(TaskDefinition taskDef,  String image, String containerName) {
    	return registerTaskDefinition(replaceImageInTaskDefinition(taskDef, image, containerName));
    }
   
    public TaskDefinition updateTaskDefinitionVersionWithImage(Map<String, Object> input) {
        return registerTaskDefinition(replaceImageInTaskDefinition(getLastTaskDefinitionVersion((String)input.get("familyName")), (String)input.get("image"), (String)input.get("containerName")));
    }

    public TaskDefinition updateTaskDefinitionVersionWithDefinition(String familyName, TaskDefinition taskDefinition) {
        return registerTaskDefinition(makeRegisterTaskDefinitionRequest(getLastTaskDefinitionVersion(familyName), null));
    }
    
    public Service createService(Map<String, Object> input, TaskDefinition taskDefinition) {
       	software.amazon.awssdk.services.ecs.model.LoadBalancer lb = software.amazon.awssdk.services.ecs.model.LoadBalancer.builder()
                .containerName((String)input.get("containerName"))
                .containerPort((int)input.get("containerPort"))
                .targetGroupArn((String)input.get("targetGroupArn"))
                .build();

        String launchTypeStr = (String)input.get("launchType");
        LaunchType lauchType = LaunchType.FARGATE;
        if("ec2".equals(launchTypeStr)) {
        	lauchType = LaunchType.EC2;
        }

        String newDefinition = taskDefinition.family() + ":" + taskDefinition.revision();
        CreateServiceRequest createServiceReq = CreateServiceRequest.builder()
                .cluster((String)input.get("clusterName"))
                .launchType(lauchType)
                .serviceName((String)input.get("serviceName"))
                .taskDefinition(newDefinition)
                .loadBalancers(lb)
                .healthCheckGracePeriodSeconds(input.get("healthCheckGracePeriodSeconds") == null ? 0 : (Integer)input.get("healthCheckGracePeriodSeconds"))
                .desiredCount((int)input.get("scale"))
                .build();

        CreateServiceResponse createServiceRes = client.createService(createServiceReq);
        return createServiceRes.service();
    }

    public Service getService(Map<String, Object>input) {
       	DescribeServicesRequest descSvcReq =DescribeServicesRequest.builder()
                        .cluster((String)input.get("clusterName"))
                        .services((String)input.get("serviceName"))
                        .build();
        
        DescribeServicesResponse descSvcRes = client.describeServices(descSvcReq);
        if (descSvcRes.services().size() > 0) {
            return descSvcRes.services().get(0);
        } else {
            return null;
        }
    }
  
    public Service updateService(Map<String, Object> input, TaskDefinition taskDefinition) {
       	String newDefinition = taskDefinition.family() + ":" + taskDefinition.revision();
       	
        UpdateServiceRequest updateReq = UpdateServiceRequest.builder()
                .cluster((String)input.get("clusterName"))
                .service((String)input.get("serviceName"))
                .desiredCount((int)input.get("scale"))
                .taskDefinition(newDefinition)
                .healthCheckGracePeriodSeconds(input.get("healthCheckGracePeriodSeconds") == null ? 0 : (Integer)input.get("healthCheckGracePeriodSeconds"))
                .forceNewDeployment(true)
                .build();
        
        UpdateServiceResponse updateServiceRes = client.updateService(updateReq);
        return updateServiceRes.service();
    }

    public Service deleteService(Map<String, Object> input) {
       	List<Task> tasks = this.getTasksByServiceName(input);

        UpdateServiceRequest scaleDownReq = UpdateServiceRequest.builder()
                .cluster((String)input.get("clusterName"))
                .service((String)input.get("serviceName"))
                .desiredCount(0)
                .build();
        
        UpdateServiceResponse scaleDownRes = client.updateService(scaleDownReq);

        DeleteServiceRequest delSvcReq = DeleteServiceRequest.builder()
                .cluster((String)input.get("clusterName"))
                .service((String)input.get("serviceName"))
                .build();
        
        DeleteServiceResponse delSvcRes = client.deleteService(delSvcReq);

        //stop tasks
        //tasks
        StopTaskResponse stopTaskResult;
        for (int i = 0; i < tasks.size(); i++) {
            String taskArn = tasks.get(i).taskArn();
            if (!"STOPPED".equals(tasks.get(i).desiredStatus())) {
                try {
                    StopTaskRequest stopTaskReq = StopTaskRequest.builder()
                            .cluster((String)input.get("clusterName"))
                            .task(taskArn)
                            .reason("stop by Devon CICD")
                            .build();
                    stopTaskResult = client.stopTask(stopTaskReq);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return delSvcRes.service();
    }
    
    public Task stopTask(Map<String, Object> input) {
       	StopTaskRequest stopTaskReq = StopTaskRequest.builder()
                    		.cluster((String)input.get("clusterName"))
                            .task((String)input.get("taskArn"))
                            .reason("stop by Devon CICD")
                            .build();
        StopTaskResponse stopTaskRes = client.stopTask(stopTaskReq);
        return stopTaskRes.task();
    }
    
    public void stopTasks(Map<String, Object> input, List<Task> tasks) {
    	if (tasks != null) {
    		Task task;
    		StopTaskResponse stopTaskRes; 
            for (int i = 0; i < tasks.size(); i++) {
                try {
                    task = tasks.get(i);
                    StopTaskRequest stopTaskReq = StopTaskRequest.builder()
                    		.cluster((String)input.get("clusterName"))
                            .task(task.taskArn())
                            .reason("stop by Devon CICD")
                            .build();
                    stopTaskRes = client.stopTask(stopTaskReq);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public Service scaleService(Map<String, Object> input) {
       	UpdateServiceRequest updateReq = UpdateServiceRequest.builder()
                .cluster((String)input.get("clusterName"))
                .service((String)input.get("serviceName"))
                .desiredCount((int)input.get("scale"))
                .build();

        UpdateServiceResponse updateServiceRe = client.updateService(updateReq);
        return updateServiceRe.service();
    }
    
    public Service restartService(Map<String, Object> input) {
       	UpdateServiceRequest updateReq = UpdateServiceRequest.builder()
                .cluster((String)input.get("clusterName"))
                .service((String)input.get("serviceName"))
                .taskDefinition((String)input.get("taskDefinitionArn"))
                .forceNewDeployment(true)
                .build();

        UpdateServiceResponse updateServiceRes = client.updateService(updateReq);
        return updateServiceRes.service();
    }
    
    private List<Task> getTasksByRequest(Map<String, Object> input, ListTasksRequest req){
        ListTasksResponse listTaskRes = client.listTasks(req);
        List<String> arns = listTaskRes.taskArns();
        
        if(arns == null || arns.size() == 0) {
        	return null;
        }
        
        DescribeTasksRequest taskReq = DescribeTasksRequest.builder()
                .cluster((String)input.get("clusterName"))
                .tasks(arns)
                .build();
        
        DescribeTasksResponse res = client.describeTasks(taskReq);
        return res.tasks();
    }

    public List<Task> getTasksByServiceName(Map<String, Object> input) {
       	ListTasksRequest req = ListTasksRequest.builder()
                .cluster((String)input.get("clusterName"))
                .serviceName((String)input.get("serviceName"))
                .build();
       	
       	return getTasksByRequest(input, req);
    }
    
    public List<Task> getTasksAllByServiceName(Map<String, Object> input) {
    	List<Task> reTasks = new ArrayList<Task>();
    	
    	input.put("status", "RUNNING");
    	List<Task> tasks = getTasksByServiceNameByStatus(input);
    	if(tasks != null && tasks.size() > 0) {
    		reTasks.addAll(tasks);
    	}
    	
    	input.put("status", "STOPPED");
    	tasks = getTasksByServiceNameByStatus(input);
    	if(tasks != null && tasks.size() > 0) {
    		reTasks.addAll(tasks);
    	}
    	
    	input.put("status", "PENDING");
    	tasks = getTasksByServiceNameByStatus(input);
    	if(tasks != null && tasks.size() > 0) {
    		reTasks.addAll(tasks);
    	}
    	
    	return reTasks;
    }
    
    public List<Task> getTasksByServiceNameByStatus(Map<String, Object> input) {
       	String status = (String)input.get("status");
    	DesiredStatus inStatus = null;
    	switch(status.toUpperCase()) {
    	case "RUNNING":
    		inStatus = DesiredStatus.RUNNING;
    		break;
    	case "STOPPED":
    		inStatus = DesiredStatus.STOPPED;
    		break;
    	case "PENDING":
    		inStatus = DesiredStatus.PENDING;
    		break;
    	default:
    		throw new RuntimeException("Task status(" + status +") is abnoraml!");
    	}
    	
    	ListTasksRequest req = ListTasksRequest.builder()
                .cluster((String)input.get("clusterName"))
                .desiredStatus(inStatus)
                .serviceName((String)input.get("serviceName"))
                .build();
    	
    	return getTasksByRequest(input, req);
   } 
    
    public Task getTaskByArn(Map<String, Object> input) {
       	DescribeTasksRequest taskReq = DescribeTasksRequest.builder()
                .cluster((String)input.get("clusterName"))
                .tasks((String)input.get("taskArn"))
                .build();
        
        DescribeTasksResponse taskRes = client.describeTasks(taskReq);
        return taskRes.tasks().get(0);
    }
    
    public ContainerInstance getContainerInstanceByArn(Map<String, Object> input) {
       	DescribeContainerInstancesRequest ciReq = DescribeContainerInstancesRequest.builder()
        		.cluster((String)input.get("clusterName"))
                .containerInstances((String)input.get("containerInstanceArn"))
                .build();
        
        DescribeContainerInstancesResponse ciRes = client.describeContainerInstances(ciReq);

        return ciRes.containerInstances().get(0);
    }
    
    public String getTargetGroupArnFromService(Map<String, Object> input, Service service) {
        String targetGroupArn = null;
        if (service == null) {
            return null;
        }
        
        List<LoadBalancer> loadBalancers = service.loadBalancers();
        for (int i1 = 0; i1 < loadBalancers.size(); i1++) {
            //only if same containerName
            if (loadBalancers.get(i1).containerName().equals((String)input.get("containerName"))) {
                targetGroupArn = loadBalancers.get(i1).targetGroupArn();
            }
        }
        return targetGroupArn;
    }
    
    public TaskSet createTaskSet(Map<String, Object> input) {
       	CreateTaskSetRequest req = CreateTaskSetRequest.builder()
       			.cluster((String)input.get("clusterName"))
       			.service((String)input.get("serviceName"))
       			.taskDefinition((String)input.get("taskDefinitionArn"))
       			.externalId("Devon-CICD")
       			.build();
       	CreateTaskSetResponse res = client.createTaskSet(req);
       	return res.taskSet();
    }
}
