package devon.cicd.resource.aws.api;

import java.util.List;
import java.util.Map;

import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesResponse;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.Reservation;

public class Ec2Api extends AwsApi {
	Ec2Client client;
	
	public Ec2Api(String cicdStep, Map<String, Object> input) {
		super(cicdStep);
		client = getEc2Client(input);
	}

    public Instance getEc2ById(String ec2InstanceId) {
       	DescribeInstancesRequest instanceReq = DescribeInstancesRequest.builder()
        		.instanceIds(ec2InstanceId)
                .build();
        
        DescribeInstancesResponse instancesRes = client.describeInstances(instanceReq);
 
        List<Reservation> reservs = instancesRes.reservations();
        if(reservs == null) {
        	return null;
        }
        List<Instance> instances = reservs.get(0).instances();
        return instances.get(0);
    }	
}
