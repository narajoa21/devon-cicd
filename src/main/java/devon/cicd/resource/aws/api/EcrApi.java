package devon.cicd.resource.aws.api;

import java.util.List;
import java.util.Map;

import devon.cicd.config.Constants;
import software.amazon.awssdk.services.ecr.EcrClient;
import software.amazon.awssdk.services.ecr.model.DescribeImagesRequest;
import software.amazon.awssdk.services.ecr.model.DescribeImagesResponse;
import software.amazon.awssdk.services.ecr.model.ImageDetail;

public class EcrApi extends AwsApi{
	EcrClient client;
	public EcrApi(String cicdStep, Map<String, Object> input) {
		super(cicdStep);
		client = this.getEcrClient(input);
	}
	
    public List<ImageDetail> listImages(String imageRepository) {
    	DescribeImagesRequest describeImagesReq = DescribeImagesRequest.builder()
        		.maxResults(Constants.LIST_HUGE_SIZE)
        		.repositoryName(imageRepository)
        		.build();
        
        DescribeImagesResponse imagesRes = client.describeImages(describeImagesReq);
        return imagesRes.imageDetails();
    }   
}
