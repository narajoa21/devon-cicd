package devon.cicd.resource.aws.api;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import software.amazon.awssdk.services.codedeploy.CodeDeployClient;
import software.amazon.awssdk.services.codedeploy.model.ApplicationInfo;
import software.amazon.awssdk.services.codedeploy.model.CodeDeployResponseMetadata;
import software.amazon.awssdk.services.codedeploy.model.ContinueDeploymentRequest;
import software.amazon.awssdk.services.codedeploy.model.ContinueDeploymentResponse;
import software.amazon.awssdk.services.codedeploy.model.CreateDeploymentRequest;
import software.amazon.awssdk.services.codedeploy.model.CreateDeploymentResponse;
import software.amazon.awssdk.services.codedeploy.model.DeploymentInfo;
import software.amazon.awssdk.services.codedeploy.model.DeploymentStatus;
import software.amazon.awssdk.services.codedeploy.model.GetApplicationRequest;
import software.amazon.awssdk.services.codedeploy.model.GetApplicationResponse;
import software.amazon.awssdk.services.codedeploy.model.GetDeploymentRequest;
import software.amazon.awssdk.services.codedeploy.model.GetDeploymentResponse;
import software.amazon.awssdk.services.codedeploy.model.ListDeploymentsRequest;
import software.amazon.awssdk.services.codedeploy.model.ListDeploymentsResponse;
import software.amazon.awssdk.services.codedeploy.model.RevisionLocation;
import software.amazon.awssdk.services.codedeploy.model.StopDeploymentRequest;
import software.amazon.awssdk.services.codedeploy.model.StopDeploymentResponse;
import software.amazon.awssdk.services.codedeploy.model.TimeRange;

public class CodeDeployApi extends AwsApi {
	CodeDeployClient client;

	public CodeDeployApi(String cicdStep, Map<String, Object> input) {
		super(cicdStep);
		client = getCodeDeployClient(input);
	}

	public List<String> getDeployments(Map<String, Object> input) {
		TimeRange createTimeRange = null;
		if(input.get("endTime") != null) {
			Instant end = Instant.ofEpochMilli((long)input.get("endTime"));
	    	Instant start = Instant.ofEpochMilli((long)input.get("endTime") -  (long)input.get("duration"));
			createTimeRange = TimeRange.builder().start(start).end(end).build();
		}
		
		ListDeploymentsRequest req = ListDeploymentsRequest.builder().applicationName((String)input.get("application")).deploymentGroupName((String)input.get("deploymentGroup")).createTimeRange(createTimeRange).build();
		ListDeploymentsResponse res = client.listDeployments(req);
		return res.deployments();
	}

	public DeploymentInfo getDeployment(String deploymentId) {
		GetDeploymentRequest req = GetDeploymentRequest.builder().deploymentId(deploymentId).build();
		
		GetDeploymentResponse res = client.getDeployment(req);
		return res.deploymentInfo();
	}
	
	public String getRunningDeployment(Map<String, Object> input) {
		List<DeploymentStatus> includeStatuses = new ArrayList<DeploymentStatus>(3);
		includeStatuses.add(DeploymentStatus.CREATED);
		includeStatuses.add(DeploymentStatus.QUEUED);
		includeStatuses.add(DeploymentStatus.IN_PROGRESS);
		ListDeploymentsRequest req = ListDeploymentsRequest.builder().applicationName((String)input.get("application")).deploymentGroupName((String)input.get("deploymentGroup")).includeOnlyStatuses(includeStatuses).build();
		
		ListDeploymentsResponse res = client.listDeployments(req);
		List<String> deployments = res.deployments();
		if(deployments == null || deployments.size() == 0) {
			return null;
		}
		return deployments.get(0);
	}

	
	public ApplicationInfo getApplication(Map<String, Object> input) {
		GetApplicationRequest req = GetApplicationRequest.builder().applicationName((String)input.get("application")).build();
		
		GetApplicationResponse res = client.getApplication(req);
		return res.application();
	}
	
	public CodeDeployResponseMetadata continueDeployment(Map<String, Object> input) {
		ContinueDeploymentRequest req = ContinueDeploymentRequest.builder().deploymentId((String)input.get("deploymentId")).build();
		
		ContinueDeploymentResponse res = client.continueDeployment(req);
		return res.responseMetadata();
	}

	public String stopDeployment(Map<String, Object> input) {
		StopDeploymentRequest req = StopDeploymentRequest.builder().deploymentId((String)input.get("deploymentId")).autoRollbackEnabled(true).build();
		
		StopDeploymentResponse res = client.stopDeployment(req);
		return res.statusAsString() + ": " + res.statusMessage();
	}
	
	public String createDeployment(Map<String, Object> input) {
		List<String> deployments =  getDeployments(input);
		if(deployments == null || deployments.size() == 0) {
			return null;
		}
		
		DeploymentInfo info;
		RevisionLocation revision = null;
		for(String deploymentId: deployments){
			info = getDeployment(deploymentId);
			revision = info.revision();
			if(revision != null) {
				break;
			}
		}
		if(revision == null) {
			return null;
		}
			
		CreateDeploymentRequest req = CreateDeploymentRequest.builder()
				.deploymentGroupName((String)input.get("deploymentGroup"))
				.applicationName((String)input.get("application"))
				.revision(revision)
				.description("By Devon-CICD")
				.build();
		CreateDeploymentResponse res = client.createDeployment(req);
		return res.deploymentId();
	}
}
