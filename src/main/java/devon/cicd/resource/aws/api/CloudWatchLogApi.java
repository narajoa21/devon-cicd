package devon.cicd.resource.aws.api;

import java.util.List;
import java.util.Map;

import software.amazon.awssdk.services.cloudwatchlogs.CloudWatchLogsClient;
import software.amazon.awssdk.services.cloudwatchlogs.model.GetLogEventsRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.GetLogEventsResponse;
import software.amazon.awssdk.services.cloudwatchlogs.model.OutputLogEvent;

public class CloudWatchLogApi extends AwsApi {
	CloudWatchLogsClient client;

	public CloudWatchLogApi(String cicdStep, Map<String, Object> input) {
		super(cicdStep);
		client = getCloudWatchLogClient(input);
	}

    public String getLog(Map<String, Object> input) { // String regionName, String logGroupName, String logStreamName, Integer limit) {	
    	GetLogEventsRequest getLogEventsReq = GetLogEventsRequest.builder()
    			.limit((int)input.get("limit"))
                .startFromHead(false)
                .logGroupName((String)input.get("logGroupName"))
                .logStreamName((String)input.get("logStreamName"))
                .build();

        GetLogEventsResponse logEvents = client.getLogEvents(getLogEventsReq);
        List<OutputLogEvent> events = logEvents.events();

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < events.size(); i++) {
            builder.append(events.get(i).message() + "\n");
        }
        return builder.toString();
    }	
}
