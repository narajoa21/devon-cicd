package devon.cicd.resource.aws.api;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import devon.cicd.config.Constants;
import devon.cicd.data.KeyValue;
import devon.cicd.resource.aws.AwsNames;
import devon.cicd.util.DateUtils;
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient;
import software.amazon.awssdk.services.cloudwatch.model.Dimension;
import software.amazon.awssdk.services.cloudwatch.model.GetMetricDataRequest;
import software.amazon.awssdk.services.cloudwatch.model.GetMetricDataRequest.Builder;
import software.amazon.awssdk.services.cloudwatch.model.GetMetricDataResponse;
import software.amazon.awssdk.services.cloudwatch.model.Metric;
import software.amazon.awssdk.services.cloudwatch.model.MetricDataQuery;
import software.amazon.awssdk.services.cloudwatch.model.MetricDataResult;
import software.amazon.awssdk.services.cloudwatch.model.MetricStat;
import software.amazon.awssdk.services.cloudwatch.model.StandardUnit;

public class CloudWatchApi extends AwsApi{
	CloudWatchClient client;
	public CloudWatchApi(String cicdStep, Map<String, Object> input) {
		super(cicdStep);
		client = getCloudWatchClient(input);
	}

    @SuppressWarnings("unchecked")
    private List<Dimension> makeDimension(Map<String, Object> input){
    	String dimensionName = (String)input.get("dimensionName");
    	
    	String setName;
    	List<Dimension> dimensions = new ArrayList<Dimension>();
    	if(dimensionName.indexOf(",") < 0) {
            String dimensionId = (String)input.get(dimensionName);
            setName = dimensionName.substring(0,1).toUpperCase() + dimensionName.substring(1);
            
        	if(dimensionId != null) {
    	    	dimensions.add(Dimension.builder().name(setName).value(dimensionId).build());
        	}else {
    			List<String> dimensionIds = (List<String>)input.get(dimensionName + "s");	
    	    	for(int i = 0; i < dimensionIds.size(); i++){
    	        	dimensions.add(Dimension.builder().name(setName).value(dimensionIds.get(i)).build());
    	    	}
        	}
    	}else {
    		String [] dimensionNames = dimensionName.split(",");
    		
            if(input.get(dimensionNames[0]) != null) {
    	    	for(String name : dimensionNames) {
    	    		setName = name.substring(0,1).toUpperCase() + name.substring(1);
    	    		dimensions.add(Dimension.builder().name(setName).value((String)input.get(name)).build());
    	    	}
        	} 		
    	}
    	
    	return dimensions;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> getMetricDatas(Map<String, Object> input){
    	List<Dimension> dimensions = makeDimension(input);

    	List<String> metricNames;
    	String metricName = (String)input.get("metricName");

    	if(metricName != null) {
    		metricNames = new ArrayList<String>(1);
    		metricNames.add(metricName);
    	}else {
    	  	metricNames = (List<String>)input.get("metricNames");  		
    	}
    	
    	Metric metric;
    	MetricStat ms;
    	MetricDataQuery metricDataQuery;
    	
    	int size = metricNames.size();
    	List<MetricDataQuery> querys = new ArrayList<>(size);
    	
    	List<Object> usageName;
    	for(int i = 0; i < size; i++) {
    		metricName = metricNames.get(i);
    		usageName = AwsNames.getUsageName(metricName);
    		metric = Metric.builder()
            		.namespace((String)usageName.get(2))
            		.metricName((String)usageName.get(0))
            		.dimensions(dimensions)
            		.build();
    		
    		ms = MetricStat.builder()
            		.metric(metric).period((Integer)Constants.MONITOR_GAP_SECONDS)
            		.unit((StandardUnit)usageName.get(1))
//            		.stat("Average")
            		.stat("Sum")
            		.build();
    		
    		metricDataQuery = MetricDataQuery.builder()
            		.metricStat(ms)
            		.id(metricName)
            		.build();
    		
            querys.add(metricDataQuery);
    	}
        
    	Instant end = Instant.ofEpochMilli((long)input.get("endTime"));
    	Instant start = Instant.ofEpochMilli((long)input.get("endTime") -  (long)input.get("duration"));
    	Builder builder = GetMetricDataRequest.builder();
    	if(input.get("maxDataPoints") != null) {
    		builder = builder.maxDatapoints((Integer)input.get("maxDataPoints"));
    	}
    	
        GetMetricDataRequest req = builder
        		.metricDataQueries(querys)
        		.startTime(start)
        		.endTime(end)
        		.build();
        
        GetMetricDataResponse res =  client.getMetricData(req);
        List<MetricDataResult> results = res.metricDataResults();
        if(results == null ||  results.size() == 0) {
        	return null;
        }
        
        String id;
        List<Instant> timestamps;
        List values;
        int i;
        
        List<KeyValue> stats;
        Map<String, Object> reStats = new HashMap<String, Object>();
        for(MetricDataResult result: results) {
        	
        	timestamps = result.timestamps();
        	size = timestamps.size();
        	if(timestamps == null || size == 0) {
        		continue;
        	}

        	id = result.id();
        	values = result.values();
        	stats = new ArrayList<KeyValue>(size);
        	for(i = 0; i < size; i++) {
        		stats.add(new KeyValue(DateUtils.getHHMM(timestamps.get(i).toEpochMilli()), values.get(i)));
        	}
        	reStats.put(id, stats);
        }
        return reStats;
    }    
}
