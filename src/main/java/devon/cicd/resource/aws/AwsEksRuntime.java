package devon.cicd.resource.aws;

import java.util.List;
import java.util.Map;

public class AwsEksRuntime extends AwsRuntime {
	public AwsEksRuntime(String stageCode, String cicdStep) {
		super(stageCode, cicdStep);
	}

	@Override
	public List<Map<String, Object>> getClusters(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public Map<String, Object> getService(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public List<Map<String, Object>> getServiceEvents(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public List<Map<String, Object>> getTasks(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public Map<String, Object> scaleService(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public List<Map<String, Object>> stopTask(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public List<Map<String, Object>> getTargetInstancesByTargetGroup(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public Map<String, Object> getSimpleStats(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public Map<String, Object> replaceImage(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public List<Map<String, Object>> getInstancesStats(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public List<Map<String, Object>> getInstancesUsage(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public List<Map<String, Object>> getLoadBalancesStats(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public List<Map<String, Object>> getLoadBalancesUsage(Map<String, Object> input) throws Exception {
		return null;
	}

	@Override
	public Map<String, Object> isRunningDeployment(Map<String, Object> input) throws Exception {
		return null;
	}
}
