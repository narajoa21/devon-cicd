package devon.cicd.resource.aws;

import software.amazon.awssdk.services.ecs.model.Service;
import software.amazon.awssdk.services.ecs.model.Task;
import software.amazon.awssdk.services.ecs.model.TaskDefinition;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
public class EcsRedisResource {

    public EcsRedisResource() {
        this.blue = new Resource();
        this.green = new Resource();
    }

    private Long appId;
    private Long sgrId;
    private Resource blue;
    private Resource green;

    @Data
    @NoArgsConstructor
    public static class Resource {
        private Service service;
        private List<Task> tasks;
        private TaskDefinition taskDefinition;
    }
}