package devon.cicd.resource.aws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import devon.cicd.DevonCicdApplication;
import devon.cicd.config.Constants;
import devon.cicd.factory.InterfaceFactory;
import devon.cicd.resource.DeploymentJob;
import devon.cicd.resource.aws.api.CloudWatchApi;
import devon.cicd.resource.aws.api.Ec2Api;
import devon.cicd.resource.aws.api.EcrApi;
import devon.cicd.resource.aws.api.EcsApi;
import devon.cicd.resource.aws.api.ElbApi;
import devon.cicd.util.DateUtils;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ecr.model.ImageDetail;
import software.amazon.awssdk.services.ecs.model.ContainerInstance;
import software.amazon.awssdk.services.ecs.model.Deployment;
import software.amazon.awssdk.services.ecs.model.Service;
import software.amazon.awssdk.services.ecs.model.ServiceEvent;
import software.amazon.awssdk.services.ecs.model.Task;
import software.amazon.awssdk.services.ecs.model.TaskDefinition;
import software.amazon.awssdk.services.ecs.model.TaskSet;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.Rule;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetHealthDescription;

public class AwsEcsRuntime extends AwsRuntime{
	public AwsEcsRuntime(String stageCode, String cicdStep) {
		super(stageCode, cicdStep);
	}
	
	private Map<String, Object> makeConditionStageAndService(Map<String, Object> input){
		Map<String, Object> condition = makeConditionStage(input);
		condition.put("clusterName", (String)getConfigValue("clusterName"));	
		condition.put("serviceName", (String)getConfigValue("serviceName"));	
		return condition;
	}
	
	@Override
	public List<Map<String, Object>> getClusters(Map<String, Object> input) throws Exception {
		Map<String, Object> condition = makeConditionStage(input);
		List<ImageDetail> images = new EcrApi("R", condition).listImages((String)getConfigValue("imageRepository"));
		if(images == null || images.size() == 0) {
			return null;
		}
		
		List<Map<String, Object>> reImages = new ArrayList<Map<String, Object>>(images.size());
		Map<String, Object> image;
		for(ImageDetail detail : images) {
			image = new HashMap<String, Object>();
			image.put("registryId", detail.registryId());
			image.put("tag", detail.imageTags().toString());
			image.put("imageRepository", detail.repositoryName());
			if(detail.imagePushedAt() != null) {
				image.put("pushTime", DateUtils.getYYYYMMDDHHMMSS(detail.imagePushedAt().toEpochMilli()));
			}
			image.put("size", detail.imageSizeInBytes()/1000000);
			image.put("digest", detail.imageDigest());
			reImages.add(image);
		}
		
		return reImages;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String, Object> getService(Map<String, Object> input) throws Exception {
		Map<String, Object> condition = makeConditionStageAndService(input);
		
		EcsApi ecsApi = new EcsApi("E", condition);
		Service service = ecsApi.getService(condition);
		if(service == null) {
			return null;
		}
		
		Map<String, Object> reService = AwsEcsDataConverter.convertEcsServiceToMap(service);
		reService.put("clusterName", condition.get("clusterName"));
		
		Map<String, Object> taskDefinition = null;
		String id = null;
		if(reService.get("taskDefinition") !=  null) {
			id = service.taskDefinition();
			taskDefinition = getTaskDefinitionSimple(id, ecsApi);
			reService.put("taskDefinition", taskDefinition);
		}
		
		List<Map<String, Object>> deploys = (List<Map<String, Object>>)reService.get("deploys");
		if(deploys != null) {
			String deployTaskDefinition;
			for(Map<String, Object> deploy : deploys) {
				deployTaskDefinition = (String)deploy.get("taskDefinition");
				if(id != null &&  deployTaskDefinition != null && id.equals(deployTaskDefinition)) {
					deploy.put("taskDefinition", taskDefinition);					
				}else {
					deploy.put("taskDefinition", getTaskDefinitionSimple(deployTaskDefinition, ecsApi));
				}
			}
			
			if(deploys.size() > 1 && "CODE_DEPLOY".equals((String)reService.get("deploymentController"))) {
				String deploymentId = getRunningDeployment(input);
				if(deploymentId != null) {
					reService.put("runningDeploymentId", deploymentId);
				}
			}
		}
		
		Map<String, Object> loadBalancer = getTargetGroupByLoadBalancer(input, (List<Map<String, Object>>)reService.get("containers"));
		if(loadBalancer != null) {
			reService.put("loadBalancer", loadBalancer);
			
			if(((Map)reService.get("loadBalancer")).get("listeners") != null && reService.get("deploys") != null) {
				setTargetGroupInDeploys((List<Map<String, Object>>)((Map)reService.get("loadBalancer")).get("listeners"), (List<Map<String, Object>>) reService.get("deploys"));
			}
		}
		
		return reService;
	}
	
	private String getRunningDeployment(Map<String, Object> input) throws Exception{
		InterfaceFactory interfaceFactory = DevonCicdApplication.applicationContext.getBean(InterfaceFactory.class);
		DeploymentJob deploymentJob = interfaceFactory.getDeployment(input);
		return deploymentJob.getRunningDeploymentId(input);
	}
	
	@SuppressWarnings("unchecked")
	private void setTargetGroupInDeploys(List<Map<String, Object>> listenrs, List<Map<String, Object>> deploys) {
		List<Map<String, Object>> actions;
		for(Map<String, Object> deploy: deploys) {
			if(deploy.get("targetGroupArn") == null) {
				continue;
			}
			for(Map<String, Object> listener: listenrs) {
				actions = (List<Map<String, Object>>)listener.get("actions");
				if(actions == null) {
					continue;
				}
				for(Map<String, Object> action : actions) {
					if(action.get("targetGroupArn") == null) {
						continue;
					}
					if(((String)action.get("targetGroupArn")).equals(deploy.get("targetGroupArn"))) {
						deploy.put("targetGroupName", action.get("targetGroupName"));
						break;
					}
				}
			}
		}
	}
	
	@Override
	public List<Map<String, Object>> getTasks(Map<String, Object> input) throws Exception {
		Map<String, Object> condition =  makeConditionStageAndService(input);
		
		EcsApi ecsApi = new EcsApi("E", condition);
		List<Task> tasks = ecsApi.getTasksAllByServiceName(condition);
		if(tasks == null) {
			return null;
		}

		List<Map<String, Object>> reTasks = new ArrayList<Map<String, Object>>(tasks.size());
		ContainerInstance container = null;
		Instance instance;
		Map<String, Object> retask;
		Map<String, Object> sinfo;
		
		Ec2Api ec2Api = new Ec2Api("E", condition);
		for(Task task : tasks) {
			retask = AwsEcsDataConverter.convertEcsTaskToMap(task);
			reTasks.add(retask);
	
			if(task.containerInstanceArn() != null) {
				condition.put("containerInstanceArn", task.containerInstanceArn());
				container = ecsApi.getContainerInstanceByArn(condition);
				sinfo = AwsEcsDataConverter.convertEcsContainerToMap(container);
				retask.put("containerInstance", sinfo);
				
				if(container.ec2InstanceId() != null) {
					instance = ec2Api.getEc2ById(container.ec2InstanceId());
					sinfo = AwsEc2DataConverter.convertEc2InstanceToMap(instance);
					retask.put("ec2Instance", sinfo);
				}
			}
		}
		return reTasks;
	}

	@Override
	public List<Map<String, Object>> getServiceEvents(Map<String, Object> input) throws Exception {
		Map<String, Object> condition =  makeConditionStageAndService(input);
		
		Service service = new EcsApi("E", condition).getService(condition);
		if(service == null) {
			return null;
		}
		
		List<Map<String, Object>> datas = null;
		Map<String, Object> data;
		
		List<ServiceEvent> events = service.events();
		datas = null;
		if(events != null) {
			datas = new ArrayList<Map<String, Object>>(events.size());
			for(ServiceEvent event : events) {
				data = new HashMap<String, Object>();
				data.put("id", event.id());
				data.put("time", DateUtils.getYYYYMMDDHHMMSS(event.createdAt().toEpochMilli()));
				data.put("message",event.message());
				datas.add(data);
			}
		}
		
		return datas;
	}
	
	public Map<String, Object> scaleService(Map<String, Object> input) throws Exception{
		Map<String, Object> condition =  makeConditionStageAndService(input);
		condition.put("scale", (int)input.get("scale"));
		Service service = new EcsApi("E", condition).scaleService(condition);
		if(service == null) {
			return null;
		}
		Map<String, Object> reService = this.getService(condition);
		return reService;
	}
	
	public Map<String, Object> getTaskDefinitionSimple(String definition, EcsApi ecsApi) throws Exception{
		TaskDefinition taskDefinition = ecsApi.getTaskDefinition(definition);
		Map<String, Object> reTaskDefinition = null;
		if(taskDefinition != null) {
			reTaskDefinition = AwsEcsDataConverter.convertTaskDefinitionToMap(taskDefinition);
			reTaskDefinition.put("taskDefinition", definition);
		}
		return reTaskDefinition;
	}

	@Override
	public List<Map<String, Object>> stopTask(Map<String, Object> input) throws Exception {
		Map<String, Object> condition = makeConditionStageAndService(input);
		condition.put("taskArn", input.get("taskArn"));	
		Task task = new EcsApi("E", condition).stopTask(condition);
		if(task == null) {
			return null;
		}
		
		return this.getTasks(condition);
	}
	
	@Override
	public List<Map<String, Object>> getTargetInstancesByTargetGroup(Map<String, Object> input) throws Exception {
		Map<String, Object> condition =  makeConditionStageAndService(input);
		ElbApi elbApi = new ElbApi("E", condition);
		
		List<Map<String, Object>> targetInstances = null;
		List<TargetHealthDescription> healths = elbApi.getTargetGroupHealth((String)input.get("targetGroupArn"));
		if(healths != null && healths.size() > 0) {
			targetInstances = new ArrayList<Map<String,Object>>(healths.size());
			for(TargetHealthDescription health : healths) {
				targetInstances.add(AwsEcsDataConverter.convertTargetHealthDescToMap(health));
			}
		}
		return targetInstances;		
	}

	@Override
	public Map<String, Object> getSimpleStats(Map<String, Object> input) throws Exception {
		Map<String, Object> condition = makeConditionStageAndService(input);
		
		EcsApi ecsApi = new EcsApi("E", condition);
		Service service = ecsApi.getService(condition);
		if(service == null) {
			return null;
		}
		
		Map<String, Object> stats = new HashMap<String,Object>();
		stats.put("desiredCount", service.desiredCount());
		stats.put("runningCount", service.runningCount());
		stats.put("pendingCount", service.runningCount());
		if(service.deployments() == null || service.deployments().size() == 0) {
			stats.put("deployCount", service.taskSets().size());			
		}else {
			stats.put("deployCount", service.deployments().size());			
		}
		
		return stats;
	}
	
	public Map<String, Object> getTargetGroupByLoadBalancer(Map<String, Object> input, List<Map<String, Object>> containers) throws Exception {
		Map<String, Object> condition = makeConditionStageAndService(input);
		
		ElbApi elbApi =  new ElbApi("E", condition);
		software.amazon.awssdk.services.elasticloadbalancingv2.model.LoadBalancer elb = elbApi.getLoadBalancers((String)this.getConfigValue("loadBalancerName"));
		if(elb == null) {
			return null;
		}
		
		Map<String, Object> loadBalancer = AwsElbDataConverter.convertLoadBalanerToMap(elb); 
		
		List<software.amazon.awssdk.services.elasticloadbalancingv2.model.Listener> listeners = elbApi.getListeners(elb.loadBalancerArn());
		if(listeners != null) {
			List<Map<String, Object>> lsnrs = new ArrayList<Map<String, Object>>(listeners.size());
			Map<String, Object> lsnr;
			for(software.amazon.awssdk.services.elasticloadbalancingv2.model.Listener listener : listeners) {
				lsnr = AwsElbDataConverter.convertListenerToMap(listener, this.getCicdConfigs());
				
				List<Rule> rules = elbApi.getRules((String)lsnr.get("listenerArn"));
				List<software.amazon.awssdk.services.elasticloadbalancingv2.model.Action> actions;

				List<Map<String, Object>> datas = new ArrayList<Map<String, Object>>();
				Map<String, Object> data;
				for(Rule rule : rules) {
					actions = rule.actions();
					for(software.amazon.awssdk.services.elasticloadbalancingv2.model.Action action: actions) {
						data = new HashMap<String, Object>(6);
						data.put("order", action.order());
						data.put("targetGroupArn", action.targetGroupArn());
						data.put("type", action.typeAsString());
			
						software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetGroup group = elbApi.getTargetGroup(action.targetGroupArn());
						if(group != null) {
							AwsElbDataConverter.convertTargetGroupToMap(group, data, containers, action.targetGroupArn());
						}
						
						List<TargetHealthDescription> healths = elbApi.getTargetGroupHealth(action.targetGroupArn());
						if(healths != null && healths.size() > 0) {
							List<Map<String, Object>> hDatas = new ArrayList<Map<String,Object>>(healths.size());
							for(TargetHealthDescription health : healths) {
								hDatas.add(AwsEcsDataConverter.convertTargetHealthDescToMap(health));
							}
							data.put("targetInstances", hDatas);
						}
						datas.add(data);
					}
				} 
				lsnr.put("actions", datas);				
				lsnrs.add(lsnr);					
			}
			loadBalancer.put("listeners",  lsnrs);			 
		}
		return loadBalancer;
	}

	
	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> getInstanceUsage(Map<String, Object> condition, List<Map<String, Object>> tasks){
		Map<String, Object> instance, stats;
		List<Map<String, Object>> reStats = new ArrayList<Map<String,Object>>(tasks.size());
		
		CloudWatchApi cwApi =  new CloudWatchApi("E", condition);
		
		String id;
		condition.put("dimensionName", "instanceId");
		for(Map<String,Object> task : tasks){
			instance = (Map<String,Object>) task.get("ec2Instance");
			if(instance == null) {
				continue;
			}
			id = (String)instance.get("ec2InstanceId");
			condition.put("instanceId", id);	
			stats = new HashMap<String, Object>(10);
			stats.put("instanceId", id);
			stats.put("containerStatus", task.get("status"));
			stats.put("ec2Status", instance.get("status"));
			stats.put("cpuCores", instance.get("cpuCores"));
			stats.put("privateIp", instance.get("privateIp"));
			stats.put("publicIp", instance.get("publicIp"));
			if(instance.get("tags") != null) {
				stats.put("name", ((Map<String, Object>)(instance.get("tags"))).get("Name"));
			}
			stats.put("stats", cwApi.getMetricDatas(condition));
			reStats.add(stats);
		}
		
		return reStats;		
	}
	
	@Override
	public List<Map<String, Object>> getInstancesStats(Map<String, Object> input) throws Exception {
		List<Map<String, Object>> tasks = this.getTasks(input);
		
		Map<String, Object> condition = makeConditionStageAndService(input);
		List<String> metricNames = new ArrayList<String>(6);		
		metricNames.add("cpuUsage");
		metricNames.add("diskReadOps");
		metricNames.add("diskWriteOps");
		metricNames.add("diskReadBytes");
		metricNames.add("diskWriteBytes");
		metricNames.add("networkInPkts");
		metricNames.add("networkOutPkts");
		metricNames.add("networkInBytes");
		metricNames.add("networkOutBytes");
		metricNames.add("healthFails");
		condition.put("metricNames", metricNames);
		
		condition.put("endTime", System.currentTimeMillis());
		condition.put("duration", 500000L);
		condition.put("maxDataPoints", 1);
		
		return getInstanceUsage(condition, tasks);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> getInstancesUsage(Map<String, Object> input) throws Exception {
		List<Map<String, Object>> tasks = this.getTasks(input);
	
		Map<String, Object> condition = makeConditionStageAndService(input);
		condition.put("metricNames", (List<String>)input.get("metrics"));
		AwsUtils.setStatsTime(input, condition);
		
		return getInstanceUsage(condition, tasks);		
	}
	
	private List<Map<String, Object>> getLoadBalanceUsage(Map<String, Object> condition, Map<String, String> targetGroups){
		CloudWatchApi cwApi =  new CloudWatchApi("E", condition);
		condition.put("dimensionName", "loadBalancer,targetGroup");
		String servicePort = (String)this.getConfigValue("loadBalancerServicePort");		
		List<Map<String, Object>> reStats = new ArrayList<Map<String, Object>>();

		String port;
		String targetGroup;
		Iterator<String> itor = targetGroups.keySet().iterator();
		while(itor.hasNext()) {
			port = itor.next();
			if("loadBalancer".equals(port)) {
				continue;
			}
		
			Map<String, Object> stats = new HashMap<String, Object>(10);
			stats.put("name", (String)condition.get("loadBalancer"));
			stats.put("port", port);
			condition.put("port", port);
			if(port.equals(servicePort)) {
				stats.put("portType", "Service");
			}else {
				stats.put("portType", "Test");
			}
			targetGroup = targetGroups.get(port);
			stats.put("targetGroup", targetGroup);
			condition.put("targetGroup", targetGroup);	
			
			stats.put("stats", cwApi.getMetricDatas(condition));
			reStats.add(stats);
		} 
				
		return reStats;		
	}
	
	
	@Override
	public List<Map<String, Object>> getLoadBalancesStats(Map<String, Object> input) throws Exception {
		Map<String, Object> condition = makeConditionStageAndService(input);
		List<String> metricNames = new ArrayList<String>(6);		
		metricNames.add("activeConnections");
		metricNames.add("clientTLSErrors");
		metricNames.add("http5xxs");
		metricNames.add("requests");
		metricNames.add("processedBytes");
		metricNames.add("rejectedConnections");
		condition.put("metricNames", metricNames);
		
		condition.put("endTime", System.currentTimeMillis());
		condition.put("duration", 500000L);
		condition.put("maxDataPoints", 1);
		
		
		ElbApi elbApi =  new ElbApi("E", condition);
		Map<String, String> cloudWatchInfo = elbApi.getCoudWatchInfo((String)this.getConfigValue("loadBalancerName"));
		if(cloudWatchInfo == null || cloudWatchInfo.size() == 0) {
			return null;
		}
		condition.put("loadBalancer", cloudWatchInfo.get("loadBalancer"));	
		
		return getLoadBalanceUsage(condition, cloudWatchInfo);
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> getLoadBalancesUsage(Map<String, Object> input) throws Exception {
		Map<String, Object> condition = makeConditionStageAndService(input);		
		
		condition.put("loadBalancer", (String)this.getConfigValue("loadBalancerName"));	
		condition.put("metricNames", (List<String>)input.get("metrics"));
		AwsUtils.setStatsTime(input, condition);

		
		ElbApi elbApi =  new ElbApi("E", condition);
		Map<String, String> targetGroups = elbApi.getTargetsGroupByLoadBalancer((String)condition.get("loadBalancer"));
		if(targetGroups == null || targetGroups.size() == 0) {
			return null;
		}
		
		return getLoadBalanceUsage(condition, targetGroups);
	}	

	@Override
	public Map<String, Object> replaceImage(Map<String, Object> input) throws Exception {
		String newImageName = AwsUtils.getCurrentImageNameHeader(input) + AwsUtils.getTargetImageName(input);
		
		Map<String, Object> condition = makeConditionStageAndService(input);
		EcsApi ecsApi =  new EcsApi("E", condition);
		
		TaskDefinition taskDefinition = ecsApi.getTaskDefinition((String)input.get("taskDefinitionArn"));
		if(taskDefinition == null || taskDefinition.taskDefinitionArn() == null) {
			throw new RuntimeException("TaskDefinition do not exist!(" + (String)input.get("taskDefinitionArn") + ")");
		}
		
		TaskDefinition newTaskDefinition = ecsApi.updateTaskDefinitionVersionWithImage(taskDefinition, newImageName, null);
		if(newTaskDefinition == null || newTaskDefinition.taskDefinitionArn() == null) {
			throw new RuntimeException("Failed to create a new taskdefinition");
		}
		condition.put("taskDefinitionArn", newTaskDefinition.taskDefinitionArn());
		
		String deploy = (String)input.get("deploy");

		Map<String, Object> reValue = null;
		switch(deploy) {
		case Constants.RESOURCE_DEPLOY_RESTART:
			Service service = ecsApi.restartService(condition);
			reValue = AwsEcsDataConverter.convertEcsServiceToMap(service);
			break;
		case Constants.RESOURCE_DEPLOY_ROLLING:
			break;
		case Constants.RESOURCE_DEPLOY_BLUEGREEN:
			InterfaceFactory factory = (InterfaceFactory)DevonCicdApplication.applicationContext.getBean(InterfaceFactory.class);
			DeploymentJob deploymentJob = factory.getDeployment(input);
			reValue = deploymentJob.createDeployment(input);
			break;
		case Constants.RESOURCE_DEPLOY_CANARY:
			break;
		default:
			throw new Exception("Not supperted deployment(" + deploy + ")");
		}
		return reValue;
	}

	@Override
	public Map<String, Object> isRunningDeployment(Map<String, Object> input) throws Exception {
		Map<String, Object> condition = makeConditionStageAndService(input);
		
		EcsApi ecsApi =  new EcsApi("E", condition);
		Service service = ecsApi.getService(condition);
		Map<String, Object> reStatus = new HashMap<String, Object>(1);
		if(service == null) {
			reStatus.put("status", Constants.RESOURCE_STATUS_COMPLETED);
			return reStatus;
		}
		
		int deploysCount = 0;
		if(service.deployments() != null && service.deployments().size() > 0) {
			deploysCount = service.deployments().size();
		}else if("CODE_DEPLOY".equals(service.deploymentController().typeAsString())){
			if(service.taskSets() != null && service.taskSets().size() > 0) {
				deploysCount = service.taskSets().size();
			}
		}
		
		if(deploysCount > 1) {
			reStatus.put("status", Constants.RESOURCE_STATUS_RUNNING);
		}else {
			reStatus.put("status", Constants.RESOURCE_STATUS_COMPLETED);			
		}
		
		return reStatus;
	}
}
