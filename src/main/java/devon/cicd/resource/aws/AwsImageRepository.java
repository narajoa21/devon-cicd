package devon.cicd.resource.aws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import devon.cicd.DevonCicdApplication;
import devon.cicd.config.CicdConfig;
import devon.cicd.config.CicdConfigManager;
import devon.cicd.resource.ImageRepository;
import devon.cicd.resource.aws.api.EcrApi;
import devon.cicd.util.CollectionUtils;
import devon.cicd.util.DateUtils;
import software.amazon.awssdk.services.ecr.model.ImageDetail;

public class AwsImageRepository implements ImageRepository{
	private CicdConfigManager cicdConfigManager;
	CicdConfig cicdConfig;
	
	public AwsImageRepository(String stageCode) {
		cicdConfigManager = DevonCicdApplication.applicationContext.getBean(CicdConfigManager.class);
		cicdConfig = cicdConfigManager.readCicdConfigBySubSystemId("R", stageCode);
	}
	
	private Object getConfigValue(String key){
		if(cicdConfig == null) {
			return null;
		}
		return cicdConfig.getConfigs().get(key);
	}
	
	private Map<String, Object> makeApiMap(Map<String, Object> input){
		Map<String, Object> condition = new HashMap<String, Object>(5);
		String region = (String)getConfigValue("region");
		if(region == null) {
			region = (String)input.get("region");
		}
		condition.put("region", region);
		condition.put("stageCode", (String)input.get("stageCode"));
		return condition;
	}
	
	@Override
	public List<Map<String, Object>> getImages(Map<String, Object> input) throws Exception {
		Map<String, Object> condition = makeApiMap(input);
		
		List<ImageDetail> images = new EcrApi("R", condition).listImages((String)getConfigValue("imageRepository"));
		
		if(images == null || images.size() == 0) {
			return null;
		}
		
		List<Map<String, Object>> reImages = new ArrayList<Map<String, Object>>(images.size());
		Map<String, Object> image;
		for(ImageDetail detail : images) {
			image = new HashMap<String, Object>();
			image.put("registryId", detail.registryId());
			if(detail.imageTags() == null || detail.imageTags().size() == 0) {
				image.put("tag", "None");
			}else {
				image.put("tag", detail.imageTags().toString());
			}
			image.put("imageRepository", detail.repositoryName());
			image.put("pushTime", DateUtils.getYYYYMMDDHHMMSS(detail.imagePushedAt().toEpochMilli()));
			image.put("size", detail.imageSizeInBytes()/1000000);
			image.put("digest", detail.imageDigest());
			reImages.add(image);
		}
		
		reImages = CollectionUtils.filter("tag",(String)input.get("branchName"), false, reImages);
		reImages = CollectionUtils.sort("pushTime",false, reImages);
		
		return reImages;
	}
}
