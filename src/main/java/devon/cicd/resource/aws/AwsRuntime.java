package devon.cicd.resource.aws;

import java.util.HashMap;
import java.util.Map;

import devon.cicd.DevonCicdApplication;
import devon.cicd.config.CicdConfig;
import devon.cicd.config.CicdConfigManager;
import devon.cicd.resource.Runtime;

public abstract class AwsRuntime implements Runtime{
	protected CicdConfigManager cicdConfigManager;
	protected CicdConfig cicdConfig;
	
	protected AwsRuntime(String stageCode, String cicdStep) {
		cicdConfigManager = DevonCicdApplication.applicationContext.getBean(CicdConfigManager.class);
		cicdConfig = cicdConfigManager.readCicdConfigBySubSystemId(cicdStep, stageCode);
	}
	
	protected Map<String, Object> getCicdConfigs(){
		if(cicdConfig == null) {
			return null;
		}
		return cicdConfig.getConfigs();		
	}
	
	protected Object getConfigValue(String key){
		if(cicdConfig == null) {
			return null;
		}
		return cicdConfig.getConfigs().get(key);
	}
	
	protected Map<String, Object> makeConditionStage(Map<String, Object> input){
		Map<String, Object> condition = new HashMap<String, Object>(6);
		String region = (String)getConfigValue("region");
		if(region == null) {
			region = (String)input.get("region");
		}
		condition.put("region", region);
		condition.put("stageCode", (String)input.get("stageCode"));	
		return condition;
	}
}
