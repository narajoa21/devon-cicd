package devon.cicd.resource.aws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import devon.cicd.DevonCicdApplication;
import devon.cicd.config.CicdConfig;
import devon.cicd.config.CicdConfigManager;
import devon.cicd.config.Constants;
import devon.cicd.resource.DeploymentJob;
import devon.cicd.resource.aws.api.CodeDeployApi;
import software.amazon.awssdk.services.codedeploy.model.CodeDeployResponseMetadata;
import software.amazon.awssdk.services.codedeploy.model.DeploymentInfo;

public class AwsDeploymentJob implements DeploymentJob {
	protected CicdConfigManager cicdConfigManager;
	protected CicdConfig cicdConfig;
	
	public AwsDeploymentJob(String stageCode) {
		cicdConfigManager = DevonCicdApplication.applicationContext.getBean(CicdConfigManager.class);
		cicdConfig = cicdConfigManager.readCicdConfigBySubSystemId("D", stageCode);
	}	
	
	private Map<String, Object> makeConditionDeployApplicationAndGroup(Map<String, Object> input){
		Map<String, Object> condition = new HashMap<String, Object>(6);
		String region = (String)cicdConfig.getConfigs().get("region");
		if(region == null) {
			region = (String)input.get("region");
		}
		condition.put("region", region);
		condition.put("application", (String)cicdConfig.getConfigs().get("application"));	
		condition.put("deploymentGroup", (String)cicdConfig.getConfigs().get("deploymentGroup"));	
		return condition;
	}
	
	@Override
	public Map<String, Object> cancelDeployment(Map<String, Object> input) throws Exception {
		CodeDeployApi cdApi = new CodeDeployApi("D", input);
		Map<String, Object> condition = makeConditionDeployApplicationAndGroup(input);

		String deploymentId = getRunningDeploymentId(cdApi, condition);
		condition.put("deploymentId", deploymentId);
		
		String status = cdApi.stopDeployment(condition);
		
		Map<String, Object> reMap = new HashMap<String, Object>();
		reMap.put("status", status);
		return reMap;
	}

	@Override
	public Map<String, Object> changeService(Map<String, Object> input) throws Exception {
		CodeDeployApi cdApi = new CodeDeployApi("D", input);
		Map<String, Object> condition = makeConditionDeployApplicationAndGroup(input);

		String deploymentId = getRunningDeploymentId(cdApi, condition);
		condition.put("deploymentId", deploymentId);
		
		CodeDeployResponseMetadata metaData = cdApi.continueDeployment(condition);
		return null;
	}

	@Override
	public String getRunningDeploymentId(Map<String, Object> input) throws Exception {
		CodeDeployApi cdApi = new CodeDeployApi("D", input);
		Map<String, Object> condition = makeConditionDeployApplicationAndGroup(input);

		return cdApi.getRunningDeployment(condition);
	}
	
	private String getRunningDeploymentId(CodeDeployApi cdApi, Map<String, Object> input) throws Exception {
		String deployment = cdApi.getRunningDeployment(input);
		if(deployment == null) {
			throw new RuntimeException("The currently running deployment does not exist." + input);
		}
		return deployment;
	}

	@Override
	public List<Map<String, Object>> getDeployments(Map<String, Object> input) throws Exception {
		CodeDeployApi cdApi = new CodeDeployApi("D", input);
		Map<String, Object> condition = makeConditionDeployApplicationAndGroup(input);

		condition.put("endTime", System.currentTimeMillis());
		condition.put("duration", (long)(Constants.LIST_SELECT_DAYS * 86400000)); // 7일
		
		List<String> deployments = cdApi.getDeployments(condition);
		if(deployments == null || deployments.size() == 0) {
			return null;
		}
		
		DeploymentInfo info;
		List<Map<String, Object>> reDeployments = new ArrayList<Map<String, Object>>(deployments.size());
		for(String deploymentId: deployments) {
			info = cdApi.getDeployment(deploymentId);
			reDeployments.add(AwsCdpDataConverter.convertDeploymentInfoToMap(info));
		}
		return reDeployments;
	}

	@Override
	public Map<String, Object> getDeployment(Map<String, Object> input) throws Exception {
		CodeDeployApi cdApi = new CodeDeployApi("D", input);
		DeploymentInfo info = cdApi.getDeployment((String)input.get("deploymentId"));
		return AwsCdpDataConverter.convertDeploymentInfoToMap(info);
	}

	@Override
	public Map<String, Object> createDeployment(Map<String, Object> input) throws Exception {
		CodeDeployApi cdApi = new CodeDeployApi("D", input);
		Map<String, Object> condition = makeConditionDeployApplicationAndGroup(input);
		String deploymentId = cdApi.createDeployment(condition);
		if(deploymentId == null) {
			throw new RuntimeException("Fail to create a deployment");
		}
		condition.put("deploymentId", deploymentId);
		return getDeployment(condition);
	}	
}
