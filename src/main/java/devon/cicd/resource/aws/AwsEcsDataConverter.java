package devon.cicd.resource.aws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import devon.cicd.util.DateUtils;
import software.amazon.awssdk.services.ecs.model.Attribute;
import software.amazon.awssdk.services.ecs.model.Container;
import software.amazon.awssdk.services.ecs.model.ContainerDefinition;
import software.amazon.awssdk.services.ecs.model.ContainerInstance;
import software.amazon.awssdk.services.ecs.model.Deployment;
import software.amazon.awssdk.services.ecs.model.LoadBalancer;
import software.amazon.awssdk.services.ecs.model.LogConfiguration;
import software.amazon.awssdk.services.ecs.model.NetworkBinding;
import software.amazon.awssdk.services.ecs.model.PortMapping;
import software.amazon.awssdk.services.ecs.model.Resource;
import software.amazon.awssdk.services.ecs.model.Service;
import software.amazon.awssdk.services.ecs.model.Task;
import software.amazon.awssdk.services.ecs.model.TaskDefinition;
import software.amazon.awssdk.services.ecs.model.TaskSet;
import software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetHealthDescription;

public class AwsEcsDataConverter {	

	@SuppressWarnings("unchecked")
	static public Map<String, Object> convertEcsServiceToMap(Service service) throws Exception {
		Map<String, Object> reService =  new HashMap<String, Object>();
		reService.put("createTime", DateUtils.getYYYYMMDDHHMMSS(service.createdAt().toEpochMilli()));
		reService.put("serviceName", service.serviceName());
		reService.put("serviceArn",service.serviceArn());
		reService.put("clusterArn",service.clusterArn());
		reService.put("roleArn",service.roleArn());
		reService.put("status", service.status());
		reService.put("runningCount", service.runningCount());
		reService.put("pendingCount", service.pendingCount());
		reService.put("schedulingStrategy", service.schedulingStrategyAsString());
		if(service.taskDefinition() != null) {
			reService.put("taskDefinition", service.taskDefinition());			
		}
		reService.put("desiredCount", service.desiredCount());

		List<Deployment> deploys = service.deployments();
		List<Map<String, Object>> datas = null;
		Map<String, Object> data;
		if(deploys != null && deploys.size() > 0) {
			datas = new ArrayList<Map<String, Object>>(deploys.size());
			for(Deployment deploy : deploys) {
				data = new HashMap<String, Object>();
				data.put("id", deploy.id());
				data.put("createTime", DateUtils.getYYYYMMDDHHMMSS(deploy.createdAt().toEpochMilli()));
				data.put("desiredCount", deploy.desiredCount());
				data.put("launchType", deploy.launchType().toString());
				data.put("runningCount", deploy.runningCount());
				data.put("pendingCount", deploy.pendingCount());
				data.put("status", deploy.status());
				if(deploy.taskDefinition() != null) {
					data.put("taskDefinition", deploy.taskDefinition());
				}
				datas.add(data);
			}
		}else if("CODE_DEPLOY".equals(service.deploymentController().typeAsString())){
			List<TaskSet> taskSets = service.taskSets();
			if(taskSets != null && taskSets.size() > 0) {
				datas = new ArrayList<Map<String, Object>>(deploys.size());
				for(TaskSet taskSet : taskSets) {
					data = new HashMap<String, Object>();
					data.put("id",taskSet.id());
					data.put("createTime", DateUtils.getYYYYMMDDHHMMSS(taskSet.createdAt().toEpochMilli()));
					data.put("pendingCount", taskSet.pendingCount());
					//data.put("desiredCount", 0);
					data.put("runningCount", taskSet.runningCount());
					data.put("startBy", taskSet.startedBy());
					data.put("launchType", taskSet.launchType().toString());
					data.put("status", taskSet.status());
					data.put("stabilityStatus", taskSet.stabilityStatusAsString());
					data.put("taskSetArn", taskSet.taskDefinition());
					if(taskSet.taskDefinition() != null) {
						data.put("taskDefinition", taskSet.taskDefinition());
					}					
					if(taskSet.loadBalancers() != null && taskSet.loadBalancers().size() > 0) {
						data.put("targetGroupArn", taskSet.loadBalancers().get(0).targetGroupArn());
						data.put("containerName", taskSet.loadBalancers().get(0).containerName());
					}
					datas.add(data);
				}
			}
		}
		reService.put("deploys", datas);
		if(service.deploymentController() != null){
			reService.put("deploymentController", service.deploymentController().typeAsString());
		}else {
			reService.put("deploymentController", "Unknown");			
		}

		List<LoadBalancer> balancers = service.loadBalancers();
		datas = null;
		if(balancers != null) {
			datas = new ArrayList<Map<String, Object>>(balancers.size());
			for(LoadBalancer  balancer : balancers) {
				data = new HashMap<String, Object>();
				data.put("loadBalancerName", balancer.loadBalancerName());
				data.put("containerName", balancer.containerName());
				data.put("containerPort", balancer.containerPort());
				data.put("targetGroupArn",balancer.targetGroupArn());
				datas.add(data);
			}
		}
		reService.put("containers", datas);
		
		if(reService.get("desiredCount") != null && (int)reService.get("desiredCount") > 0) {
			datas  = (List<Map<String, Object>>)reService.get("deploys");
			if(datas != null) {
				for(Map<String, Object> deployData :datas) {
					if(deployData.get("desiredCount") == null) {
						deployData.put("desiredCount", reService.get("desiredCount"));
					}
				}
			}
		}

		return reService;
	}
	
	static public Map<String, Object> convertEcsTaskToMap(Task task) {
		Map<String, Object> reTask =  new HashMap<String, Object>();
		
		reTask.put("taskArn", task.taskArn());
		reTask.put("taskDefinitionArn", task.taskDefinitionArn());
		reTask.put("containerInstanceArn", task.containerInstanceArn());
		reTask.put("createTime", DateUtils.getYYYYMMDDHHMMSS(task.createdAt().toEpochMilli()));
		reTask.put("cpu", task.cpu());
		reTask.put("memory", task.memory());
		reTask.put("launchType", task.launchTypeAsString());
		reTask.put("status", task.lastStatus());
		reTask.put("healthStatus", task.healthStatus());
		reTask.put("desiredStatus", task.desiredStatus());
		reTask.put("startedBy", task.startedBy());

		if(task.pullStartedAt() != null) {
			reTask.put("pullTime", DateUtils.getYYYYMMDDHHMMSS(task.pullStartedAt().toEpochMilli()));
		}
		if(task.startedAt() != null) {
			reTask.put("startTime", DateUtils.getYYYYMMDDHHMMSS(task.startedAt().toEpochMilli()));
		}
		if(task.stoppingAt() != null) {
			reTask.put("stoppingTime", DateUtils.getYYYYMMDDHHMMSS(task.stoppingAt().toEpochMilli()));
		}
		if(task.stoppedAt() != null) {
			reTask.put("stoppedTime", DateUtils.getYYYYMMDDHHMMSS(task.stoppedAt().toEpochMilli()));
			reTask.put("stoppedReason", task.stoppedReason());
		}

		List<Container> containers = task.containers();
		List<Map<String, Object>> datas = null;
		Map<String, Object> data;
		if(containers != null) {
			datas = new ArrayList<Map<String, Object>>(containers.size());
			for(Container container : containers) {
				data = new HashMap<String, Object>();
				data.put("name", container.name());
				data.put("containerArn", container.containerArn());
				data.put("cpu", container.cpu());
				data.put("memory", container.memory());
				data.put("helthStatus", container.healthStatusAsString());
				data.put("status", container.lastStatus());
				data.put("reason", container.reason());
				
				List<NetworkBinding> networkBinds = container.networkBindings();
				List<Map<String, Object>> binds = null;
				if(networkBinds != null) {
					binds = new ArrayList<Map<String, Object>>(networkBinds.size());
					Map<String, Object> bind;
					for(NetworkBinding networkBind : networkBinds) {
						bind = new HashMap<String, Object>();
						bind.put("bindIp", networkBind.bindIP());
						bind.put("containerPort", networkBind.containerPort());
						bind.put("hostPort", networkBind.hostPort());
						bind.put("protocol", networkBind.protocol());
						binds.add(bind);
					}
				}
				data.put("networkBinds", binds);
				datas.add(data);
			}
		}
		reTask.put("containers", datas);
		return reTask;
	}
	
	static private Map<String, Object> convertResourceToMap(List<Resource> resources){
		Map<String, Object> rsrcs = new  HashMap<String, Object>(5);
		for(Resource resource: resources) {
			if(resource.integerValue() != 0) {
				rsrcs.put(resource.name(), resource.integerValue());
			}else if(resource.doubleValue() != 0D) {
				rsrcs.put(resource.name(), resource.doubleValue());
			}else if(resource.longValue() != 0L) {
				rsrcs.put(resource.name(), resource.longValue());
			}else {
				rsrcs.put(resource.name(), 0);
			}
		}
		return rsrcs;
	}
	
	static public Map<String, Object> convertEcsContainerToMap(ContainerInstance container){
		Map<String, Object> reCon = new HashMap<String, Object>();
		reCon.put("agentConnected", container.agentConnected());
		List<Attribute> attributes = container.attributes();
		if(attributes != null) {
			Map<String, String> attrbs = new HashMap<String, String>();
			for(Attribute attr : attributes) {
				if(attr.value() != null) {
					attrbs.put(attr.name(), attr.value());
				}
			}
			if(attrbs.size() > 0) {
				reCon.put("attributes", attrbs);
			}
		}
		reCon.put("containerInstanceArn", container.containerInstanceArn());
		reCon.put("ec2InstanceId",container.ec2InstanceId());
		
		List<Resource> resources = container.registeredResources();
		if(resources!= null) {
			reCon.put("registeredResources", convertResourceToMap(resources));			
		}
		
		resources = container.remainingResources();
		if(resources != null) {
			reCon.put("remainingResources", convertResourceToMap(resources));			
		}
		reCon.put("status", container.status());
		reCon.put("runningTaskCount", container.runningTasksCount());
		reCon.put("version", container.version());
		reCon.put("agentVersion", container.versionInfo().agentVersion());
		reCon.put("dockerVersion", container.versionInfo().dockerVersion());

		return reCon;
	}

	static public Map<String, Object> convertTaskDefinitionToMap(TaskDefinition definition){
		 Map<String, Object> reDef = new HashMap<String, Object>();
		 
		 reDef.put("family", definition.family());
		 reDef.put("netowrkMode", definition.networkModeAsString());
		 reDef.put("revision", definition.revision());
		 reDef.put("status", definition.statusAsString());
		 reDef.put("family", definition.family());
		 reDef.put("taskDefinitionArn", definition.taskDefinitionArn());
		 
		 List<ContainerDefinition> conDefs = definition.containerDefinitions();
		 if(conDefs != null && conDefs.size() > 0) {
			 List<Map<String, Object>> defs = new ArrayList<Map<String, Object>>(conDefs.size());
			 for(ContainerDefinition def : conDefs) {
				 defs.add(convertContainerDefinitionToMap(def));
			 }
			 reDef.put("containerDefinitions", defs); 
		 }
		 return reDef;
	}
	
	static public Map<String, Object> convertContainerDefinitionToMap(ContainerDefinition definition){
		 Map<String, Object> reDef = new HashMap<String, Object>();
		 reDef.put("image", definition.image());
		 reDef.put("name", definition.name());
		 
		 List<Map<String, Object>> datas;
		 List<PortMapping> portMappings = definition.portMappings();
		 if(portMappings != null && portMappings.size() > 0) {
			 datas = new ArrayList<Map<String, Object>>(portMappings.size());
			Map<String, Object> data;
			for(PortMapping port : portMappings) {
				data = new HashMap<String, Object>();
				data.put("containerPort", port.containerPort());
				data.put("hostPort", port.hostPort());
				data.put("protocol", port.protocolAsString());
				datas.add(data);
			}
			reDef.put("portMappings", datas);
		 }
		 
		 LogConfiguration logConfig = definition.logConfiguration();
		 if(logConfig != null) {
			 reDef.put("logDriver", logConfig.logDriverAsString());
			 reDef.put("options", logConfig.options());
		 }
		 
		 return reDef;
	}
	
	static Map<String, Object> convertTargetHealthDescToMap(TargetHealthDescription health){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ec2InstanceId", health.target().id());
		map.put("port", health.target().port());
		map.put("state", health.targetHealth().stateAsString());
		return map;
	}
}
