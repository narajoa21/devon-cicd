package devon.cicd.resource.aws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AwsElbDataConverter {
	
	static public Map<String, Object> convertLoadBalanerToMap(software.amazon.awssdk.services.elasticloadbalancingv2.model.LoadBalancer elb){
		 Map<String, Object> reLoad = new HashMap<String, Object>();
		 
		 reLoad.put("loadBalancerName", elb.loadBalancerName());
		 reLoad.put("loadBalancerArn", elb.loadBalancerArn());
		 reLoad.put("dnsName", elb.dnsName());
		 reLoad.put("ipAddressType", elb.ipAddressTypeAsString());
		 reLoad.put("type", elb.type());
		 reLoad.put("status", elb.state().code());
		 reLoad.put("reason", elb.state().reason());
		 
		 return reLoad;
	}
	
	static public Map<String, Object> convertListenerToMap(software.amazon.awssdk.services.elasticloadbalancingv2.model.Listener listener, Map<String, Object> configs){
		 Map<String, Object> reLsnr = new HashMap<String, Object>();
		 
		 reLsnr.put("port", listener.port());
		 reLsnr.put("protocol", listener.protocolAsString());
		 reLsnr.put("loadBalancerArn", listener.loadBalancerArn());
		 reLsnr.put("listenerArn", listener.listenerArn());
		 reLsnr.put("runType", getListenerRunType(configs, listener.port()));
		 return reLsnr;
	}
	
	static public void convertTargetGroupToMap(software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetGroup group, Map<String, Object> data, List<Map<String, Object>> containers, String actionTargetGroup) {
		data.put("targetGroupName", group.targetGroupName());
		data.put("targetType", group.targetTypeAsString());
		data.put("healthCheckEnabled", group.healthCheckEnabled());			 
		data.put("healthCheckPath", group.healthCheckPath()); 
		data.put("healthCheckIntervalSeconds", group.healthCheckIntervalSeconds());
		data.put("healthCheckThresholdCount", group.healthyThresholdCount());
		data.put("port", group.port());
		data.put("protocol", group.protocolAsString());
		if(containers != null) {
			for(Map<String, Object> container: containers) {
				if(actionTargetGroup.equals((String)container.get("targetGroupArn"))) {
					data.put("containerName", container.get("containerName"));
					data.put("containerPort", container.get("containerPort"));
					break;
				}
			}
		}		
	}
	
	static private String getListenerRunType(Map<String, Object>configs, int port) {
		String sPort = ""+ port;
		String servicePort = (String)configs.get("loadBalancerServicePort");
		if(servicePort != null && sPort.equals(servicePort)) {
			return "service";
		}
		
		String testPort = (String)configs.get("loadBalancerTestPort");
		if(testPort != null && sPort.equals(testPort)) {
			return "test";
		}
		return "unkown";
	}
}
