package devon.cicd.resource.aws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.InstanceBlockDeviceMapping;
import software.amazon.awssdk.services.ec2.model.Tag;

public class AwsEc2DataConverter {
	static public Map<String, Object> convertEc2InstanceToMap(Instance instance){
		Map<String, Object> reIns = new HashMap<String, Object>();
		
		reIns.put("architecture", instance.architecture());

		List<InstanceBlockDeviceMapping> blocks = instance.blockDeviceMappings();
		if(blocks != null) {
			Map<String, String> blks = new HashMap<String, String>();
			for(InstanceBlockDeviceMapping block : blocks) {
				blks.put("deviceName",block.deviceName());
				blks.put("ebsVolumnId",block.ebs().volumeId());
			}
			if(blks.size() > 0) {
				reIns.put("blockDevices", blks);
			}
		}
		reIns.put("clientToken", instance.clientToken());
		reIns.put("ec2InstanceId",instance.instanceId());
		reIns.put("cpuCores", instance.cpuOptions().coreCount());
		reIns.put("cpuThreads",instance.cpuOptions().threadsPerCore());
		reIns.put("hypervisor",instance.hypervisor());
		reIns.put("instanceType", instance.instanceTypeAsString());
		reIns.put("keyName", instance.keyName());
		reIns.put("monitoring", instance.monitoring().stateAsString());
		reIns.put("privateDnsName", instance.privateDnsName());
		reIns.put("privateIp", instance.privateIpAddress());
		reIns.put("publicIp", instance.publicIpAddress());
		reIns.put("rootBlockDevice", instance.rootDeviceName());
		reIns.put("status", instance.state().nameAsString());
		reIns.put("vpcId", instance.vpcId());
		reIns.put("subnetId", instance.subnetId());
		reIns.put("statusReason", instance.stateTransitionReason());
		
		List<Tag> tags = instance.tags();
		if(tags!= null) {
			Map<String, Object> tgs = new  HashMap<String, Object>(5);
			for(Tag tag: tags) {
				tgs.put(tag.key(), tag.value());
			}
			reIns.put("tags", tgs);			
		}		
		return reIns;		
	}
}
