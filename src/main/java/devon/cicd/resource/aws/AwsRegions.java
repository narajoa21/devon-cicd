package devon.cicd.resource.aws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import devon.cicd.db.dao.CommonWrapper;
import devon.cicd.resource.CloudRegions;
import devon.cicd.DevonCicdApplication;

public class AwsRegions implements CloudRegions{
	@Override
	public List<Map<String, Object>> getRegions() {
		CommonWrapper commonWrapper = DevonCicdApplication.applicationContext.getBean(CommonWrapper.class);
		return commonWrapper.selectCodeLIst("201");
	}
	
	public Map<String, Object> getRegion(String region) {
		CommonWrapper commonWrapper = DevonCicdApplication.applicationContext.getBean(CommonWrapper.class);
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("division", "201");
		condition.put("code", region);
		
		return commonWrapper.selectCode(condition);
	}
}
