package devon.cicd.resource.aws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import software.amazon.awssdk.services.cloudwatch.model.StandardUnit;

public class AwsNames {
	private static Map<String, List<Object>> usageNames;
	
	static {
		usageNames = new HashMap<String,  List<Object>>();
		// ELB
		AwsUtils.makeUsageName(usageNames, "activeConnections", 	"ActiveConnectionCount",					StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "newConnections", 		"NewConnectionCount",						StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "consumedLCUs", 			"ConsumedLCUs",								StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "httpFixeds",	 		"HTTP_Fixed_Response_Count",				StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "httpRedirects", 		"HTTP_Redirect_Count",						StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "httpRedirectLimits", 	"HTTP_Redirect_Url_Limit_Exceeded_Count",	StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "http2xxs", 				"HTTPCode_Target_2XX_Count",				StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "http3xxs", 				"HTTPCode_Target_3XX_Count",				StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "http4xxs", 				"HTTPCode_Target_4XX_Count",				StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "http5xxs", 				"HTTPCode_Target_5XX_Count",				StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "ipV6ProcessedBytes", 	"IPv6ProcessedBytes",						StandardUnit.BYTES,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "ipV6Requests", 			"IPv6RequestCount",							StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "processedBytes", 		"ProcessedBytes",							StandardUnit.BYTES,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "requests", 				"RequestCount",								StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "rejectedConnections", 	"RejectedConnectionCount",					StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "httpUnhealths", 		"UnHealthyHostCount",						StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "clientTLSErrors", 		"ClientTLSNegotiationErrorCount",			StandardUnit.COUNT,	"AWS/ApplicationELB");
		AwsUtils.makeUsageName(usageNames, "targetTLSErrors", 		"TargetTLSNegotiationErrorCount",			StandardUnit.COUNT,	"AWS/ApplicationELB");
		
		//EC2
		AwsUtils.makeUsageName(usageNames, "cpuUsage", 			"CPUUtilization",		StandardUnit.PERCENT,	"AWS/EC2");
		AwsUtils.makeUsageName(usageNames, "diskReadOps",		"EBSReadOps",			StandardUnit.COUNT,		"AWS/EC2");
		AwsUtils.makeUsageName(usageNames, "diskWriteOps",		"EBSWriteOps",			StandardUnit.COUNT,		"AWS/EC2");
		AwsUtils.makeUsageName(usageNames, "diskReadBytes",		"EBSReadBytes",			StandardUnit.BYTES,		"AWS/EC2");
		AwsUtils.makeUsageName(usageNames, "diskWriteBytes",	"EBSWriteBytes",		StandardUnit.BYTES,		"AWS/EC2");
		AwsUtils.makeUsageName(usageNames, "networkInPkts",		"NetworkPacketsIn",		StandardUnit.COUNT,		"AWS/EC2");
		AwsUtils.makeUsageName(usageNames, "networkOutPkts",	"NetworkPacketsOut",	StandardUnit.COUNT,		"AWS/EC2");
		AwsUtils.makeUsageName(usageNames, "networkInBytes",	"NetworkIn",			StandardUnit.BYTES,		"AWS/EC2");
		AwsUtils.makeUsageName(usageNames, "networkOutBytes",	"NetworkOut",			StandardUnit.BYTES,		"AWS/EC2");
		AwsUtils.makeUsageName(usageNames, "healthFails",		"StatusCheckFailed",	StandardUnit.COUNT,		"AWS/EC2");
	}
	
	static public List<Object> getUsageName(String name) {
		return usageNames.get(name);
	}
}
