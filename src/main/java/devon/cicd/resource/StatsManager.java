package devon.cicd.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devon.cicd.ctx.ThreadContext;
import devon.cicd.data.KeyValue;
import devon.cicd.db.dao.ConfigWrapper;
import devon.cicd.factory.InterfaceFactory;

@Service
public class StatsManager {
	@Autowired
	InterfaceFactory interfaceFactory;
	@Autowired
	ConfigWrapper configWrapper;
	
	private List<Map<String, Object>> getStages(Map<String, Object> input){
		String stageCode = (String)input.get("stageCode");
		List<Map<String, Object>> stages;
		if(stageCode != null || "ALL".equals(stageCode)) {
			stages = configWrapper.selectStageList(ThreadContext.getUserContext().getSubSystemId());
		}else {
			stages = new ArrayList<Map<String,Object>>(1);
			stages.add(input);
		}
		return stages;
	}
	
	public Map<String, Object> getSimpleStats(Map<String, Object> input) throws Exception{
		List<Map<String, Object>> stages = getStages(input);
		if(stages == null || stages.size()==0) {
			return null;
		}
		
		Map<String, Object> reStats = new HashMap<String, Object>();
		Runtime runtime;

		// Runtime
		List<Map<String, Object>> datas = new ArrayList<Map<String,Object>>(stages.size());
		Map<String,Object> data;
		for(Map<String, Object> stage: stages) {
			runtime = interfaceFactory.getRuntime(stage);
			data = new HashMap<String, Object>(2);
			data.put("stage", stage);
			data.put("runtimeStats", runtime.getSimpleStats(stage));
			datas.add(data);
		}
		reStats.put("stages", datas);
		
		// Merge
		SourceRepository sourceRepository = interfaceFactory.getSourceRepository();
		reStats.put("mergeStats", sourceRepository.getMergeRequestSimpleStats(input));
		
		return reStats;
	}
	
	public List<Map<String, Object>> getSourceCommitStatsByDay(Map<String, Object> input) throws Exception{
		List<Map<String, Object>> stages = getStages(input);
		if(stages == null || stages.size()==0) {
			return null;
		}

		Map<String,Object> stats;
		List<KeyValue> list;

		List<Map<String, Object>> reStats = new ArrayList<Map<String,Object>>(stages.size());
		SourceRepository sourceRepository = interfaceFactory.getSourceRepository();
		for(Map<String, Object> stage: stages) {
			stats = new HashMap<String, Object>(2);
			stats.put("stage", stage);
			list = sourceRepository.getCommitsStatsByBranch((String)stage.get("branchName"));
			stats.put("commitStats", list);
			reStats.add(stats);
		}
		
		return reStats;
	}
	
	public List<Map<String, Object>> getSourceMergeRequestStatsByDay(Map<String, Object> input) throws Exception{
		List<Map<String, Object>> stages = getStages(input);
		if(stages == null || stages.size()==0) {
			return null;
		}

		Map<String,Object> stats;
		List<KeyValue> list;

		List<Map<String, Object>> reStats = new ArrayList<Map<String,Object>>(stages.size());
		SourceRepository sourceRepository = interfaceFactory.getSourceRepository();
		for(Map<String, Object> stage: stages) {
			sourceRepository = interfaceFactory.getSourceRepository();
			stats = new HashMap<String, Object>(2);
			stats.put("stage", stage);
			list = sourceRepository.getMerteRequestStatsByBranch((String)stage.get("branchName"));
			stats.put("mergeRequestStats", list);
			reStats.add(stats);
		}
		
		return reStats;
	}	
	
	public List<Map<String, Object>> getSourcePipelineStatsByDay(Map<String, Object> input) throws Exception{
		List<Map<String, Object>> stages = getStages(input);
		if(stages == null || stages.size()==0) {
			return null;
		}

		List<Map<String, Object>> reStats = new ArrayList<Map<String,Object>>(stages.size());
		Map<String,Object> stats;
		List<KeyValue> list;
		
		PipelineJob pipelineJob = interfaceFactory.getPipelineJob(null);
		for(Map<String, Object> stage: stages) {
			stats = new HashMap<String, Object>(2);
			stats.put("stage", stage);
			list = pipelineJob.getPipeLineStats((String)stage.get("branchName"));
			stats.put("pipelineStats", list);
			reStats.add(stats);
		}
		
		return reStats;
	}
	
	public List<Map<String, Object>> getInstancesStats(Map<String, Object> input) throws Exception{
		List<Map<String, Object>> stages = getStages(input);
		if(stages == null || stages.size()==0) {
			return null;
		}
		Runtime runtime;
		List<Map<String, Object>> usages = new ArrayList<Map<String, Object>>(stages.size());
		
		Map<String, Object> usage;
		for(Map<String, Object> stage: stages) {
			usage = new HashMap<String, Object>(stages.size());
			input.put("stageCode", stage.get("stageCode"));
			input.put("region", (String)input.get("region"));

			runtime = interfaceFactory.getRuntime(input);
			usage.put("stage", stage);
			usage.put("runtimeStats",  runtime.getInstancesStats(input));
			usages.add(usage);
		}
		
		return usages;
	}
	
	public List<Map<String, Object>> getLoadBalancesStats(Map<String, Object> input) throws Exception{
		List<Map<String, Object>> stages = getStages(input);
		if(stages == null || stages.size()==0) {
			return null;
		}
		Runtime runtime;
		List<Map<String, Object>> usages = new ArrayList<Map<String, Object>>(stages.size());
		
		Map<String, Object> usage;
		for(Map<String, Object> stage: stages) {
			usage = new HashMap<String, Object>(stages.size());
			input.put("stageCode", stage.get("stageCode"));
			input.put("region", (String)input.get("region"));

			runtime = interfaceFactory.getRuntime(input);
			usage.put("stage", stage);
			usage.put("runtimeStats",  runtime.getLoadBalancesStats(input));
			usages.add(usage);
		}
		
		return usages;
	}	
	
	public List<Map<String, Object>> getInstancesUsage(Map<String, Object> input) throws Exception{
		List<Map<String, Object>> stages = getStages(input);
		if(stages == null || stages.size()==0) {
			return null;
		}
		
		Runtime runtime = interfaceFactory.getRuntime(input);
		return  runtime.getInstancesUsage(input);		
	}	
	
	public List<Map<String, Object>> getLoadBalancesUsage(Map<String, Object> input) throws Exception{
		List<Map<String, Object>> stages = getStages(input);
		if(stages == null || stages.size()==0) {
			return null;
		}
		
		Runtime runtime = interfaceFactory.getRuntime(input);
		return  runtime.getLoadBalancesUsage(input);			
	}	
}