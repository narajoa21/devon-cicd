package devon.cicd.resource;

import java.util.List;
import java.util.Map;

import devon.cicd.data.KeyValue;


public interface PipelineJob {
	public List<Map<String, Object>> getPipeLineLogs(String branchName) throws Exception;
	public Map<String, Object> getDefine(Map<String, Object> input) throws Exception;
	public Map<String, Object> runPipeline(Map<String, Object> input) throws Exception;
	public List<KeyValue> getPipeLineStats(String branchName) throws Exception;
	public Map<String, Object> isRunningPipeline(Map<String, Object> input) throws Exception;
}
