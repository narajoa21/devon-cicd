package devon.cicd.resource.git;

import devon.cicd.DevonCicdApplication;
import devon.cicd.config.Constants;
import devon.cicd.config.SourceRepoConfig;
import devon.cicd.ctx.ThreadContext;
import devon.cicd.factory.InterfaceFactory;
import devon.cicd.util.HttpUtils;
import devon.cicd.util.JsonUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.RepositoryFile;
import org.springframework.util.StringUtils;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GitlabExtentApi {
    private GitLabApi gitLabApi;
    private InterfaceFactory interfaceFactory;
    private SourceRepoConfig sourceRepoConfig;
	int projectId;
    
    public GitlabExtentApi() {
    	interfaceFactory = DevonCicdApplication.applicationContext.getBean(InterfaceFactory.class);
    	sourceRepoConfig = ThreadContext.getSourceRepoConfig();
    	gitLabApi = (GitLabApi)interfaceFactory.getSourceRepoApi(sourceRepoConfig);
    	System.out.println("projectId:" + ThreadContext.getUserContext().getProjectId());
    	projectId = Integer.parseInt(ThreadContext.getUserContext().getProjectId());
    }
    
    public GitlabExtentApi(SourceRepoConfig sourceRepoConfig) {
    	interfaceFactory = DevonCicdApplication.applicationContext.getBean(InterfaceFactory.class);
    	this.sourceRepoConfig = sourceRepoConfig;
    	gitLabApi = (GitLabApi)interfaceFactory.getSourceRepoApi(sourceRepoConfig);
    	if(ThreadContext.getUserContext() != null && ThreadContext.getUserContext().getProjectId() != null) {
    	  	projectId = Integer.parseInt(ThreadContext.getUserContext().getProjectId());  		
    	}
    }    
    
    private Map<String, String> addHeaders() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("PRIVATE-TOKEN", (String)sourceRepoConfig.getConfigs().get("token"));
        return headers;
    }

    private Map<String, String> addUserHeaders() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        String token = ThreadContext.getUserContext().getUserPersonalAccessToken();
        headers.put("PRIVATE-TOKEN", token);
        return headers;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Map> getRunners() throws Exception {
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/runners/all",
                null,
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        return JsonUtils.unmarshalToList(json);
    }

    @SuppressWarnings("rawtypes")
    public int getDockerRunnerId() throws Exception {
        List<Map> runners = this.getRunners();
        String runnerName = (String)sourceRepoConfig.getConfigs().get("runnerName");
        for (Map runner : runners) {
            if (runner.get("description").equals(runnerName)) {
                return Integer.parseInt(runner.get("id").toString());
            }
        }
        throw new Exception("Not Found " + runnerName);
    }
    
    public String getRepositoryFile(Map<String, Object> input) throws Exception {
    	InputStream inputStream = null;
    	String commitId = (String)input.get("commitId");
    	if(commitId != null) {
        	inputStream = gitLabApi.getRepositoryFileApi().getRawFile(projectId, commitId, (String)input.get("filePath"));
    	}else {
        	inputStream = gitLabApi.getRepositoryFileApi().getRawFile(projectId, (String)input.get("branchName"), (String)input.get("filePath"));    		
    	}
    	
        String content = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
        return content;
    }

    public void removeRepositoryFile(Map<String, Object> input) throws Exception {
        gitLabApi.getRepositoryFileApi().deleteFile((String)input.get("filePath"), projectId, (String)input.get("branchName"), "Devon-cicd Update");
    }

    //한번씩만 보완.
    public void updateOrCraeteRepositoryFile(Map<String, Object> input) throws Exception {
        RepositoryFile file = null;
        try {
            file = gitLabApi.getRepositoryFileApi().getFile((String)input.get("filePath"), projectId, (String)input.get("branchName"));
        } catch (Exception e) {
        }

        if (file != null) {
            file.setContent((String)input.get("contents"));
            file.setEncoding(null);
            gitLabApi.getRepositoryFileApi().updateFile(file, projectId, (String)input.get("branchName"), "Devon-cicd Update");
        } else {
            file = new RepositoryFile();
            file.setFilePath((String)input.get("filePath"));
            file.setContent((String)input.get("contents"));
            file.setEncoding(null);
            gitLabApi.getRepositoryFileApi().createFile(file, projectId,  (String)input.get("branchName"), "Devon-cicd Update");
        }
    }
    
    @SuppressWarnings("rawtypes")
    public Map getActiveUsers(Map<String, Object> input) throws Exception {
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + (String)input.get("projectId") +"/search?scope=users&state=active",
                null,
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 201) {
            throw new Exception(json);
        }
        return JsonUtils.unmarshal(json);   	
    }

    @SuppressWarnings("rawtypes")
	public Map createUser(Map user) throws Exception {
        HttpResponse response = new HttpUtils().makeRequest("POST",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/users",
                JsonUtils.marshal(user),
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 201) {
            throw new Exception(json);
        }
        return JsonUtils.unmarshal(json);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Map forkProject(String namespace) throws Exception {
        Map params = new HashMap();
        params.put("namespace", namespace);
        HttpResponse response = new HttpUtils().makeRequest("POST",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/fork",
                JsonUtils.marshal(params),
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        return JsonUtils.unmarshal(json);
    }

    @SuppressWarnings({"unused" })
    public void deleteForkRelation() throws Exception {
        HttpResponse response = new HttpUtils().makeRequest("DELETE",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/fork",
                null,
                this.addHeaders()
        );
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map enableRunner(int runnerId) throws Exception {
        Map params = new HashMap();
        params.put("runner_id", runnerId);
        HttpResponse response = new HttpUtils().makeRequest("POST",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/runners",
                JsonUtils.marshal(params),
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        return JsonUtils.unmarshal(json);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map createTrigger(String gitlabUsername) throws Exception {
        Map params = new HashMap();
        params.put("description", (String)sourceRepoConfig.getConfigs().get("triggerName"));

        Map<String, String> headers = this.addHeaders();
        if (!StringUtils.isEmpty(gitlabUsername)) {
            headers.put("Sudo", gitlabUsername);
        }
        HttpResponse response = new HttpUtils().makeRequest("POST",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/triggers",
                JsonUtils.marshal(params),
                headers
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        return JsonUtils.unmarshal(json);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public String getProjectTriggerToken(String triggerName) throws Exception {
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/triggers",
                null,
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        List<Map> list = JsonUtils.unmarshalToList(json);
        String token = null;
        for (int i = 0; i < list.size(); i++) {
            Map map = list.get(i);
            if (map.get("description").equals(triggerName)) {
                token = map.get("token").toString();
            }
        }
        return token;
    }
    
    @SuppressWarnings("rawtypes")
    public String getProjectTriggerTokenById(int id) throws Exception {
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/triggers/" + id,
                null,
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        Map map = JsonUtils.unmarshal(json);
        return (String)map.get("token");
    }    

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map triggerPipeline(String token, String ref, Map<String, Object> variables) throws Exception {
        Map headers = new HashMap();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("PRIVATE-TOKEN", (String)sourceRepoConfig.getConfigs().get("token"));

        Map params = new HashMap();
        if (variables!= null && !variables.isEmpty()) {
            for (Map.Entry<String, Object> entry : variables.entrySet()) {
                params.put("variables[" + entry.getKey() + "]", entry.getValue());
            }
        }
        params.put("token", token);
        params.put("ref", ref);
        String postQueryString = HttpUtils.createPOSTQueryString(params);

        HttpResponse response = new HttpUtils().makeRequest("POST",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/trigger/pipeline",
                postQueryString,
                headers
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode > 299) {
        	throw new RuntimeException(json);
        }
        return JsonUtils.unmarshal(json);
    }

    @SuppressWarnings("unused")
	public void playJob(int jobId) throws Exception {
        HttpResponse response = new HttpUtils().makeRequest("POST",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/jobs/" + jobId + "/play",
                null,
                this.addHeaders()
        );
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public List<Map> getCommitsByBranch(String branchName) throws Exception{ 
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/repository/commits?ref_name=" + branchName, // + "&until=2019-08-01T14:28:36.000Z" ,
        		null,
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        return JsonUtils.unmarshalToList(json);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public List<Map> getCommitsByBranch(String branchName, String sinceTime, boolean withStats) throws Exception{ 
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/repository/commits?ref_name=" + branchName + "&since=" + sinceTime + (withStats?"&with_stats=true":""),
        		null,
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        return JsonUtils.unmarshalToList(json);
    }
    
    @SuppressWarnings({ "rawtypes"})
    public Map getCommitsByCommitId(String commitId) throws Exception{ 
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/repository/commits/" + commitId,
        		null,
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        return JsonUtils.unmarshal(json);
    }    
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public List<Map> getFilesByCommit(String id) throws Exception{ 
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/repository/commits/"+ id + "/diff",
        		null,
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        return JsonUtils.unmarshalToList(json);
    } 
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Map> getMergeRequests(Map<String, Object> input) throws Exception {
		String condition = "?sort=desc&order_by=created_at";
		if(input.get("state") != null) {
			condition = condition + "&state=" + input.get("state");
		}
		if(input.get("assigneeId") != null) {
			condition = condition + "&assignee_id=" + input.get("assigneeId");			
		}
		if(input.get("authorId") != null) {
			condition = condition + "&author_id=" + input.get("authorId");			
		}		
		if(input.get("targetBranchName") != null) {
			condition = condition + "&target_branch=" + input.get("targetBranchName");			
		}
		if(input.get("createTime") != null) {
			condition = condition + "&created_after=" + input.get("createTime");
		}
		
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/merge_requests" + condition,
        		null,
                this.addHeaders()
        );
        
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        return JsonUtils.unmarshalToList(json);
	}
	
    @SuppressWarnings("rawtypes")
	public Map createMergeRequest(Map input) throws Exception {
        HttpResponse response = new HttpUtils().makeRequest("POST",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" +  projectId + "/merge_requests",
                JsonUtils.marshal(input),
                this.addUserHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 201) {
            throw new Exception(json);
        }
        return JsonUtils.unmarshal(json);
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Map addMergeRequestComment(Map input) throws Exception {
    	Map condition = new HashMap(1);
    	condition.put("body", input.get("comment"));
        HttpResponse response = new HttpUtils().makeRequest("POST",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" +  projectId + "/merge_requests/" + input.get("requestIid") + "/notes",
                JsonUtils.marshal(condition),
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 201) {
            throw new Exception(json);
        }
        return JsonUtils.unmarshal(json);
    } 
    
    public boolean deletePersonalToken(int userId, int tokenId) throws Exception {
        HttpResponse response = new HttpUtils().makeRequest("DELETE",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/users/" +  userId + "/impersonation_tokens/" + tokenId,
                null,
                this.addHeaders()
        );
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 204) {
        	return false;
        }
    	return true;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Map> getPersonalTokens (int userId) throws Exception {
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/users/" +  userId + "/impersonation_tokens",
                null,
                this.addHeaders()
        );
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 201 && statusCode != 200) {
        	throw new Exception(json);
        }
        return JsonUtils.unmarshalToList(json);
    }
    
    @SuppressWarnings("rawtypes")
	public boolean checkPersonalToken(int userId, int tokenId) throws Exception {
    	List<Map> tokens = getPersonalTokens(userId);
    	if(tokens != null) {
    		for(Map person: tokens) {
    			if(((String)person.get("name")).equals(Constants.SOURCE_PERSONAL_TOKEN_NAME)) {
    				if((int)person.get("id") == tokenId && ((boolean)person.get("active"))) {
    					return true;
    				}else {
    					deletePersonalToken(userId, (int)person.get("id"));
    				}
    			}
    		}
    	}
    	return false;
    }
    
    @SuppressWarnings("rawtypes")
	public void findAndDeletePersonalToken(int userId) throws Exception {
    	List<Map> tokens = getPersonalTokens(userId);
    	if(tokens != null) {
    		for(Map person: tokens) {
    			if(((String)person.get("name")).equals(Constants.SOURCE_PERSONAL_TOKEN_NAME)) {
					deletePersonalToken(userId, (int)person.get("id"));
    			}
    		}
    	}
    }    
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Map createPersonalToken(int userId) throws Exception {
       	Map condition = new HashMap(1);
       	String [] scopes= {"api", "read_user", "read_repository", "write_repository"};
    	condition.put("name", Constants.SOURCE_PERSONAL_TOKEN_NAME);
    	condition.put("scopes", scopes);
    	
    	HttpResponse response = new HttpUtils().makeRequest("POST",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/users/" +  userId + "/impersonation_tokens",
                JsonUtils.marshal(condition),
                this.addHeaders()
        );
        
    	HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 201) {
            throw new Exception(json);
        }
        return JsonUtils.unmarshal(json);
     }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map acceptMergeRequestComment(Map input) throws Exception {
    	Map condition = new HashMap(1);
    	
    	boolean isAccept = (boolean)input.get("isAccept");
    	HttpResponse response = null;
    	if(isAccept) {
    		condition.put("	", input.get("title"));
	        response = new HttpUtils().makeRequest("PUT",
	        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" +  projectId + "/merge_requests/" + input.get("requestIid") + "/merge",
	                JsonUtils.marshal(condition),
	                this.addUserHeaders()
	        );
    	}else {
    		condition.put("state_event", "close");
	        response = new HttpUtils().makeRequest("PUT",
	        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" +  projectId + "/merge_requests/" + input.get("requestIid"),
	                JsonUtils.marshal(condition),
	                this.addUserHeaders()
	        );    		
    	}
        
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 200 ) {
            throw new Exception(json);
        }
        addMergeRequestComment(input);
        return JsonUtils.unmarshal(json);
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Map> getEvents(Map<String, Object> input) throws Exception {
		String condition = "?sort=desc&order_by=created_at";
		if(input.get("action") != null) {
			condition = condition + "&action=" + input.get("action");
		}
		if(input.get("targetType") != null) {
			condition = condition + "&target_type=" + input.get("targetType");			
		}
		if(input.get("after") != null) {
			condition = condition + "&after=" + input.get("after");			
		}
		
        HttpResponse response = new HttpUtils().makeRequest("GET",
        		(String)sourceRepoConfig.getConfigs().get("host") + "/api/v4/projects/" + projectId + "/events" + condition,
        		null,
                this.addHeaders()
        );
        
        HttpEntity entity = response.getEntity();
        String json = EntityUtils.toString(entity);
        return JsonUtils.unmarshalToList(json);
	}	
}
