package devon.cicd.resource.git;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.CommitAction;
import org.gitlab4j.api.models.CommitAction.Action;
import org.gitlab4j.api.models.Member;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.User;

import devon.cicd.DevonCicdApplication;
import devon.cicd.config.Constants;
import devon.cicd.config.SourceRepoConfig;
import devon.cicd.ctx.CicdContext;
import devon.cicd.ctx.ThreadContext;
import devon.cicd.ctx.UserContext;
import devon.cicd.data.KeyValue;
import devon.cicd.db.dao.ConfigWrapper;
import devon.cicd.factory.InterfaceFactory;
import devon.cicd.resource.SourceManager;
import devon.cicd.resource.SourceRepository;
import devon.cicd.util.CollectionUtils;
import devon.cicd.util.DataUtils;
import devon.cicd.util.DateUtils;

public class GitlabSourceRepository implements SourceRepository{
    private GitLabApi gitLabApi;
    private InterfaceFactory interfaceFactory;
    private SourceRepoConfig sourceRepoConfig;
    private ConfigWrapper configWrappter;
    
    public GitlabSourceRepository() {
    	interfaceFactory = DevonCicdApplication.applicationContext.getBean(InterfaceFactory.class);
    	configWrappter = DevonCicdApplication.applicationContext.getBean(ConfigWrapper.class);
    	sourceRepoConfig = CicdContext.instance().getSourceRepoConfig(ThreadContext.getUserContext().getSourceRepositoryId());
    	gitLabApi = (GitLabApi)interfaceFactory.getSourceRepoApi(sourceRepoConfig);
    }
    
    public Map<String, Object> getUserByUserName(String userName)  throws Exception{
    	User user = gitLabApi.getUserApi().getUser(userName);
    	if(user == null) {
    		return null;
    	}
 
    	Map<String, Object> userMap = new HashMap<String, Object>();
    	userMap.put("type", "Gitlab User");
    	userMap.put("userId", user.getId());
    	userMap.put("name", user.getName());
    	userMap.put("userName",user.getUsername());
    	userMap.put("state", user.getState());
    	if(user.getLocation() != null) {
    		userMap.put("location", user.getLocation());
    	}
    	if(user.getLocation() != null) {
    		userMap.put("orgonization", user.getOrganization());
    	}
    	userMap.put("email", user.getEmail());
    	userMap.put("isAdmin", user.getIsAdmin());
    	userMap.put("isExternal", user.getExternal());
    	userMap.put("skipConfirmation", user.getSkipConfirmation());
    	userMap.put("twoFactorEnabled", user.getTwoFactorEnabled());
    	userMap.put("canCreateGroup", user.getCanCreateGroup());
    	userMap.put("canCreateProject", user.getCanCreateGroup());
    	userMap.put("createTime", DateUtils.getYYYYMMDDHHMMSS(user.getCreatedAt()));
    	userMap.put("lastActivity", DateUtils.getYYYYMMDDHHMMSS(user.getLastActivityOn()));
    	userMap.put("lastLogon",DateUtils.getYYYYMMDDHHMMSS(user.getLastActivityOn()));
    	
    	if(user.getBio() != null) {
    		userMap.put("bio", user.getBio());
    	}
    	if(user.getAvatarUrl() != null) {
    		userMap.put("avatarUrl", user.getAvatarUrl());
    	}
    	if(user.getLinkedin() != null) {
    		userMap.put("linkedIn", user.getLinkedin());
    	}
    	if(user.getTwitter() != null) {
    		userMap.put("twitter", user.getTwitter());
    	}
    	if(user.getWebsiteUrl() != null) {
    		userMap.put("websiteUrl", user.getWebsiteUrl());
    	}
    	if(user.getWebUrl() != null) {
    		userMap.put("webUrl", user.getWebUrl());
    	}
    	if(user.getSkype() != null) {
    		userMap.put("skype", user.getSkype());
    	}
    	if(user.getProvider() != null) {
    		userMap.put("provider", user.getProvider());
    	}
    	if(user.getProjectsLimit() != null) {
    		userMap.put("projectsLimit", user.getProjectsLimit());
    	}
    	if(user.getSharedRunnersMinutesLimit() != null) {
    		userMap.put("sharedRunnersMinutesLimit", user.getSharedRunnersMinutesLimit());
    	}
    	
    	return userMap;
    }
    
    public List<Map<String, Object>> getProjects() throws Exception {
    	List<Project> projets = gitLabApi.getProjectApi().getMemberProjects();
    	if(projets == null) {
    		return null;
    	}
    	List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();
    	Map<String, Object> item;
    	Map<String, Object> systemGroup;
    	for(Project project : projets) {
    		item = new HashMap<String,Object>(4);
    		item.put("sourceRepository", project.getPathWithNamespace());
    		item.put("sourceRepositoryUrl", project.getHttpUrlToRepo());
    		item.put("projectId", project.getId());
  
    		systemGroup = configWrappter.selectSystemGroup(project.getPathWithNamespace());
    		if(systemGroup == null) {
    			item.put("subSystemId", null);
    			item.put("groupDiv", null);
    			item.put("subSystemName", null);
    			item.put("systemId", null);
    			item.put("systemName", null);
    		}else {
    			item.put("subSystemId", systemGroup.get("groupId"));
    			item.put("groupDiv", systemGroup.get("groupDiv"));
    			item.put("subSystemName", systemGroup.get("groupName"));
    			item.put("systemId", systemGroup.get("parentId"));
    			item.put("systemName", systemGroup.get("parentName"));    			
    		}
    		returnList.add(item);
    	}
    	return returnList;
    }
    
	@SuppressWarnings("rawtypes")
	public List<Map<String, Object>> getCommitsByBranch(String branchName) throws Exception {
		SourceManager sourceManager = DevonCicdApplication.applicationContext.getBean(SourceManager.class);
		List<Map<String, Object>> reCommits = null;
		System.out.println(branchName);
		
		List<Map> commits = new GitlabExtentApi().getCommitsByBranch(branchName);
		if(commits != null && commits.size() > 0) {
			Map<String, Object> reCommit;
			reCommits = new ArrayList<Map<String, Object>>();
			Map<String, Object> user;
			for(Map commit: commits) {
				reCommit =  new HashMap<String,Object>();
				reCommit.put("commitId", commit.get("id"));
				reCommit.put("shortId", commit.get("sort_id"));
				reCommit.put("createTime", DateUtils.arangeGitlabDate((String)commit.get("created_at")));
				reCommit.put("commitTime", DateUtils.arangeGitlabDate((String)commit.get("committed_date")));
				reCommit.put("committer", commit.get("committer_name"));
				user = sourceManager.getUserByUserName((String)commit.get("committer_name"));
				if(user == null) {
					reCommit.put("committerName", commit.get("committer_name"));
				}else {
					reCommit.put("committerName", user.get("name"));
				}
				reCommit.put("title", commit.get("title"));
				reCommit.put("message", commit.get("message"));
				reCommits.add(reCommit);
			}
		}
		
		return reCommits;
	}

	@SuppressWarnings("rawtypes")
	public List<KeyValue> getCommitsStatsByBranch(String branchName) throws Exception {
		long currentTime = System.currentTimeMillis();
		List<KeyValue> statsList = DataUtils.makeDaysByZero(currentTime, "yyyy-MM-dd", Constants.DASHBOARD_STATS_DAYS);
		Map<String, KeyValue> searchs = CollectionUtils.convertListToMapByKeyValue(statsList);
		
		String since = DateUtils.getISO8601Format(new Date(System.currentTimeMillis() - (86400000L * (Constants.DASHBOARD_STATS_DAYS - 1))));
		since = since.substring(0, since.indexOf('T')) + "T00:00:00Z";
		
		KeyValue keyValue;
		List<Map> commits = new GitlabExtentApi().getCommitsByBranch(branchName, since, true);
		if(commits != null && commits.size() > 0) {
			String time;
			for(Map commit: commits) {
				time = (String)commit.get("committed_date");
				if(time == null) {
					continue;
				}
				time = time.substring(0, 10);
				keyValue = searchs.get(time);
				if(keyValue == null) {
					continue;
				}
				keyValue.setValue((Integer)keyValue.getValue() +  1);
			}
		}
		return statsList;
	}

	@SuppressWarnings("rawtypes")
	public List<KeyValue> getMerteRequestStatsByBranch(String branchName) throws Exception {
		long currentTime = System.currentTimeMillis();
		List<KeyValue> statsList = DataUtils.makeDaysByZero(currentTime, "yyyy-MM-dd", Constants.DASHBOARD_STATS_DAYS);
		Map<String, KeyValue> searchs = CollectionUtils.convertListToMapByKeyValue(statsList);

		String since = DateUtils.getISO8601Format(new Date(System.currentTimeMillis() - (86400000L * (Constants.DASHBOARD_STATS_DAYS - 1))));
		since = since.substring(0, since.indexOf('T')) + "T00:00:00Z";
		
		Map<String, Object> condition = new HashMap<String, Object>(3);
		condition.put("targetBranchName", branchName);
		condition.put("createTime", since);
		
		KeyValue keyValue;
		List<Map> mrs = new GitlabExtentApi().getMergeRequests(condition);
		if(mrs != null && mrs.size() > 0) {
			String time;
			for(Map mr: mrs) {
				time = (String)mr.get("created_at");
				if(time == null) {
					continue;
				}
				time = time.substring(0, 10);
				keyValue = searchs.get(time);
				if(keyValue == null) {
					continue;
				}
				keyValue.setValue((Integer)keyValue.getValue() +  1);
			}
		}
		return statsList;
	}
	
	@SuppressWarnings("rawtypes")
	public List<Map<String, Object>> getFilesByCommit(String id) throws Exception {
		List<Map<String, Object>> reCommits = null;;
		List<Map> commits = new GitlabExtentApi().getFilesByCommit(id);
		if(commits != null && commits.size() > 0) {
			Map<String, Object> reCommit;
			reCommits = new ArrayList<Map<String, Object>>();
			for(Map commit: commits) {
				reCommit =  new HashMap<String,Object>(3);
				reCommit.put("diff", commit.get("diff"));
				if((boolean)commit.get("deleted_file")) {
					reCommit.put("type", "D");
					reCommit.put("filePath", commit.get("old_path"));
				}else if((boolean)commit.get("renamed_file")) {
					reCommit.put("type", "R");
					reCommit.put("filePath", commit.get("new_path"));
				}else if((boolean)commit.get("new_file")) {
					reCommit.put("type", "C");
					reCommit.put("filePath", commit.get("new_path"));
				}else {
					reCommit.put("type", "U");
					reCommit.put("filePath", commit.get("old_path"));
				}
				reCommits.add(reCommit);
			}
		}
		
		return reCommits;
	}
	public Map<String, Object> getFilesContents(Map<String, Object> input) throws Exception {
		String contents = new GitlabExtentApi().getRepositoryFile(input);
		Map<String, Object> reContents = new HashMap<String, Object>(1);
		reContents.put("contents", contents);
		return reContents;		
	}
	
	private Integer getUserId(String userName) throws Exception {
		SourceManager sourceManager = DevonCicdApplication.applicationContext.getBean(SourceManager.class);
		Map<String, Object> user = sourceManager.getUserByUserName(userName);
		if(user == null) {
			return null;
		}
		return (Integer)user.get("userId");		
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List<Map<String, Object>> getMergeRequests(Map<String, Object> input) throws Exception {
		String userName = (String)input.get("assignee");
		Integer id;
		if(userName != null) {
			id = getUserId(userName);
			if(id != null) {
				input.put("assigneeId", id);
			}
		}
		userName = (String)input.get("author");
		if(userName != null) {
			id = getUserId(userName);
			if(id != null) {
				input.put("authorId", id);
			}
		}

		List<Map> mrs = new GitlabExtentApi().getMergeRequests(input);
		if(mrs == null || mrs.size() == 0) {
			return null;
		}
		
		List<Map<String, Object>> reMrs = new ArrayList<Map<String,Object>>(mrs.size());
		for(Map mr : mrs) {
			reMrs.add(GitlabDataConverter.convertMergeRequestToMap(mr));
		}
		return reMrs;
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> createMergeRequest(Map<String, Object> input) throws Exception {
		List assignees = (List)input.get("assignees");
		if(assignees == null || assignees.size() == 0) {
			return null;
		}
		
		SourceManager sourceManager = DevonCicdApplication.applicationContext.getBean(SourceManager.class);
		Integer [] assigneeIds = new Integer[assignees.size()];
		String userName;
		Map<String, Object> user;
		for(int inx = 0; inx <assignees.size(); inx++) {
			userName = (String)assignees.get(inx);
			user = sourceManager.getUserByUserName(userName);
			assigneeIds[inx] = (int)user.get("userId");
		}
		
		Map condition = new HashMap();
		condition.put("id", ThreadContext.getUserContext().getProjectId());
		condition.put("source_branch", input.get("sourceBranchName"));
		condition.put("target_branch", input.get("targetBranchName"));
		condition.put("assignee_ids", assigneeIds);
		condition.put("title", input.get("title"));
		if(input.get("description") != null) {
			condition.put("description", input.get("description"));			
		}
		
		Map mr = new GitlabExtentApi().createMergeRequest(condition);
		if(mr == null || mr.size() == 0) {
			return null;
		}
		return GitlabDataConverter.convertMergeRequestToMap(mr);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map<String, Object> addMergeRequestComment(Map<String, Object> input) throws Exception {
		Map note = new GitlabExtentApi().addMergeRequestComment(input);
		if(note == null || note.size() == 0) {
			return null;
		}
		Map<String, Object> reNote = new HashMap<String, Object>();
		reNote.put("noteId", note.get("id"));
		reNote.put("body", note.get("body"));
		reNote.put("userName", ((Map)note.get("author")).get("userName"));
		reNote.put("name", ((Map)note.get("author")).get("user"));
		reNote.put("createTime", DateUtils.arangeGitlabDate((String)note.get("created_at")));
		return reNote;
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	@Override
	public Map<String, Object> acceptMergeRequest(Map<String, Object> input) throws Exception {
		Map mr = new GitlabExtentApi().acceptMergeRequestComment(input);
		return GitlabDataConverter.convertMergeRequestToMap(mr);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map<String, Object> getMergeRequestSimpleStats(Map<String, Object> input) throws Exception {
		GitlabExtentApi api = new GitlabExtentApi();
		
		int userId = this.getUserId(ThreadContext.getUserContext().getUserName());
		
		Map<String, Object> reStats = new HashMap<String, Object>(2);		
		Map<String, Object> condition = new HashMap<String, Object>(5);
		
		condition.put("state", "opened");

		condition.put("assigneeId", userId);
		List<Map> res = api.getMergeRequests(condition);
		int count = 0;
		if(res != null && res.size() >= 0) {
			count = res.size();
		}
		reStats.put("assignedMergeCount", count);
		
		condition.remove("assigneeId");
		condition.put("authorId", userId);
		res = api.getMergeRequests(condition);
		count = 0;
		if(res != null && res.size() >= 0) {
			count = res.size();
		}
		reStats.put("requestMergeCount", count);

		return reStats;
	}
	
	public Branch createBranchRequest(Map<String, Object> input) throws Exception{
		  GitlabExtentApi api = new GitlabExtentApi();
		  int projectId = api.projectId;
		  Random generator = new Random();
		  String branchName = "cherrypick" + generator.nextInt(1000000);
		  String ref = (String)input.get("commitId");
		  
	      return gitLabApi.getRepositoryApi().createBranch(projectId, branchName, ref);
		
	}
	
	public Commit createCommitRequest(Map<String, Object> input, Branch branch) throws Exception{
		 GitlabExtentApi api = new GitlabExtentApi();
		 int projectId = api.projectId;
		 String branchName = branch.getName();
		 String commitMessage = (String)input.get("message");
		 String authorEmail = (String)input.get("authorEmail");
		 String authorName = (String)input.get("authorName");
		 String fileName = branchName +"commit.txt";
		 CommitAction commitAction = new CommitAction()
	                .withAction(Action.CREATE)
	                .withContent(null)
	                .withFilePath(fileName + ".bk");
		 return gitLabApi.getCommitsApi().createCommit(projectId, branchName, commitMessage, null, authorEmail, authorName, Arrays.asList(commitAction));
	}
		
	@Override
	public List<Map<String, Object>> getProjectMembers() throws Exception {
		List<Member> members = gitLabApi.getProjectApi().getMembers(Integer.parseInt(ThreadContext.getUserContext().getProjectId()));
		if(members == null || members.size() == 0) {
			return null;
		}
		List<Map<String, Object>> reMembers = new ArrayList<Map<String, Object>>(members.size());
		for(Member member: members) {
			reMembers.add(GitlabDataConverter.convertMemberToMap(member));
		}
		return reMembers;
	}
	
	@Override
	public Map<String, Object> getProjectCurrentUser() throws Exception {
		UserContext userContext = ThreadContext.getUserContext();
		Member member = gitLabApi.getProjectApi().getMember(Integer.parseInt(userContext.getProjectId()), Integer.parseInt(userContext.getId()));
		if(member == null ) {
			return null;
		}
		return GitlabDataConverter.convertMemberToMap(member);
	}	
}
