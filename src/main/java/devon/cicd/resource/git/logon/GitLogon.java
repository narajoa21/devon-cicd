package devon.cicd.resource.git.logon;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.User;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import devon.cicd.DevonCicdApplication;
import devon.cicd.config.Constants;
import devon.cicd.config.SourceRepoConfig;
import devon.cicd.ctx.CicdContext;
import devon.cicd.ctx.ThreadContext;
import devon.cicd.data.LogonUser;
import devon.cicd.db.dao.TokenWrapper;
import devon.cicd.factory.InterfaceFactory;
import devon.cicd.logon.Logon;
import devon.cicd.resource.git.GitlabExtentApi;
import devon.cicd.util.HttpUtils;
import net.minidev.json.JSONObject;

public class GitLogon implements Logon {
    private ObjectMapper objectMapper;
    private InterfaceFactory interfaceFactory;
    
    private final Log logger = LogFactory.getLog(getClass());
    
    public GitLogon() {
    	 objectMapper = DevonCicdApplication.applicationContext.getBean(ObjectMapper.class);
    	 interfaceFactory = DevonCicdApplication.applicationContext.getBean(InterfaceFactory.class);
    }
    
    public Map<String, Object> tokenInfo(String token) throws Exception {
        try {
        	SourceRepoConfig repoConfig = ThreadContext.getSourceRepoConfig();
        	Map<String, Object> config = repoConfig.getConfigs();
            Map<String, Object> map = new HashMap<String, Object>();

            JWSObject jwsObject = JWSObject.parse(token);
            JSONObject jsonPayload = jwsObject.getPayload().toJSONObject();
            JWTClaimsSet jwtClaimsSet = JWTClaimsSet.parse(jsonPayload);

            String jwtIssuer = jwtClaimsSet.getIssuer();
            if (!jwtIssuer.equals((String)config.get("jwtIssuer"))) {
                throw new RuntimeException("Invalid issuer");
            }

            boolean validated = this.verifyWithKey(jwsObject, (String)config.get("jwtKey"));
            if (!validated) {
                throw new RuntimeException("Invalid token secret");
            }

            //코드의 발급시간을 확인한다.
            Date currentTime = new Date();
            Date expirationTime = jwtClaimsSet.getExpirationTime();
            long diff = (long) Math.floor((expirationTime.getTime() - currentTime.getTime()) / 1000);

            if (diff <= 0) {
                throw new RuntimeException("requested access_token has expired");
            } else {
                map.put("expires_in", diff);
            }

            Map<String, Object> claims = jwtClaimsSet.getClaims();
            map.putAll(claims);

            return map;
        } catch (Exception ex) {
            throw new RuntimeException("Validate token failed", ex);
        }
    }

    @SuppressWarnings("unchecked")
	public Map<String, Object> login(Map<String, Object> input) throws Exception {
    	SourceRepoConfig repoConfig = CicdContext.instance().getSourceRepoConfig((String)input.get("repositoryId"));
    	Map<String, Object> config = repoConfig.getConfigs();
    	String username = (String)input.get("userId");
    	
        ResourceOwnerPasswordCredentials credentials = new ResourceOwnerPasswordCredentials();
        credentials.setUsername(username);
        credentials.setPassword((String)input.get("password"));
        credentials.setClient_id((String)config.get("apiKey"));
        credentials.setClient_secret((String)config.get("apiSecret"));

		Map<String, Object> params = objectMapper.convertValue(credentials, Map.class);

        String queryString = HttpUtils.createPOSTQueryString(params);

        HttpUtils httpUtils = new HttpUtils();
        String url = (String)config.get("host") + "/oauth/token";

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");

        HttpResponse response = httpUtils.makeRequest("POST", url, queryString, headers);

        HttpEntity entity = response.getEntity();
        int statusCode = response.getStatusLine().getStatusCode();
        String responseText = EntityUtils.toString(entity);

        if (statusCode != 200) {
        	logger.error("Login Fail:" + username + " (" + statusCode + ")");
            throw new RuntimeException("login failed");
        }
    	logger.info("Login Success:" + username + " (" + responseText + ")");
    	//response.response.
        
    	GitLabApi gitLabApi = (GitLabApi)interfaceFactory.getSourceRepoApi(repoConfig);
    	
        //if 200, get gitlab id
        User user = gitLabApi.getUserApi().getUser(username);
        LogonUser lognUser = new LogonUser();
        lognUser.setId(user.getId().toString());
        lognUser.setEmail(user.getEmail());
        lognUser.setUserName(username);
        lognUser.setName(user.getName());

        String personalToken = processToken(lognUser, repoConfig);
        //response
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("personalToken", personalToken);
        result.put("logonUser", lognUser);
        
        /*
        //create jwt token.
        @SuppressWarnings("rawtypes")
		Map responseMap = objectMapper.readValue(responseText, Map.class);
        String refresh_token = responseMap.get("refresh_token").toString();
        String scope = responseMap.get("scope").toString();
        Long lifetime = 72000L;

        String jwtToken = this.generateJWTToken(config, lognUser, refresh_token, scope, lifetime);  
     
        result.put("accessToken", jwtToken);
        result.put("expiresIn", lifetime);
        result.put("refreshToken", refresh_token);
        */
        return result;
    }

    @SuppressWarnings("unused")
	private String generateJWTToken(Map<String, Object> config, LogonUser logonUser, String refresh_token, String scope, Long lifetime) throws Exception {
        //logon time
        Date issueTime = new Date();
        // expire Time
        Date expirationTime = new Date(new Date().getTime() + lifetime * 1000);
        // Context config
        Map<String, Object> context = new HashMap<String, Object>();
        context.put("clientKey", (String)config.get("apiKey"));
        context.put("refreshToken", refresh_token);

        context.put("userName", logonUser.getUserName());
        context.put("user", logonUser);

        //Put scopes
        List<String> scopes = Arrays.asList(scope.split(","));
        context.put("scopes", scopes);


        JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder();
        JWTClaimsSet claimsSet = builder
                .issuer((String)config.get("jwtIssuer"))
                .issueTime(issueTime)
                .expirationTime(expirationTime)
                .claim("context", context)
                .build();

        //Algorism
        JWSSigner signer = new MACSigner((String)config.get("jwtKey"));
        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);
        signedJWT.sign(signer);
        return signedJWT.serialize();
    }

    private boolean verifyWithKey(JWSObject jwsObject, String HS256SecretKey) throws Exception {
        JWSVerifier verifier = new MACVerifier(HS256SecretKey);
        return jwsObject.verify(verifier);
    }
    
    @SuppressWarnings("rawtypes")
	private String processToken(LogonUser user, SourceRepoConfig config) throws Exception{
    	TokenWrapper tokenWrapper = DevonCicdApplication.applicationContext.getBean(TokenWrapper.class);
    	String repositoryId = config.getRepositoryId();
    	String userName = user.getUserName();
    	
      	GitlabExtentApi extentApi = new GitlabExtentApi(config);
        Map<String, Object> tokenMap = tokenWrapper.selectToken(repositoryId, userName);
    	boolean isUpdate = false;
    	int id = Integer.parseInt(user.getId());
    	
        if(tokenMap != null && tokenMap.size() > 0) {
    		isUpdate = true;
    		if(extentApi.checkPersonalToken(id, Integer.parseInt((String)tokenMap.get("tokenId")))) {
    			return (String)tokenMap.get("token");
    		}
    	}else {
    		extentApi.findAndDeletePersonalToken(id);
    	}
        
    	Map makeMap = extentApi.createPersonalToken(id);
    	if(makeMap == null || makeMap.size() == 0) {
    		return null;
    	}
    	if(tokenMap == null) {
    		tokenMap = new HashMap<String, Object>(5);
    	}
    	tokenMap.put("repositoryId", repositoryId);
    	tokenMap.put("userName", userName);
    	tokenMap.put("tokenId", ""+ makeMap.get("id"));
      	tokenMap.put("token", makeMap.get("token"));

    	if(isUpdate) {
    		tokenWrapper.updateToken(tokenMap);
    	}else {
    		tokenWrapper.insertToken(tokenMap);
    	}
    	
    	return (String)makeMap.get("token");
    }

	@Override
	public String getAuthority(String userId) throws Exception {
		return Constants.AUTHORITY_ADMIN;
	}
}