package devon.cicd.resource.git.logon;

import lombok.Data;

@Data
public class ResourceOwnerPasswordCredentials {
    private String client_id;
    private String client_secret;
    private String grant_type = "password";
    private String username;
    private String password;
}
