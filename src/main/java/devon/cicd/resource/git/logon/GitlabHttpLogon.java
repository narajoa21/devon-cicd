package devon.cicd.resource.git.logon;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.config.CookieSpecs;

import devon.cicd.config.CicdConfig;
import devon.cicd.httpclient.BaseClient;
import devon.cicd.httpclient.HttpRequestData;
import devon.cicd.logon.HttpLogon;

public class GitlabHttpLogon implements HttpLogon {
	CicdConfig config;
	
	public GitlabHttpLogon(CicdConfig config) {
		this.config = config;
	}
	
	@Override
	public Map<String, Object> login() throws Exception {
		BaseClient client = new BaseClient(CookieSpecs.STANDARD, true, null);
		String host = (String)config.getConfigs().get("host");
		
		HttpRequestData request = new HttpRequestData();
		request.setPost(false);
		request.setUrl(host + "/users/sign_in");
		if(!client.processHttp(request)) {
			return null;
		}

		request = new HttpRequestData();
		request.setPost(true);
		request.setUrl(host + "/users/sign_in");
		
		request.addMapping("authenticity_token", "\"authenticity_token\" value=\"", "\"", 0);
		request.addParameter("user[login]", (String)config.getConfigs().get("userId"));
		request.addParameter("user[password]", (String)config.getConfigs().get("password"));
		request.addParameter("user[remember_me]", "0");
		
		String body = client.getBodyData(request);
		body = "utf8=%E2%9C%93&" + body;
		request.setBody(body);
		request.setBodyType("form");

		String hostHeader = client.getHostParam(host);
		request.addHeader("Host", hostHeader);
		//request.addHeader("Content-Type", "application/x-www-form-urlencoded"); 
		request.addHeader("Referer", host + "/users/sign_in");
		request.addHeader("Upgrade-Insecure-Requests", "1");
		request.addHeader("Origin", "http://ci.lgcicd.net:6060");
		client.processHttp(request);
		
		Map<String, Object> logonMap = new HashMap<String, Object>(2);
		logonMap.put("_gitlab_session", client.getCookieValue("_gitlab_session"));
		logonMap.put("host", hostHeader);
		
		return logonMap;
	}
}
