package devon.cicd.resource.git;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gitlab4j.api.Constants.PipelineOrderBy;
import org.gitlab4j.api.Constants.SortOrder;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.PipelineApi;
import org.gitlab4j.api.models.Pipeline;
import org.gitlab4j.api.models.PipelineStatus;

import devon.cicd.DevonCicdApplication;
import devon.cicd.config.CicdConfig;
import devon.cicd.config.CicdConfigManager;
import devon.cicd.config.Constants;
import devon.cicd.ctx.ThreadContext;
import devon.cicd.data.KeyValue;
import devon.cicd.factory.InterfaceFactory;
import devon.cicd.resource.PipelineJob;
import devon.cicd.util.CollectionUtils;
import devon.cicd.util.DataUtils;
import devon.cicd.util.DateUtils;

public class GitlabPipelineJob implements PipelineJob {
    private GitLabApi gitLabApi;
    private InterfaceFactory interfaceFactory;
    @SuppressWarnings("unused")
	private int projectId;
    
    public GitlabPipelineJob(String solutionCode, String regionName) {
    	interfaceFactory = DevonCicdApplication.applicationContext.getBean(InterfaceFactory.class);
    	Map<String, Object> input = new HashMap<String, Object>(2);
    	input.put("solutionCode", solutionCode);
    	input.put("stageCode", null);
    	input.put("region", "regionName");
    	
    	projectId = Integer.parseInt(ThreadContext.getUserContext().getProjectId());
    	gitLabApi = (GitLabApi)interfaceFactory.getResourceApi(input);
    }
    
	@SuppressWarnings("rawtypes")
	public List<Map<String, Object>> getPipeLineLogs(String branchName) throws Exception{
 		PipelineApi pipelineApi = gitLabApi.getPipelineApi();
 		int projectId = Integer.parseInt((String)ThreadContext.getUserContext().getProjectId());

 		List<Pipeline> pipelines = pipelineApi.getPipelines(projectId, null, null, branchName, false, null, null, PipelineOrderBy.ID, SortOrder.DESC, 0, Constants.LIST_MEDIUM_SIZE);
		if(pipelines == null || pipelines.size() == 0) {
			return null;
		}
		
		List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>(pipelines.size());
		Pipeline pipeline;
		Map<String, Object> log;

		Map commit;
		GitlabExtentApi gitlabExtentApi = null;
		
		Map<String, Map> cache = new HashMap<String, Map>(5);
		for(int inx = 0; inx < pipelines.size(); inx++) {
			pipeline = pipelines.get(inx);
			
			log = new HashMap<String, Object>(10);
			
			pipeline = pipelineApi.getPipeline(projectId, pipeline.getId());
			log.put("id", pipeline.getId());
			log.put("status", pipeline.getStatus().toString());
			log.put("startTime", DateUtils.getYYYYMMDDHHMMSS(pipeline.getStarted_at()));
			log.put("duration", pipeline.getDuration());
			log.put("branchName",  pipeline.getRef());
			log.put("commitId", pipeline.getSha());
			log.put("userName", pipeline.getUser().getUsername());
			log.put("user", pipeline.getUser().getName());

			if(pipeline.getCommitted_at() == null) {
				if(cache.size() <= 5) {
					commit = cache.get((String)pipeline.getSha());
					if(commit == null) {
						if(gitlabExtentApi == null) {
							gitlabExtentApi = new GitlabExtentApi();
						}
						commit = gitlabExtentApi.getCommitsByCommitId(pipeline.getSha());
						cache.put((String)pipeline.getSha(), commit);
					}
					if(commit != null) {
						log.put("commitTime", DateUtils.arangeGitlabDate((String)commit.get("committed_date")));
					}
				}
			}else {
				log.put("commitTime", DateUtils.getYYYYMMDDHHMMSS(pipeline.getCommitted_at()));
			}

			returnList.add(log);		
		}	
		return returnList;		
	}
	
	@SuppressWarnings("unchecked")
	public List<KeyValue> getPipeLineStats(String branchName) throws Exception{
 		PipelineApi pipelineApi = gitLabApi.getPipelineApi();
 		int projectId = Integer.parseInt((String)ThreadContext.getUserContext().getProjectId());

 		List<Pipeline> pipelines = pipelineApi.getPipelines(projectId, null, null, branchName, false, null, null, PipelineOrderBy.ID, SortOrder.DESC, 0, Constants.LIST_LARGE_SIZE);
		if(pipelines == null || pipelines.size() == 0) {
			return null;
		}
		
		long currentTime = System.currentTimeMillis();
		List<KeyValue> statsList = DataUtils.makeDays(currentTime, "yyyy-MM-dd", Constants.DASHBOARD_STATS_DAYS);
		
		KeyValue keyValue;
		List<KeyValue> list;
		for(int inx = 0; inx < statsList.size(); inx++) {
			keyValue = statsList.get(inx);
			list = new ArrayList<KeyValue>(3);
			list.add(DataUtils.makeKeyValue("success", 0));
			list.add(DataUtils.makeKeyValue("failed", 0));
			list.add(DataUtils.makeKeyValue("canceled", 0));
			list.add(DataUtils.makeKeyValue("etc", 0));
			keyValue.setValue(list);
		}
		
		Map<String, KeyValue> searchs = CollectionUtils.convertListToMapByKeyValue(statsList);
		String since = DateUtils.getISO8601Format(new Date(System.currentTimeMillis() - (86400000L * (Constants.DASHBOARD_STATS_DAYS - 1))));
		since = since.substring(0, since.indexOf('T'));
		
		Pipeline pipeline;
		String date;
		String status;
		for(int inx = 0; inx < pipelines.size(); inx++) {
			pipeline = pipelines.get(inx);			
			pipeline = pipelineApi.getPipeline(projectId, pipeline.getId());
			date = DateUtils.getYYYYMMDD(pipeline.getStarted_at());
			if(date.compareTo(since) < 0) {
				break;
			}
			keyValue = searchs.get(date);
			list = (List<KeyValue>)keyValue.getValue();
			status = pipeline.getStatus().toString();
			if("success".equals(status)) {
				list.get(0).setValue((Integer)list.get(0).getValue() + 1);
			}else if("failed".equals(status)) {
				list.get(1).setValue((Integer)list.get(1).getValue() + 1);				
			}else if("canceled".equals(status)) {
				list.get(2).setValue((Integer)list.get(2).getValue() + 1);				
			}else {
				list.get(3).setValue((Integer)list.get(3).getValue() + 1);				
			}
 		}	
		return statsList;		
	}

	public Map<String, Object> getDefine(Map<String, Object> input) throws Exception{
		GitlabExtentApi gitlabExtentApi = new GitlabExtentApi();
		input.put("filePath", "/" + (String)input.get("pipeline"));
		String contents = gitlabExtentApi.getRepositoryFile(input);
		Map<String, Object> reContents = new HashMap<String, Object>(1);
		reContents.put("define", contents);
		return reContents;
	}
	
	@SuppressWarnings("rawtypes")
	public Map<String, Object> runPipeline(Map<String, Object> input) throws Exception{
		CicdConfigManager cicdConfigManager = (CicdConfigManager)DevonCicdApplication.applicationContext.getBean(CicdConfigManager.class);
		CicdConfig cicdConfig = cicdConfigManager.readCicdConfig("P", (String)input.get("stageCode"));
		if(cicdConfig == null) {
			throw new RuntimeException("Config do not exist! (P," + (String)input.get("stageCode") + ")");
		}
		
		String triggerToken = (String)cicdConfig.getConfigs().get("triggerToken");
		GitlabExtentApi gitlabExtentApi = new GitlabExtentApi();
		if(triggerToken == null || triggerToken.length() == 0) {
			triggerToken = gitlabExtentApi.getProjectTriggerToken((String)cicdConfig.getConfigs().get("triggerName"));
		}
		if(triggerToken == null) {
			throw new RuntimeException("Trigger token do not exist!");
		}

		@SuppressWarnings("unchecked")
		Map pipeline = gitlabExtentApi.triggerPipeline(triggerToken,(String)input.get("branchName"), (Map)input.get("params"));
		Map<String, Object> reReult = new HashMap<String, Object>(10);
		reReult.put("id", pipeline.get("id"));
		reReult.put("status", ((String)pipeline.get("status")).toUpperCase());
		reReult.put("startTime", DateUtils.arangeGitlabDate((String)pipeline.get("started_at")));
		reReult.put("duration", pipeline.get("duration"));
		reReult.put("branchName", pipeline.get("ref"));
		reReult.put("commitId", pipeline.get("sha"));
		reReult.put("userName", ((Map)pipeline.get("user")).get("username"));
		
		Map commit = gitlabExtentApi.getCommitsByCommitId((String)pipeline.get("sha"));
		if(commit != null) {
			reReult.put("commitTime", DateUtils.arangeGitlabDate((String)commit.get("committed_date")));
		}
		return reReult;
	}

	@Override
	public Map<String, Object> isRunningPipeline(Map<String, Object> input) throws Exception {
 		PipelineApi pipelineApi = gitLabApi.getPipelineApi();
 		int projectId = Integer.parseInt((String)ThreadContext.getUserContext().getProjectId());

 		List<Pipeline> pipelines = pipelineApi.getPipelines(projectId, null, null, (String)input.get("branchName"), false, null, null, PipelineOrderBy.ID, SortOrder.DESC, 0, 5);
		Map<String, Object> status = new HashMap<String, Object>(2);
		
		status.put("status", Constants.RESOURCE_STATUS_COMPLETED);
		if(pipelines == null || pipelines.size() == 0) {	
			return status;
		}

		PipelineStatus pipelineStatus;
		for(Pipeline pipeline: pipelines) {
			pipelineStatus = pipeline.getStatus();
			if(pipelineStatus == PipelineStatus.PENDING || pipelineStatus == PipelineStatus.RUNNING) {
				status.put("status", Constants.RESOURCE_STATUS_RUNNING);
				status.put("id", pipeline.getId());
				pipeline = pipelineApi.getPipeline(Integer.parseInt((String)ThreadContext.getUserContext().getProjectId()), pipeline.getId());
				status.put("duration", (int)((System.currentTimeMillis() - pipeline.getStarted_at().getTime())/1000));
				break;
			}
		}
		return status;
	}
}
