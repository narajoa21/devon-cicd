package devon.cicd.resource.git;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gitlab4j.api.models.Member;

import devon.cicd.config.Constants;
import devon.cicd.util.DateUtils;

public class GitlabDataConverter {
	@SuppressWarnings("rawtypes")
	static public void setUserInfo(Map<String, Object> target, Map source) {
		target.put("userId", source.get("id"));
		target.put("name", source.get("name"));
		target.put("userName", source.get("username"));
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	static public Map<String, Object> convertMergeRequestToMap(Map mr){
		Map<String, Object> reMr = new HashMap<String, Object>(20);
		reMr.put("requestId", mr.get("id"));
		reMr.put("requestIid", mr.get("iid"));
		reMr.put("title", mr.get("title"));
		reMr.put("description", mr.get("description"));
		reMr.put("createTime", DateUtils.arangeGitlabDate((String)mr.get("created_at")));
		reMr.put("sourceBranchName", mr.get("source_branch"));
		reMr.put("targetBranchName", mr.get("target_branch"));
		reMr.put("state", mr.get("state"));

		Map<String, Object >data = new HashMap<String, Object>(3);
		Map mrData = (Map)mr.get("author");
		setUserInfo(data, mrData);
		reMr.put("author", data);
		
		mrData = (Map)mr.get("assignee");
		if(mrData != null && mrData.size() > 0) {
			data = new HashMap<String, Object>(3);
			setUserInfo(data, mrData);
			reMr.put("assignee", data);
		}
		
		data = new HashMap<String, Object>(3);
		List<Map> mrDatas = (List<Map>)mr.get("assignees");
		
		if(mrDatas != null && mrDatas.size() > 0) {
			List<Map<String, Object>> datas = new ArrayList<Map<String, Object>>(mrDatas.size());
			for(int i = 0; i < mrDatas.size(); i++) {
				mrData = mrDatas.get(i);
				data = new HashMap<String, Object>(3);
				setUserInfo(data, mrData);
				datas.add(data);
			}
			reMr.put("assignees", datas);
		}
		
		reMr.put("inProgress", mr.get("work_in_progress"));
		reMr.put("sourceProjectId", mr.get("source_project_id"));
		reMr.put("targetProjectId", mr.get("target_project_id"));
		reMr.put("commitId", mr.get("sha"));
		reMr.put("mergeCommitId", mr.get("merge_commit_sha"));
		return reMr;
	}
	
	static public Map<String, Object> convertMemberToMap(Member member){
		Map<String, Object> reMember = new HashMap<String, Object>(6);
		reMember.put("userId", member.getId());
		reMember.put("userName", member.getUsername());
		reMember.put("name", member.getName());
		reMember.put("state", member.getState());
		if(member.getCreatedAt() != null) {
			reMember.put("createTime", DateUtils.getYYYYMMDDHHMMSS(member.getCreatedAt()));
		}
		if(member.getAccessLevel() != null) {
			reMember.put("authority", getAuthorityLevel(member.getAccessLevel().toValue()));
		}		
		return reMember;
	}
	
	static private String getAuthorityLevel(int access_level) {
		switch(access_level) {
		case 10:
			return Constants.AUTHORITY_GUEST;
		case 20:
			return Constants.AUTHORITY_REPORTER;
		case 30:
			return Constants.AUTHORITY_DEVELOPER;
		case 40:
			return Constants.AUTHORITY_MAINTAINER;
		case 50:
			return Constants.AUTHORITY_OWNER;
		default:
			throw new RuntimeException("Gitlab user's access_level(" + access_level + ") is a abnormal value");
		}		
	}	
}
