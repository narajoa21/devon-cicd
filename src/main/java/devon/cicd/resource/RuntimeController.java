package devon.cicd.resource;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/runtime/")
public class RuntimeController {
	@Autowired
	RuntimeManager runManager;
	
    @GetMapping(value = "/region/{region}", produces = "application/json;charset=UTF-8")
    public Object getRgion(@PathVariable("region") String region) throws Exception {
    	return runManager.getRegion(region);
    }
	
    @PostMapping(value = "clusters", produces = "application/json;charset=UTF-8")
    public Object getClusters(@RequestBody Map<String, Object> body) throws Exception {
    	return runManager.getClusters(body);
    }
 
    @PostMapping(value = "service", produces = "application/json;charset=UTF-8")
    public Object getService(@RequestBody Map<String, Object> body) throws Exception {
    	return runManager.getService(body);
    }

    @PostMapping(value = "tasks", produces = "application/json;charset=UTF-8")
    public Object getTasks(@RequestBody Map<String, Object> body) throws Exception {
    	return runManager.getTasks(body);
    }
    
    @PostMapping(value = "serviceEvents", produces = "application/json;charset=UTF-8")
    public Object getServiceEvents(@RequestBody Map<String, Object> body) throws Exception {
    	return runManager.getServiceEvents(body);
    }

    @PostMapping(value = "scaleService", produces = "application/json;charset=UTF-8")
    public Object scaleService(@RequestBody Map<String, Object> body) throws Exception {
    	return runManager.scaleService(body);
    }

    @PostMapping(value = "stopTask", produces = "application/json;charset=UTF-8")
    public Object stopTask(@RequestBody Map<String, Object> body) throws Exception {
    	return runManager.stopTask(body);
    }

    @PutMapping(value = "replaceImage", produces = "application/json;charset=UTF-8")
    public Object replaceImage(@RequestBody Map<String, Object> body) throws Exception {
    	return runManager.replaceImage(body);
    }
    
    @PostMapping(value = "isRunningDeployment", produces = "application/json;charset=UTF-8")
    public Object isRunningDeployment(@RequestBody Map<String, Object> body) throws Exception {
    	return runManager.isRunningDeployment(body);
    }	   
}