package devon.cicd.resource;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import devon.cicd.ctx.CicdContext;

@RestController
@RequestMapping("/source/")
public class SourceController {
	@Autowired
	SourceManager sourceManager; 
	
    @GetMapping(value = "repositories", produces = "application/json")
    public Object getRepostories() throws Exception {
    	return CicdContext.instance().getSourceRepositories();
    }

    @GetMapping(value = "projects", produces = "application/json")
    public Object getMemberProjects() throws Exception{
    	return sourceManager.getProjects();
    } 

    @GetMapping(value = "projectMembers", produces = "application/json")
    public Object getProjectMembers() throws Exception{
    	return sourceManager.getProjectMembers();
    }
 
    @GetMapping(value = "projectCurrentUser", produces = "application/json")
    public Object getProjectCurrentUser() throws Exception{
    	return sourceManager.getProjectCurrentUser();
    }
    
    @PostMapping(value="currentProject", produces="application/json")
    public Object postCurrentProject(@RequestBody Map<String, Object> body) throws Exception{
    	return sourceManager.postCurrentProject(body);
    }   

    @GetMapping(value = "/commits/{branchName}", produces = "application/json;charset=UTF-8")
    public Object getCommits(@PathVariable("branchName") String branchName) throws Exception {
    	return sourceManager.getCommitsByBranch(branchName);
    }

    @GetMapping(value = "/files/{id}", produces = "application/json;charset=UTF-8")
    public Object getFilesByCommit(@PathVariable("id") String id) throws Exception {
    	return sourceManager.getFilesByCommit(id);
    }
    
    @PostMapping(value = "file/contents", produces = "application/json;charset=UTF-8")
    public Object getFilesContents(@RequestBody Map<String, Object> body) throws Exception {
    	return sourceManager.getFilesContents(body);
    } 
    
    @PostMapping(value = "approvalUsers", produces = "application/json;charset=UTF-8")
    public Object getApprovalUsers(@RequestBody Map<String, Object> body) throws Exception {
    	return sourceManager.getApprovalUsers(body);
    }
    
    @PostMapping(value = "mergeRequests", produces = "application/json;charset=UTF-8")
    public Object getMergeRequest(@RequestBody Map<String, Object> body) throws Exception {
    	return sourceManager.getMergeRequests(body);
    }
    
    @PostMapping(value = "createMergeRequest", produces = "application/json;charset=UTF-8")
    public Object createRequest(@RequestBody Map<String, Object> body) throws Exception {
    	return sourceManager.createMergeRequest(body);
    }
    
    @PostMapping(value = "acceptMergeRequest", produces = "application/json;charset=UTF-8")
    public Object acceptMergeRequest(@RequestBody Map<String, Object> body) throws Exception {
    	return sourceManager.acceptMergeRequest(body);
    }
    
    @PostMapping(value = "cherryPickRequest", produces = "application/json; charset=UTF-8")
    public Object postCherryPickRequest(@RequestBody Map<String, Object> body) throws Exception{
    	System.out.println("controller");
    	return sourceManager.cherryPickRequest(body);
    }
    
}