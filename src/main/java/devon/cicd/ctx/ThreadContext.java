package devon.cicd.ctx;

import javax.servlet.http.HttpSession;

import devon.cicd.config.Constants;
import devon.cicd.config.SourceRepoConfig;

public class ThreadContext {
	private static final ThreadLocal<HttpSession> threadLocal = new ThreadLocal<HttpSession>();
	
	static public void setHttpSession(HttpSession httpSession) {
		threadLocal.set(httpSession);
	}
	
	static public void clearHttpSession() {
		threadLocal.set(null);
	}
	
	static public HttpSession getHttpSesson() {
		return threadLocal.get();
	}
	
	static public void setUserContext(UserContext userContext) {
		HttpSession httpSession = getHttpSesson();
		if(httpSession == null) {
			return;
		}
		httpSession.setAttribute(Constants.USER_CONTEXT, userContext);
	}
	
	static public UserContext getUserContext() {
		HttpSession httpSession = getHttpSesson();
		if(httpSession == null) {
			return null;
		}
		return (UserContext)httpSession.getAttribute(Constants.USER_CONTEXT);
	}
	
	static public SourceRepoConfig getSourceRepoConfig() {
		UserContext userContext = getUserContext();
		if(userContext == null) {
			return null;
		}
		return CicdContext.instance().getSourceRepoConfig(userContext.getSourceRepositoryId());
	}
	
	static public String getSessionId() {
		HttpSession httpSession = getHttpSesson();
		if(httpSession == null) {
			return null;
		}
		return httpSession.getId();
	}
}
