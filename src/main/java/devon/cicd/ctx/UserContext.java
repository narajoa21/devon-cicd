package devon.cicd.ctx;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class UserContext implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String userName;
	private String userPersonalToken;
	private String password;
	private String athority;
	
	private String systemId;
	private String sourceRepositoryId;
	private String subSystemId;
	private String sourceRepository;
	private String sourceRepositoryUrl;
	private String projectId;
	
	private Map<String, Object> solutionHttpSessions;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAthority() {
		return athority;
	}
	public void setAthority(String athority) {
		this.athority = athority;
	}
	
	
	public String getSourceRepositoryId() {
		return sourceRepositoryId;
	}
	public void setSourceRepositoryId(String sourceRepositoryId) {
		this.sourceRepositoryId = sourceRepositoryId;
	}
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public String getSubSystemId() {
		return subSystemId;
	}
	public void setSubSystemId(String subSystemId) {
		this.subSystemId = subSystemId;
	}
	public String getSourceRepository() {
		return sourceRepository;
	}
	public void setSourceRepository(String sourceRepository) {
		this.sourceRepository = sourceRepository;
	}
	public String getSourceRepositoryUrl() {
		return sourceRepositoryUrl;
	}
	public void setSourceRepositoryUrl(String sourceRepositoryUrl) {
		this.sourceRepositoryUrl = sourceRepositoryUrl;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public void addSolutionHttpSession(String solutionCode, Object value) {
		if(solutionHttpSessions == null) {
			solutionHttpSessions = new HashMap<String, Object>(5);
		}
		solutionHttpSessions.put(solutionCode, value);
	}
	public Object getSolutionHttpSession(String solutionCode) {
		if(solutionHttpSessions == null) {
			return null;
		}
		return solutionHttpSessions.get(solutionCode);
	}
	public String getUserPersonalAccessToken() {
		return userPersonalToken;
	}
	public void setUserPersonalAccessToken(String userPersonalToken) {
		this.userPersonalToken = userPersonalToken;
	}
}
