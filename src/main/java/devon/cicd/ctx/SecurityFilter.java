package devon.cicd.ctx;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import devon.cicd.config.Constants;
import devon.cicd.util.JsonUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;

public class SecurityFilter implements Filter
{
	private	static final String [] skipContents = {"/css/", "/js/", "/img/", "/fonts/", "/auth/login", "/source/repositories" };

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException
	{
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		//req.setCharacterEncoding("UTF-8");
		
		String loginUri = "/auth/login";
		String uri = req.getRequestURI();
		try {
			HttpSession session = null;
			if(isSkipContents(uri)) {
				if(uri.indexOf(loginUri) < 0) {
					chain.doFilter(request, response);
				}else {
					session = req.getSession(true);
					UserContext userContext = (UserContext)session.getAttribute(Constants.USER_CONTEXT);
					if(userContext != null) {
						redirectPage(res, "/admin/dashboard");
						return;
					}
					ThreadContext.setHttpSession(session);
					chain.doFilter(request, response);
					userContext = (UserContext)session.getAttribute(Constants.USER_CONTEXT);
					if(userContext == null) {
						session.invalidate();
					}
				}
			}else {
				if(uri.indexOf("/auth/logout")>=0) {
					session = req.getSession(false);
					if(session != null) {
						String sessionId = session.getId();
						session.invalidate();
						CicdContext.instance().removeResourceApi(sessionId);
					}
					redirectPage(res, loginUri);
				}else {
					session = req.getSession(false);
					if(session == null) {
						redirectPage(res, loginUri);
						return;
					}
					UserContext userContext = (UserContext)session.getAttribute(Constants.USER_CONTEXT);
					if(userContext == null) {
						redirectPage(res, loginUri);					
					}else {
						ThreadContext.setHttpSession(session);
						chain.doFilter(request, response);
					}
				}
			}
		}finally{
			ThreadContext.clearHttpSession();
		}
	}

	private boolean isSkipContents(String uri) {
		for(String path : skipContents) {
			if(uri.indexOf(path)>=0) {
				return true;
			}
		}
		return false;
	}
	
	private void redirectPage(HttpServletResponse res, String path) throws IOException {
		res.setHeader("Content-Type","application/json");
		Map<String, String> map = new HashMap<String, String>();
		map.put("routerMove", path);
		res.getWriter().write(JsonUtils.marshal(map));
	}
	
	@Override
	public void destroy()
	{
	}
}