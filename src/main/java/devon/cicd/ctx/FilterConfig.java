package devon.cicd.ctx;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig
{
	@Bean
	public FilterRegistrationBean<SecurityFilter> getFilterRegistrationBean()
	{
		FilterRegistrationBean<SecurityFilter> registrationBean = new FilterRegistrationBean<SecurityFilter>();
		registrationBean.setFilter(new SecurityFilter());
		registrationBean.addUrlPatterns("/*");
		return registrationBean;
	}
}