package devon.cicd.ctx;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import devon.cicd.DevonCicdApplication;
import devon.cicd.config.CicdConfigManager;
import devon.cicd.config.SourceRepoConfig;
import devon.cicd.resource.CachedResourceApi;

public class CicdContext {
	static private final Log logger = LogFactory.getLog(CicdContext.class);
	static private CicdContext context;
	
	private String cicdAdmin;
	private List<Map<String, Object>> sourceRepositories;
	private Map<String, SourceRepoConfig> sourceRepoConfigs;
	private Map<String, Map<String, Object>> sourceUsers;
	
	private Map<String, Thread> daemons;  // key threadName
	// 각종 API 정보
	private Map<String, Map<String, CachedResourceApi>> resourceApis;  // Key SessionId
	
	public static CicdContext instance() {
		if(context == null) {
			context = new CicdContext();
		}
		return context;
	}
	
	public String getCicdAdmin() {
		return cicdAdmin;
	}

	public void setCicdAdmin(String cicdAdmin) {
		this.cicdAdmin = cicdAdmin;
	}
	
	public  List<Map<String, Object>> getSourceRepositories() {
		return sourceRepositories;
	}

	public void setSourceRepositories(List<Map<String, Object>> sourceRepositories) {
		this.sourceRepositories = sourceRepositories;
	}
	
	public SourceRepoConfig getSourceRepoConfig(String repositoryId) {
		if(sourceRepoConfigs == null) {
			Map<String, Object> input = new HashMap<String, Object>(1);
			input.put("repositoryId", repositoryId);
			CicdConfigManager cicdConfigManager = DevonCicdApplication.applicationContext.getBean(CicdConfigManager.class);
			SourceRepoConfig sourceRepoConfig = cicdConfigManager.readRepoConfig(input);
			if(sourceRepoConfig == null) {
				throw new RuntimeException("Can't read source repository info!");
			}else {
				addSourceRepoConfig(sourceRepoConfig);
			}
			return sourceRepoConfig;
		}
		return sourceRepoConfigs.get(repositoryId);
	}
	
	public void addSourceRepoConfig(SourceRepoConfig sourceRepoConfig) {
		if(sourceRepoConfigs == null) {
			sourceRepoConfigs = new Hashtable<String, SourceRepoConfig>();
		}
		sourceRepoConfigs.put(sourceRepoConfig.getRepositoryId(), sourceRepoConfig);
	}
	
	public void addDaemonThread(Thread daemon) {
		daemon.start();
		if(daemons == null) {
			daemons = new Hashtable<String, Thread>();
		}
		daemons.put(daemon.getName(), daemon);
	}
	
	public Thread getDaemonThread(String name) {
		if(daemons == null) {
			return null;
		}
		return daemons.get(name);
	}
	
	public void addResourceApi(String key, Object api) {
		logger.info("addResourceApi: " + key + "=>" + api);
		if(key == null || api == null) {
			return;
		}
		String sessionId = ThreadContext.getSessionId();
		if(sessionId == null) {
			return;
		}
		
		if(resourceApis == null) {
			resourceApis = new Hashtable<String, Map<String, CachedResourceApi>>();
		}
		Map<String, CachedResourceApi> apis = resourceApis.get(sessionId);
		if(apis == null) {
			apis = new Hashtable<String, CachedResourceApi>();
			resourceApis.put(sessionId, apis);
		}
		CachedResourceApi resourceApi = new CachedResourceApi();
		resourceApi.setApi(api);
		resourceApi.setLastAccessTime(System.currentTimeMillis());
		apis.put(key, resourceApi);
	}
	
	public Map<String, Map<String, CachedResourceApi>> getResourceApis(){
		return resourceApis;
	}
	
	public Object getResourceApi(String key) {
		if(key == null || resourceApis == null) {
			logger.info("getResourceApi(1): " + key + "=>" + null);
			return null;
		}
		String sessionId = ThreadContext.getSessionId();
		if(sessionId == null) {
			logger.info("getResourceApi(2): " + key + "=>" + null);
			return null;
		}
		
		Map<String, CachedResourceApi> apis = resourceApis.get(sessionId);
		if(apis == null) {
			logger.info("getResourceApi(3): " + key + "=>" + null);
			return null;
		}
		CachedResourceApi resourceApi = apis.get(key);
		if(resourceApi == null) {
			logger.info("getResourceApi(4): " + key + "=>" + null);
			return null;
		}
		resourceApi.setLastAccessTime(System.currentTimeMillis());
		logger.info("getResourceApi(5): " + key + "=>" + resourceApi.getApi());
		return resourceApi.getApi();
	}
	
	public void removeResourceApi(String sessionId) {
		if(sessionId == null || resourceApis == null) {
			return;
		}
		resourceApis.remove(sessionId);
		logger.info("removeResourceApi: " + sessionId);
	}
	
	public void addSourceUser(Map<String, Object> user) {
		if(sourceUsers == null) {
			sourceUsers = new Hashtable<String, Map<String, Object>>();
		}
		user.put("createTime", System.currentTimeMillis());
		sourceUsers.put((String)user.get("userName"), user);
	}
	
	public Map<String, Object> getSourceUser(String userName) {
		if(sourceUsers == null) {
			return null;
		}
		return sourceUsers.get(userName);
	}
}
