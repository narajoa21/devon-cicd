package devon.cicd.httpclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import devon.cicd.httpclient.HttpMapping;
import devon.cicd.httpclient.HttpResponseData;
import devon.cicd.util.StringUtils;

public class BaseClient {
    static private final Log logger = LogFactory.getLog(BaseClient.class);
	
   private HttpClient httpClient;
	private HttpResponseData preResponseData;
	private RequestConfig requestConfig;
	private CookieStore cookieStore;
	
	public BaseClient(String cookieSpec, boolean ignoreTLSValidation, String [] TLSVersion) throws Exception {
		requestConfig = RequestConfig.custom()
				  .setSocketTimeout(30000)
				  .setConnectTimeout(10000)
				  .setConnectionRequestTimeout(30000)
				  .setCookieSpec(cookieSpec)
				  .build();
		
		HttpClientBuilder builder =  HttpClientBuilder.create();
		
		if(ignoreTLSValidation) {
			SSLContext ctx = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
	            @Override
	            public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	                return true;
	            }
	        }).build();
	
	        ctx.init(new KeyManager[0], new TrustManager[] { new DefaultTrustManager()}, new SecureRandom());
	        SSLContext.setDefault(ctx);
	
	        HostnameVerifier hostnameVerifier = new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
	        };
	        
	        if(TLSVersion == null) {
				builder.setSSLContext(ctx);
				builder.setSSLHostnameVerifier(hostnameVerifier);
	        }else {
				SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(ctx, TLSVersion, null, hostnameVerifier);
	        	builder.setSSLSocketFactory(sslSocketFactory);
	        }
		}
	
        builder.setRedirectStrategy(new LaxRedirectStrategy());
        cookieStore = new BasicCookieStore();
        builder.setDefaultCookieStore(cookieStore);
        httpClient = builder.build();
	}

	public boolean processHttp(HttpRequestData command) throws Exception {
		Map<String,String> inParameters = command.getParameters();
		
		List<HttpMapping> mappings = command.getMappings();
		String body = command.getBody();
		
		if(mappings != null && preResponseData != null) {
			String value;
			if(command.getBody() == null) {
				for(HttpMapping mapping: mappings) {
					if(inParameters == null) {
						inParameters = new HashMap<String,String>(mappings.size());
					}
					
					value = StringUtils.getValueFromHTML(preResponseData.getResponseHtml(), mapping.getStart(), mapping.getEnd(), mapping.getIndex());
					if(value == null) {
						inParameters.put(mapping.getName(), "");
					}else {
						inParameters.put(mapping.getName(), value);					
					}
				}
			}else {
				for(HttpMapping mapping: mappings) {
					value = StringUtils.getValueFromHTML(preResponseData.getResponseHtml(), mapping.getStart(), mapping.getEnd(), mapping.getIndex());					
					if(value == null) {
						value ="";
					}
					body = StringUtils.replace(body, "{" + mapping.getName() +"}", value);
				}
			}
		}
		
		HttpResponseData responseData;
		if(body != null) {
			responseData = doPost(httpClient, command.getUrl(), command.getHeaders(), body, command.getBodyType());
		}else if(command.isPost()) {
			responseData = doPost(httpClient, command.getUrl(), command.getHeaders(), inParameters);
		}else {
			responseData = doGet(httpClient, command.getUrl(), command.getHeaders());
		}
		preResponseData = responseData;
		
		logger.debug("<Response Code>\r\n" + responseData.getResponseCode());
		logger.debug("<Response Body>");
		logger.debug(responseData.getResponseHtml());
		
		List<Integer> responses = command.getResponseCodes();
		if(responses != null) {
			boolean isSuccess = false;
			for(int responseCode : responses) {
				if(responseCode == responseData.getResponseCode()) {
					isSuccess = true;
					break;
				}
			}
			
			if(!isSuccess) {
				return false;
			}
		}else {
			if(responseData.getResponseCode() >= 400) {
				return false;
			}
		}
		
		String reponseHtml = responseData.getResponseHtml();
		List<String> successes = command.getSuccesses();
		
		if(reponseHtml == null) {
			if(successes != null) {
				return false;
			}
		}else if(successes != null){
			for(String value : successes) {
				if(reponseHtml.indexOf(value)>=0){
					return true;
				}
			}
			return false;
		}	
		return true;
	}
	
	private void setConfigChrome(HttpRequestBase request, Map<String, String> headers) throws IOException {
		request.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
		request.addHeader("Accept-Encoding", "gzip, deflate");
		request.addHeader("Accept-Language", "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7");
		request.addHeader("Connection", "keep-alive");
		request.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36");		

		if(headers != null) {
			Iterator<String> itor = headers.keySet().iterator();
			String headerName;
			while(itor.hasNext()) {
				headerName = itor.next();
				request.addHeader(headerName, headers.get(headerName));
			}
		}
		
		Header [] reqHeaders = request.getAllHeaders();
		if(headers != null) {
			logger.debug("<Request Headers>");
			
			for(Header header : reqHeaders) {
				logger.debug(header.getName() + ": " + header.getValue());
			}
		}
	}
	
	private HttpResponseData doGet(HttpClient httpClient, String url, Map<String, String> headers) throws UnsupportedOperationException, IOException {
		logger.debug("\r\n\r\nRequest-(get) " + url);
		HttpGet get = new HttpGet(url);
		get.setConfig(requestConfig);
		setConfigChrome(get, headers);

		return doReponse(httpClient.execute(get));
	}
	
	private HttpResponseData doPost(HttpClient httpClient, String url, Map<String, String> headers, String body, String bodyType) throws ClientProtocolException, IOException {
		logger.debug("\r\n\r\nRequest-(post Body) " + url);
		HttpPost post = new HttpPost(url);
		post.setConfig(requestConfig);
		setConfigChrome(post, headers);
		logger.debug("<Body - " + bodyType + ">");
		logger.debug(body);
		
		if("json".equalsIgnoreCase(bodyType)) {
			post.setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));
		}else if("xml".equalsIgnoreCase(bodyType)) {
			post.setEntity(new StringEntity(body, ContentType.APPLICATION_XML));			
		}else if("form".equalsIgnoreCase(bodyType)) {
			post.setEntity(new StringEntity(body, ContentType.APPLICATION_FORM_URLENCODED));			
		}else {
			post.setEntity(new StringEntity(body, ContentType.DEFAULT_TEXT));			
		}
		return doReponse(httpClient.execute(post));
	}
	
	private HttpResponseData doPost(HttpClient httpClient, String url, Map<String, String> headers, Map<String, String> data) throws ClientProtocolException, IOException {
		logger.debug("\r\n\r\nRequest-(post) " + url);
		HttpPost post = new HttpPost(url);
		post.setConfig(requestConfig);
		setConfigChrome(post, headers);
		
		UrlEncodedFormEntity entity = makeUrlEncodedFormEntity(data);
		if(entity != null) {
			post.setEntity(entity);
		}
		return doReponse(httpClient.execute(post));
	}
	
	private UrlEncodedFormEntity makeUrlEncodedFormEntity(Map<String, String> data) throws UnsupportedEncodingException {
		if(data != null) {		
			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			Iterator<String> itor = data.keySet().iterator();
			String key;
			String value;
			logger.debug("<Parameter>");
			while(itor.hasNext()) {
				key = itor.next();
				value = data.get(key);
				urlParameters.add(new BasicNameValuePair(key, value));
				logger.debug(key + ":" + value);
			}
			
			return new UrlEncodedFormEntity(urlParameters, "UTF-8");
		}
		return null;
	}
	
	public String getBodyData(HttpRequestData request) throws IOException {
		Map<String, String> data = request.getParameters();
		List<HttpMapping> mappings = request.getMappings();
		
		if(mappings != null) {
			String value;
			for(HttpMapping mapping: mappings) {
				if(data == null) {
					data = new HashMap<String,String>(mappings.size());
				}
				
				value = StringUtils.getValueFromHTML(preResponseData.getResponseHtml(), mapping.getStart(), mapping.getEnd(), mapping.getIndex());
				if(value == null) {
					data.put(mapping.getName(), "");
				}else {
					data.put(mapping.getName(), value);					
				}
			}
		}
		
		if(data != null) {
			UrlEncodedFormEntity entity = makeUrlEncodedFormEntity(data);
			if(entity != null) {
				byte [] datas = new byte[1024];
				int size = entity.getContent().read(datas);
				return new String(datas, 0, size, "UTF-8");		
			}
		}
		return null;
	}
	
	private HttpResponseData doReponse(HttpResponse response) throws IOException  {
		HttpResponseData httpResponseData = new HttpResponseData();
		httpResponseData.setResponseCode(response.getStatusLine().getStatusCode());
		
		Header [] headers = response.getHeaders("Set-Cookie");
		if(headers!= null && headers.length > 0) {
			for(Header header : headers) {
				logger.debug(header.getName() + ":" + header.getValue());
			}
		}
		if(response.getEntity().getContentEncoding() == null) {
			httpResponseData.setResponseHtml(EntityUtils.toString(response.getEntity(), "UTF-8"));
		}else {
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer(1024);
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line).append('\n');
			}
			httpResponseData.setResponseHtml(result.toString());			
		}
		
		return httpResponseData;
	}		
	
	
	public String getCookieValue(String name) {
		List<Cookie> cookies = cookieStore.getCookies();
		if(cookies != null) {
			for(Cookie cookie : cookies) {
				if(cookie.getName().equals(name)) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}
	
	private static class DefaultTrustManager implements X509TrustManager {
		@Override
		public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
				throws java.security.cert.CertificateException {
		}

		@Override
		public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
				throws java.security.cert.CertificateException {
		}

		@Override
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}
   
	}
	
	public String getHostParam(String host) {
		if(host == null) {
			return null;
		}
		int index = host.indexOf("://");
		String returnVal;
		if(index < 0) {
			returnVal = host;
		}else {
			returnVal = host.substring(index+3);
		}
		index = returnVal.indexOf('/');
		if(index > 0) {
			returnVal = returnVal.substring(0, index);
		}
		return returnVal;
	}
}
