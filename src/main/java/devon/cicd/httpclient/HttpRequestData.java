package devon.cicd.httpclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import devon.cicd.httpclient.HttpMapping;

public class HttpRequestData {
	private String url;
	private boolean isPost;
	private List<Integer> responseCodes;
	private List<HttpMapping> mappings;
	private List<String> successes;
	private Map<String,String> parameters;
	private Map<String,String> headers;
	private String body;
	private String bodyType;
	
	public HttpRequestData() {
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isPost() {
		return isPost;
	}

	public void setPost(boolean isPost) {
		this.isPost = isPost;
	}

	public List<Integer> getResponseCodes() {
		return responseCodes;
	}

	public void addResponseCode(int reponseCode) {
		if(responseCodes == null) {
			responseCodes = new ArrayList<Integer>(5);
		}
		responseCodes.add(reponseCode);
	}

	public List<HttpMapping> getMappings() {
		return mappings;
	}

	public void addMapping(String name, String start, String end, int index) {
		if(mappings == null) {
			mappings = new ArrayList<HttpMapping>(5);
		}
		HttpMapping mapping = new HttpMapping(name, start, end, index);
		mappings.add(mapping);
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void addParameter(String name, String value) {
		if(parameters == null) {
			parameters = new HashMap<String, String>(5);
		}
		parameters.put(name,  value);
	}

	public Map<String,String> getHeaders() {
		return headers;
	}

	public void addHeader(String name, String value) {
		if(headers == null) {
			headers = new HashMap<String, String>(5);
		}
		headers.put(name,  value);
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getBodyType() {
		return bodyType;
	}

	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	public List<String> getSuccesses() {
		return successes;
	}

	public void addSuccess(String message) {
		if(successes == null) {
			successes = new ArrayList<String>(2);
		}
		successes.add(message);
	}
	
	
}
