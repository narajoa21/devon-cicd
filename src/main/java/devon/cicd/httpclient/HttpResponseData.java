package devon.cicd.httpclient;

public class HttpResponseData {
	private int responseCode;
	private String responseHtml;
	
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseHtml() {
		return responseHtml;
	}
	public void setResponseHtml(String responseHtml) {
		this.responseHtml = responseHtml;
	}
}
