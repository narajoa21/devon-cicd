package devon.cicd.httpclient;

public class HttpMapping {
	private String name;
	private String start;
	private String end;
	private int index;
	
	public HttpMapping(String name, String start, String end, int index) {
		this.name = name;
		this.start = start;
		this.end = end;
		this.index = index;
	}
	
	public String getName() {
		return name;
	}
	public String getStart() {
		return start;
	}
	public String getEnd() {
		return end;
	}
	public int getIndex() {
		return index;
	}
}
