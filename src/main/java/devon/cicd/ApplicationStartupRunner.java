package devon.cicd;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import devon.cicd.ctx.CicdContext;
import devon.cicd.db.TableInitializer;
import devon.cicd.db.dao.CommonWrapper;
import devon.cicd.db.dao.ConfigWrapper;
import devon.cicd.db.dao.DBCheckerWrapper;
import devon.cicd.thread.TrayCleaner;
import devon.cicd.util.AES256Utils;

@Component
public class ApplicationStartupRunner implements ApplicationRunner {
    private static final Log logger = LogFactory.getLog(ApplicationStartupRunner.class);
 
    @Autowired
    DBCheckerWrapper dBCheckerWrapper;
    @Autowired
    CommonWrapper commonWrapper;
    @Autowired
    ConfigWrapper configWrapper;
    
    @Autowired
    private DataSource dataSource;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
    	logger.info("Application started with option names : {}" + args.getOptionNames());
    	checkDB();
    	initialValues();
    	CicdContext.instance().addDaemonThread(new TrayCleaner());
    }
    
    private void checkDB() throws Exception {
    	logger.info("check DB");
    	try {
    		dBCheckerWrapper.selectCheckTable();
    	}catch(Exception ex) {
    		logger.info("CheckerDB: " + ex.getLocalizedMessage());
    		String message = ex.getMessage();
    		if(message != null && message.toUpperCase().indexOf("TABLE") >= 0) {
    			initializeDB();
    		}else {
    			throw ex;
    		}
    	}
    }
	private void initialValues() {
    	logger.info("initialValues");
    	Map<String, Object> input = new HashMap<String, Object>();
    	input.put("division", "000");
    	input.put("code", null);
    	
		readKey(commonWrapper.selectCode(input));
    	input.put("division", "001");
		readCicdAdmin(commonWrapper.selectCode(input));
		readSourceRepository(configWrapper.selectGroupList("SR"));
	}
	
	private void readCicdAdmin(Map<String, Object> data) {
		String id;
		if(data == null || (id = (String)data.get("code")) == null) {
			throw new RuntimeException("CICD Admin ID do not exist!");
		}
		CicdContext.instance().setCicdAdmin(id);
	}
	
	private void readKey(Map<String, Object> data) {
		String key;
		if(data == null || (key = (String)data.get("name")) == null) {
			throw new RuntimeException("AES256 key do not exist!");
		}
		AES256Utils.setKey(key);		
	}

    private void initializeDB() throws Exception{
    	Connection conn = null;
    	try {
    		conn = dataSource.getConnection();
    		TableInitializer.createTables(conn);
    	}finally {
    		if(conn != null) {
    			try { conn.close(); }catch(Exception ex) {};
    		}
    	}
    }
    
    private void readSourceRepository(List<Map<String, Object>> list) {
    	if(list == null || list.size() == 0) {
    		return;
    	}

    	List<Map<String,Object>> sourceRepositorys = new ArrayList<Map<String,Object>>();
    	Map<String, Object> data;
    	for(Map<String, Object> map : list) {
    		data = new HashMap<String, Object>(2);
    		data.put("repositoryId", map.get("groupId"));
    		data.put("repositoryName", map.get("groupName"));
    		sourceRepositorys.add(data);
    	}
    	CicdContext.instance().setSourceRepositories(sourceRepositorys);
    }
}
