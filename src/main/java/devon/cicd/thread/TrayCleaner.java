package devon.cicd.thread;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import devon.cicd.config.Constants;
import devon.cicd.ctx.CicdContext;
import devon.cicd.resource.CachedResourceApi;

public class TrayCleaner extends Thread {
    private static final Log logger = LogFactory.getLog(TrayCleaner.class);
    
	public TrayCleaner() {
		this.setName("TrayCleaner");
		this.setDaemon(true);
	}
	
	public void run() {
		while(true) {
			try {
				Thread.sleep(60000L);
				cleanResourceApi();
			}catch(Exception ex) {
				logger.error("TrayCleanerException" + ex.getMessage(), ex);
			}
		}
	}
	
	private void cleanResourceApi() throws Exception{
		Map<String, Map<String, CachedResourceApi>> resourceApis = CicdContext.instance().getResourceApis();
		if(resourceApis == null || resourceApis.size() == 0) {
			return;
		}
		
		Object [] sessionIds = resourceApis.keySet().toArray();
		Map<String, CachedResourceApi> apis;
		CachedResourceApi api;
		Object [] keys;
		
		long currentTime = System.currentTimeMillis();
		long elapseTime;
		for(Object sessionId : sessionIds) {
			apis = resourceApis.get(sessionId);
			if(apis == null) {
				continue;
			}
			keys = apis.keySet().toArray();
			for(Object key: keys) {
				api = apis.get(key);
				if(api == null) {
					continue;
				}
				elapseTime = currentTime - api.getLastAccessTime();
				if(elapseTime > Constants.MAX_RESOURCE_API_KEEP_TIME) {
					apis.remove(key);
					logger.info("cleanResourceApi(API): " + key);
					if(apis.size() == 0) {
						resourceApis.remove(sessionId);
						logger.info("cleanResourceApi(Session): " + sessionId);
					}
				}
			}
		}
	}
}
