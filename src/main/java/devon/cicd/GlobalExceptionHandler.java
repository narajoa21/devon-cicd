package devon.cicd;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;  
import org.springframework.web.bind.annotation.ControllerAdvice;  
import org.springframework.web.bind.annotation.ExceptionHandler;  
import org.springframework.web.bind.annotation.ResponseStatus;  
import org.springframework.web.bind.annotation.RestController;  
  
@ControllerAdvice  
@RestController  
public class GlobalExceptionHandler {  
    private static final Log logger = LogFactory.getLog(GlobalExceptionHandler.class);
 
    @ResponseStatus(HttpStatus.BAD_REQUEST)  
    @ExceptionHandler(value = RuntimeException.class)  
    public String handleBaseException(RuntimeException e){
       	logger.error("GlobalExceptionHandler:" + e.getMessage(), e);
        return e.getMessage();
    }  
  
    @ResponseStatus(HttpStatus.BAD_REQUEST)  
    @ExceptionHandler(value = Exception.class)  
    public String handleException(Exception e){
    	logger.error("GlobalExceptionHandler:" + e.getMessage(), e);
    	return e.getMessage();
    } 
} 
