package devon.cicd.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devon.cicd.ctx.ThreadContext;
import devon.cicd.ctx.UserContext;
import devon.cicd.db.dao.ConfigWrapper;

@Service
public class ConfigManager {

	
	@Autowired
	ConfigWrapper configWrapper;
	
	public List<Map<String, Object>> getGroupDiv(){
		return configWrapper.selectGroupDiv();
	}
	public List<Map<String, Object>> getDevonGroupList(){
		return configWrapper.selectDevonGroupList();
	}
	
	public List<Map<String, Object>> getDevonGroupListByGroupDiv(Map<String,Object> groupDiv){
		return configWrapper.selectDevonGroupByGroupDiv(groupDiv);
		
	}
	
	public int addDevonGroup(Map<String,Object> devonGroup){
		return configWrapper.insertDevonGroup(devonGroup);
	}
	
	public int updateDevonGroup(Map<String,Object> devonGroup) {
		return configWrapper.updateDevonGroup(devonGroup);
	}
	
	public int deleteDevonGroup(Map<String,Object> devonGroup) {
		return configWrapper.deleteDevonGroup(devonGroup);
	}
	
	public List<Map<String, Object>> getDevonSolutionList(){
		return configWrapper.selectDevonSolution();
	}
	
	public List<Map<String, Object>> getStageList(Map<String,Object> project){
		List<Map<String,Object>> list = configWrapper.selectStageList(project.get("subSystemId").toString());
		System.out.println("subsystem id:" + project.get("subSystemId"));
		System.out.println("list size:" + list.size());
		return list;
	}
	
	public List<Map<String,Object>> getStageKeyList(){
		return configWrapper.selectStageKeyList();
	}
	
	public int updateStage(Map<String,Object> project) {
		return configWrapper.updateStage(project);
	}
	
	public int deleteStage(Map<String,Object> project) {
		return configWrapper.deleteStage(project);
	}
	
	public int addStage(Map<String,Object> project) {
		return configWrapper.insertStage(project);
	}
	
	public List<Map<String,Object>> getCicdList(Map<String,Object> project){
		return configWrapper.selectCicdList(project);
    }
	
	public List<Map<String,Object>> getSolutionKeyList(){
		return configWrapper.selectSolutionKeyList();
	}
	
	public List<Map<String,Object>> getCicdStepKeyList(){
		return configWrapper.selectCicdStepKeyList();
	}
	
	public List<Map<String,Object>> getAuthKeyList(){
		return configWrapper.selectAuthKeyList();
	}
	
	public int addCicd(Map<String,Object> list) {
		return configWrapper.insertCicd(list);
	}
	public int updateCicd(Map<String,Object> list) {
		return configWrapper.updateCicd(list);
	}
	
	public int deleteCicd(Map<String,Object> list) {
		return configWrapper.deleteCicd(list);

	}
	
	public UserContext getSystemInfo(){
		UserContext userContext = ThreadContext.getUserContext();
		return userContext;
	}
	
	public List<Map<String,Object>> getConfigStage(Map<String,Object> group){	
		return configWrapper.selectConfigStage(group);
	}
	
	public List<Map<String,Object>> getConfigList(Map<String,Object> config){
		List<Map<String,Object>> configs = configWrapper.selectConfigList(config);
		for(Map<String, Object> conf: configs) {
			conf.put("update", "U");
		}
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		String groupId = (String) config.get("groupId");
		String stageCode = (String) config.get("stageCode");
		String solutionCode = (String) config.get("solutionCode");
		if(configs.size() != 0) {
	    solutionCode = (String) configs.get(0).get("solutionCode");
	    map.put("solutionCode", solutionCode);
	    map.put("configs", configs);
	    System.out.println("exists");
	    list = configWrapper.selectAddConfigList(map);
		}else if(groupId != null && stageCode != null && solutionCode !=null ) {
        System.out.println("groupId : " + groupId);
        System.out.println("stageCode : "  + stageCode);
        System.out.println("solutioncode: " + solutionCode);
;		list = configWrapper.selectAddConfigListBysCode(solutionCode);
		}
		
		for(Map<String,Object> addConfig : list) {
			addConfig.put("update", "I");
			configs.add(addConfig);
		}
		
		return configs;
	}

	
	public List<Integer> updateConfig(List<Map<String,Object>> configs) {
		List<Integer> result = new ArrayList<Integer>();
		for(Map<String,Object> config : configs) {
		 System.out.println(config.get("update"));
		 if(config.get("update").equals("U")) {
		 result.add(configWrapper.updateConfig(config));
		 System.out.println("update");
		 }
		 else if(config.get("update").equals("I")) {
		 result.add(configWrapper.insertConfig(config));
		 System.out.println("insert");
		 }
		}
		return result;
		
	}
	
	public int deleteConfig(Map<String,Object> config) {
		return configWrapper.deleteConfig(config);
	}
	public List<Integer> addConfig(List<Map<String,Object>> configs){
		List<Integer> list = new ArrayList<Integer>();
		System.out.println("addCall");
		for(Map<String,Object> config: configs) {
			list.add(configWrapper.insertConfig(config));
		}
		return list;	
	}
	
	public List<Map<String,Object>> getRegionList(){
		return configWrapper.selectRegionList();
	}
	

}
