package devon.cicd.config;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/configuration")
public class ConfigController {

	@Autowired
	ConfigManager configService;
	
	@GetMapping(value = "groupDiv", produces = "application/json")
	public Object getGroupDiv() throws Exception{
		return configService.getGroupDiv();
	}
	
	@GetMapping(value = "devonGroup", produces = "application/json")
	public Object getDevonGroup() throws Exception{
		return configService.getDevonGroupList();
	}
	
	@PostMapping(value = "getDevonGroup", produces = "application/json")
	public Object getDevonGroupByGroupDiv(@RequestBody Map<String, Object> params) throws Exception{
		return configService.getDevonGroupListByGroupDiv(params);
	}
	
	@PostMapping(value = "addDevonGroup", produces = "application/json")
	public Object postDevonGroup(@RequestBody Map<String, Object> params) throws Exception{
		return configService.addDevonGroup(params);
	}
	
	@PostMapping(value = "updateDevonGroup", produces = "application/json")
	public Object updateDevonGroup(@RequestBody Map<String, Object> params) throws Exception{
		return configService.updateDevonGroup(params);
	}
	
	@PostMapping(value= "deleteDevonGroup", produces = "application/json")
	public Object deleteDevonGroup(@RequestBody Map<String, Object> params) throws Exception{
		return configService.deleteDevonGroup(params);
		
	}
	
	@GetMapping(value ="devonSolution", produces ="application/json")
	public Object getDevonSolution() throws Exception{
		return configService.getDevonSolutionList();
	}
	
	@PostMapping(value="stages", produces="application/json")
	public Object getStages(@RequestBody Map<String, Object> params) throws Exception{
		return configService.getStageList(params);
	}
	
	@GetMapping(value="stageKeyList", produces="application/json")
	public Object getStageKeyList() throws Exception{
		return configService.getStageKeyList();
	}
	
	@PostMapping(value="updateStage", produces="application/json")
	public Object postUpdateStage(@RequestBody Map<String,Object> params) throws Exception{
		return configService.updateStage(params);
	}
	
    @PostMapping(value="deleteStage", produces="application/json")
    public Object postDelete(@RequestBody Map<String,Object> params) throws Exception{
    	return configService.deleteStage(params);
    }
    
    @PostMapping(value="addStage", produces="application/json")
    public Object postStage(@RequestBody Map<String,Object> params) throws Exception{
    	return configService.addStage(params);
    }
    
    @PostMapping(value="cicds", produces="application/json")
    public Object getCicds(@RequestBody Map<String,Object> params) throws Exception{
    	System.out.println("call");
    	return configService.getCicdList(params);
    }
    
    @GetMapping(value = "solutionKey", produces="application/json")
    public Object getSolutionKeyList() throws Exception{
    	return configService.getSolutionKeyList();
    }
    
    @GetMapping(value ="cicdStepKey", produces="application/json")
    public Object getCicdStepKeyList() throws Exception{
    	return configService.getCicdStepKeyList();
    }
    
    @GetMapping(value ="authKey", produces ="application/json")
    public Object getAuthKeyList() throws Exception{
    	return configService.getAuthKeyList();
    }
    
    @PostMapping(value="addCicd", produces="application/json")
    public Object postCicd(@RequestBody Map<String,Object> params) throws Exception{
    	return configService.addCicd(params);
    }
    
    @PostMapping(value="updateCicd", produces="application/json")
    public Object postUpdateCicd(@RequestBody Map<String,Object> params) throws Exception{
    	return configService.updateCicd(params);	
    }
    
    @GetMapping(value="systemInfo", produces="application/json")
    public Object getSystemInfo() throws Exception{
    	return configService.getSystemInfo();
    }
    
    @PostMapping(value="deleteCicd", produces="application/json")
    public Object postDeleteCicd(@RequestBody Map<String,Object> params) throws Exception{
    	return configService.deleteCicd(params);
    }
    
    @PostMapping(value="configStage", produces="application/json")
    public Object postConfigStage(@RequestBody Map<String,Object> params) throws Exception{
    	return configService.getConfigStage(params);
    }
    
    @PostMapping(value="configList", produces="application/json")
    public Object postConfigList(@RequestBody Map<String,Object> params) throws Exception{
    	List<Map<String, Object>> list = configService.getConfigList(params);
    	for(Map<String, Object> map : list) {
    		System.out.println("iT:"+map.get("inputType"));
    	}
    	return list;
    }
    
    
    
    @PostMapping(value="updateConfig", produces="application/json")
    public Object postUpdateConfig(@RequestBody List<Map<String,Object>> params) throws Exception{
    	System.out.println("uccall");
    	return configService.updateConfig(params);
    }
    
    @PostMapping(value="deleteConfig", produces="application/json")
    public Object postDeleteConfig(@RequestBody Map<String,Object> params) throws Exception{
    	return configService.deleteConfig(params);
    }
    
    @PostMapping(value="addConfig", produces="application/json")
    public Object postAddConfig(@RequestBody List<Map<String,Object>> params) throws Exception{
    	return configService.addConfig(params);
    }
    
    @GetMapping(value="regionList", produces="application/json")
    public Object getRegionList() throws Exception{
    	return configService.getRegionList();
    }
    
	
	
}
