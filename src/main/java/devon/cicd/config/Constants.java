package devon.cicd.config;

public class Constants {
	final public static String	RESOURCE_GITLAB_CI		= "GLC";
	final public static String	RESOURCE_GITLAB_RUNNER	= "GLR";
	final public static String	RESOURCE_GITLAB_AUTH	= "GLA";
	final public static String	RESOURCE_AWS_AUTH		= "AWSA";
	final public static String	RESOURCE_AWS_ELB		= "AELB";
	final public static String	RESOURCE_AWS_ECS		= "AECS";
	final public static String	RESOURCE_AWS_EKS		= "AEKS";
	final public static String	RESOURCE_AWS_ECR		= "AECR";
	final public static String	RESOURCE_AWS_EC2		= "AEC2";
	final public static String	RESOURCE_AWS_STS		= "ASTS";
	final public static String	RESOURCE_AWS_FARGATE	= "AFGT";
	final public static String	RESOURCE_AWS_LOG		= "ALOG";
	final public static String	RESOURCE_AWS_CLOUDWATCH	= "ACWC";
	final public static String	RESOURCE_AWS_CODEDEPLOY	= "ACDP";
	
	final public static String	RESOURCE_DEPLOY_RESTART		= "RS";
	final public static String	RESOURCE_DEPLOY_ROLLING		= "RO";
	final public static String	RESOURCE_DEPLOY_BLUEGREEN	= "BG";
	final public static String	RESOURCE_DEPLOY_CANARY		= "CA";
	
	final public static String	RESOURCE_STATUS_RUNNING		= "Running";
	final public static String	RESOURCE_STATUS_COMPLETED	= "Completed";
	final public static String	RESOURCE_STATUS_FAIL		= "Fail";
	final public static String	RESOURCE_STATUS_ERROR		= "Error";
	final public static String	RESOURCE_STATUS_STOP		= "Stop";
	
	final public static String	AUTHORITY_ADMIN			= "ADMIN";
	final public static String	AUTHORITY_OWNER			= "OWNER";
	final public static String	AUTHORITY_MAINTAINER	= "MAINTAINER";
	final public static String	AUTHORITY_DEVELOPER		= "DEVELOPER";
	final public static String	AUTHORITY_REPORTER		= "REPORTER";
	final public static String	AUTHORITY_GUEST			= "GUEST";

	final public static	int		LIST_HUGE_SIZE			= 500;
	final public static int		LIST_LARGE_SIZE			= 100;
	final public static int		LIST_MEDIUM_SIZE		= 20;
	final public static int		LIST_SMALL_SIZE			= 10;
	final public static int		LIST_DETAIL_VIEW_SIZE 	= 10;
	
	final public static long	MAX_RESOURCE_API_KEEP_TIME	= 1800000L;
	final public static long	MAX_SOURCEUSER_KEEP_TIME	= 60000L;
	
	final public static int     LIST_SELECT_DAYS		= 7;
	final public static int     DASHBOARD_STATS_DAYS	= 7;
	final public static int		MONITOR_GAP_SECONDS		= 300;
	
	final public static String 	SOURCE_PERSONAL_TOKEN_NAME = "Devon-CICD-Personal-Token";
	final public static String 	USER_CONTEXT = "UserConext";
	final public static String 	CHARSET = "UTF-8";
}
