package devon.cicd.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devon.cicd.ctx.ThreadContext;
import devon.cicd.db.dao.ConfigWrapper;
import devon.cicd.util.DataConverter;

@Service
public class CicdConfigManager {
	@Autowired
	ConfigWrapper configWrapper;

	public Map<String, Object> readCicd(String cicdStep){
	    Map<String, Object> condition = new HashMap<String, Object>(3);
	    condition.put("systemId", ThreadContext.getUserContext().getSystemId());
	    condition.put("cicdStep", cicdStep);

		return configWrapper.selectCicd(condition);		
	}
	
	public CicdConfig readCicdConfig(String cicdStep, String stageCode) {
		CicdConfig config = readCicdConfigBySubSystemId(cicdStep, stageCode);
		if(config == null) {
			config = readCicdConfigBySubSystemId(cicdStep, stageCode);
		}
		return config;
	}
	
	public CicdConfig readCicdConfigBySystemId(String cicdStep, String stageCode) {
		return readCicdConfig(true, ThreadContext.getUserContext().getSystemId(), cicdStep, stageCode);
	}
	
	public CicdConfig readCicdConfigBySubSystemId(String cicdStep, String stageCode) {
		return readCicdConfig(false, ThreadContext.getUserContext().getSubSystemId(), cicdStep, stageCode);
	}
		
	public CicdConfig readCicdConfig(boolean isSystemId, String id, String cicdStep, String stageCode) {
	    Map<String, Object> condition = new HashMap<String, Object>(3);
	    if(isSystemId){
	    	condition.put("systemId", id);
	    }else {
	    	condition.put("subSystemId", id);	    	
	    }
	    condition.put("cicdStep", cicdStep);
	    condition.put("stageCode", stageCode);
	    
		List<Map<String, Object>> list;
		if(isSystemId){
			list = configWrapper.selectCicdConfigBySystemId(condition);
		}else {
			list = configWrapper.selectCicdConfigBySubSystemId(condition);			
		}
		
		if(list == null || list.size() == 0) {
			if(!"ALL".equals(stageCode)) {
				stageCode = "ALL";
			    condition.put("stageCode", stageCode);
				if(isSystemId){
					list = configWrapper.selectCicdConfigBySystemId(condition);
				}else {
					list = configWrapper.selectCicdConfigBySubSystemId(condition);			
				}
				if(list == null || list.size() == 0) {
					return null;
				}
			}else {
				return null;
			}
		}
		
		CicdConfig cicdConfig = new CicdConfig();
		cicdConfig.setSystemId((String)condition.get("systemId"));
		cicdConfig.setCicdStep(cicdStep);
		cicdConfig.setStageCode(stageCode);
		cicdConfig.setSolutionCode((String)list.get(0).get("solutionCode"));		
		cicdConfig.setConfigs(DataConverter.convertListToMap(list, "item", "value"));

		return cicdConfig;
	}
	
	public CicdConfig readAuthConfig(String solutionCode, String stageCode) {
	    Map<String, Object> condition = new HashMap<String, Object>(3);
	    condition.put("systemId", ThreadContext.getUserContext().getSystemId());
	    condition.put("solutionCode", solutionCode);
	    condition.put("stageCode", stageCode);
	    
		List<Map<String, Object>> list = configWrapper.selectAuthConfig(condition);
		if(list == null || list.size() == 0) {
			if(!"ALL".equals(stageCode)) {
				stageCode = "ALL";
			    condition.put("stageCode", stageCode);
				list = configWrapper.selectAuthConfig(condition);
				if(list == null || list.size() == 0) {
					return null;
				}
			}else {
				return null;
			}
		}

		CicdConfig authConfig = new CicdConfig();
		authConfig.setSystemId((String)condition.get("systemId"));
		authConfig.setStageCode(stageCode);
		authConfig.setSolutionCode((String)list.get(0).get("solutionCode"));
		authConfig.setAuthId((String)list.get(0).get("authId"));
		authConfig.setConfigs(DataConverter.convertListToMap(list, "item", "value"));

		return authConfig;
	}
	
	public CicdConfig readAuthConfigById(String authId, String stageCode) {	
	    Map<String, Object> condition = new HashMap<String, Object>(3);
	    condition.put("authId", authId);
	    condition.put("stageCode", stageCode);
	    
		List<Map<String, Object>> list = configWrapper.selectAuthConfigById(condition);
		if(list == null || list.size() == 0) {
			if(!"ALL".equals(stageCode)) {
				stageCode = "ALL";
			    condition.put("stageCode", stageCode);
			    list = configWrapper.selectAuthConfigById(condition);
			    if(list == null || list.size() == 0) {
			    	return null;
			    }			
			}else {
				return null;
			}
		}
		
		CicdConfig authConfig = new CicdConfig();
		authConfig.setSolutionCode((String)list.get(0).get("solutionCode"));
		authConfig.setStageCode(stageCode);
		authConfig.setAuthId((String)list.get(0).get("authId"));
		authConfig.setConfigs(DataConverter.convertListToMap(list, "item", "value"));		

		return authConfig;
	}	
	
	public SourceRepoConfig readRepoConfig(Map<String, Object> input) {
		List<Map<String, Object>> list = configWrapper.selectRepoConfig(input);
		if(list == null || list.size() == 0) {
			return null;
		}
		SourceRepoConfig repoConfig = new SourceRepoConfig();
		repoConfig.setRepositoryId((String)input.get("repositoryId"));
		repoConfig.setSolutionCode((String)list.get(0).get("solutionCode"));
		repoConfig.setConfigs(DataConverter.convertListToMap(list, "item", "value"));		
		return repoConfig;
	}
	
	public List<Map<String, Object>> readMergeApprovalUsers(Map<String, Object> input) {
		Map<String, Object> condition = new HashMap<String, Object>(2);
		condition.put("stageCode", input.get("stageCode"));
		condition.put("subSystemId", ThreadContext.getUserContext().getSubSystemId());
		
		Map<String, Object> map = configWrapper.selectApprovalUsers(input);
		if(map == null || map.size() == 0) {
			return null;
		}
		
		String users = (String)map.get("approvalUsers");
		if(users == null || users.trim().length() == 0) {
			return null;
		}
		
		String [] splitUsers = users.split(",");
		List<Map<String, Object>> reUsers = new ArrayList<Map<String, Object>>(splitUsers.length);
		String user;
		Map<String, Object> reUser;
		for(String splitUser : splitUsers) {
			user = splitUser.trim();
			if(user.length() == 0) {
				continue;
			}
			
			reUser = new HashMap<String, Object>();
			reUser.put("userName", user);
			reUsers.add(reUser);
		}
		return reUsers;
	}
}