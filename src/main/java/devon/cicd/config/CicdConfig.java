package devon.cicd.config;

import java.util.Map;

public class CicdConfig {
	private String systemId;
	private String cicdStep;
	private String stageCode;
	private String solutionCode;
	private String authId;
	private Map<String, Object> configs;
	
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public String getCicdStep() {
		return cicdStep;
	}
	public void setCicdStep(String cicdStep) {
		this.cicdStep = cicdStep;
	}
	public String getStageCode() {
		return stageCode;
	}
	public void setStageCode(String stageCode) {
		this.stageCode = stageCode;
	}
	public String getSolutionCode() {
		return solutionCode;
	}
	public void setSolutionCode(String solutionCode) {
		this.solutionCode = solutionCode;
	}
	public String getAuthId() {
		return authId;
	}
	public void setAuthId(String authId) {
		this.authId = authId;
	}
	public Map<String, Object> getConfigs() {
		return configs;
	}
	public void setConfigs(Map<String, Object> configs) {
		this.configs = configs;
	}

	public Object getConfig(String item) {
		if(item == null || this.configs == null) {
			return null;
		}
		return this.configs.get(item);
	}
}
