package devon.cicd.factory;

import java.util.Map;

import org.gitlab4j.api.GitLabApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import devon.cicd.config.CicdConfig;
import devon.cicd.config.CicdConfigManager;
import devon.cicd.config.Constants;
import devon.cicd.config.SourceRepoConfig;
import devon.cicd.ctx.CicdContext;
import devon.cicd.ctx.ThreadContext;
import devon.cicd.logon.HttpLogon;
import devon.cicd.logon.Logon;
import devon.cicd.resource.CloudRegions;
import devon.cicd.resource.DeploymentJob;
import devon.cicd.resource.ImageRepository;
import devon.cicd.resource.PipelineJob;
import devon.cicd.resource.Runtime;
import devon.cicd.resource.SourceRepository;
import devon.cicd.resource.aws.AwsImageRepository;
import devon.cicd.resource.aws.AwsRegions;
import devon.cicd.resource.aws.AwsDeploymentJob;
import devon.cicd.resource.aws.AwsEcsRuntime;
import devon.cicd.resource.aws.AwsEksRuntime;
import devon.cicd.resource.git.GitlabPipelineJob;
import devon.cicd.resource.git.GitlabSourceRepository;
import devon.cicd.resource.git.logon.GitLogon;
import devon.cicd.resource.git.logon.GitlabHttpLogon;
import devon.cicd.util.DataConverter;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient;
import software.amazon.awssdk.services.cloudwatchlogs.CloudWatchLogsClient;
import software.amazon.awssdk.services.codedeploy.CodeDeployClient;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ecr.EcrClient;
import software.amazon.awssdk.services.ecs.EcsClient;
import software.amazon.awssdk.services.eks.EksClient;
import software.amazon.awssdk.services.elasticloadbalancingv2.ElasticLoadBalancingV2Client;
import software.amazon.awssdk.services.sts.StsClient;

@Component
public class InterfaceFactory {
	@Autowired
	CicdConfigManager cicdConfigManager;
	
	public HttpLogon getHttpLogon(CicdConfig config) {
		switch(config.getSolutionCode()){
		case Constants.RESOURCE_GITLAB_AUTH:
			return new GitlabHttpLogon(config);
		}
		return null;
	}
	
	public Logon getLogon(String solutionCode) {
		switch(solutionCode){
		case Constants.RESOURCE_GITLAB_AUTH:
			return new GitLogon();
		}
		return null;
	}
	
	public CloudRegions getCloudRegions() {
		Map<String, Object> cicd = cicdConfigManager.readCicd("E");
		String solutionCode = (String)cicd.get("solutionCode");		
		switch(solutionCode){
		case Constants.RESOURCE_AWS_ECS:
			return new AwsRegions();
		}
		return null;
	}	

	public SourceRepository getSourceRepository() {
		String solutionCode = ThreadContext.getSourceRepoConfig().getSolutionCode();
		switch(solutionCode){
		case Constants.RESOURCE_GITLAB_AUTH:
			return new GitlabSourceRepository();
		}
		return null;
	}
	
	public ImageRepository getImageRepository(Map<String, Object> input) {
		String stageCode = (String)input.get("stageCode");
		Map<String, Object> cicd = cicdConfigManager.readCicd("R");
		String solutionCode = (String)cicd.get("solutionCode");
		if(solutionCode == null) {
			return null;
		}
		switch(solutionCode){
		case Constants.RESOURCE_AWS_ECR:
			return new AwsImageRepository(stageCode);
		}
		return null;
	}
		
	public Runtime getRuntime(Map<String, Object> input) {
		String stageCode = (String)input.get("stageCode");
		Map<String, Object> cicd = cicdConfigManager.readCicd("E");
		String solutionCode = (String)cicd.get("solutionCode");
		if(solutionCode == null) {
			return null;
		}
		switch(solutionCode){
		case Constants.RESOURCE_AWS_ECS:
			return new AwsEcsRuntime(stageCode, "E");
		case Constants.RESOURCE_AWS_EKS:
			return new AwsEksRuntime(stageCode, "E");		
		}
		return null;
	}	
	
	public PipelineJob getPipelineJob(String regionName) {
		Map<String, Object> cicd = cicdConfigManager.readCicd("P");
		String solutionCode = (String)cicd.get("solutionCode");
		if(solutionCode == null) {
			return null;
		}
		switch(solutionCode){
		case Constants.RESOURCE_GITLAB_RUNNER:
			return new GitlabPipelineJob(solutionCode, regionName);
		}
		return null;
	}
	
	public DeploymentJob getDeployment(Map<String, Object> input) {
		String stageCode = (String)input.get("stageCode");
		Map<String, Object> cicd = cicdConfigManager.readCicd("D");
		String solutionCode = (String)cicd.get("solutionCode");
		if(solutionCode == null) {
			return null;
		}
		switch(solutionCode){
		case Constants.RESOURCE_AWS_CODEDEPLOY:
			return new AwsDeploymentJob(stageCode);
		}
		return null;
	}		
	
	public Object getSourceRepoApi(SourceRepoConfig repoConfig) {
		Object api = null;
		String key = DataConverter.convertSameSolutionCode(repoConfig.getSolutionCode()) + "-" + repoConfig.getRepositoryId();
		api = CicdContext.instance().getResourceApi(key);
		if(api != null) {
			return api;
		}

		switch(repoConfig.getSolutionCode()) {
		case Constants.RESOURCE_GITLAB_AUTH:
			api = makeGitLabApi(repoConfig.getConfigs());
	        break;
		} 
	    if(api != null) {
	    	CicdContext.instance().addResourceApi(key, api);
	    }
		return api;
	}
	
	public Object getResourceApiByAuthId(Map<String, Object> input) {
		CicdConfig authConfig = cicdConfigManager.readAuthConfigById((String)input.get("authId"), "ALL");
		input.put("solutionCode", authConfig.getSolutionCode());
		input.put("apiSolutionCode", authConfig.getSolutionCode());
		
		return getReousrceApi(authConfig, input);
	}
	
	public Object getResourceApi(Map<String, Object> input) {
		CicdConfig authConfig = cicdConfigManager.readAuthConfig((String)input.get("solutionCode"), (String)input.get("stageCode"));
		if(input.get("apiSolutionCode") == null) {
			input.put("apiSolutionCode", input.get("solutionCode"));
		}
		return getReousrceApi(authConfig, input);						
	}
	
	private Object getReousrceApi(CicdConfig authConfig, Map<String, Object> input) {
		Map<String, Object> configs = authConfig.getConfigs();
		
		Object api = null;
		String apiSolutionCode = (String)input.get("apiSolutionCode");
		String key = DataConverter.convertSameSolutionCode(authConfig.getSolutionCode()) + "-" + apiSolutionCode +"-" + authConfig.getAuthId();
		api = CicdContext.instance().getResourceApi(key);
		if(api != null) {
			return api;
		}

		if(Constants.RESOURCE_AWS_AUTH.equals(authConfig.getSolutionCode())) {
			Region region = Region.of((String)input.get("region"));
			AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(AwsBasicCredentials.create((String)configs.get("accessKeyId"), (String)configs.get("secretAccessKey")));
	        switch(apiSolutionCode) {
	        case Constants.RESOURCE_AWS_ECS:
	        	api = EcsClient.builder().region(region).credentialsProvider(credentialsProvider).build();
	        	break;
	        case Constants.RESOURCE_AWS_EKS:
	        	api = EksClient.builder().region(region).credentialsProvider(credentialsProvider).build();
	        	break;
	        case Constants.RESOURCE_AWS_ECR:
	        	api = EcrClient.builder().region(region).credentialsProvider(credentialsProvider).build();
	        	break;
	        case Constants.RESOURCE_AWS_EC2:
	        	api = Ec2Client.builder().region(region).credentialsProvider(credentialsProvider).build();
	        	break;
	        case Constants.RESOURCE_AWS_STS:
	        	api = StsClient.builder().region(region).credentialsProvider(credentialsProvider).build();
	        	break;
	        case Constants.RESOURCE_AWS_ELB:
	        	api = ElasticLoadBalancingV2Client.builder().region(region).credentialsProvider(credentialsProvider).build();
	        	break;
	        case Constants.RESOURCE_AWS_CLOUDWATCH:
	        	api = CloudWatchClient.builder().region(region).credentialsProvider(credentialsProvider).build();
	        	break;
	        case Constants.RESOURCE_AWS_LOG:
	        	api = CloudWatchLogsClient.builder().region(region).credentialsProvider(credentialsProvider).build();
	        	break;
	        case Constants.RESOURCE_AWS_CODEDEPLOY:
	        	api = CodeDeployClient.builder().region(region).credentialsProvider(credentialsProvider).build();
	        	break;
	        }
		}else if(Constants.RESOURCE_GITLAB_AUTH.equals(authConfig.getSolutionCode())) {
			api = makeGitLabApi(configs);
		}
	    if(api != null) {
	    	CicdContext.instance().addResourceApi(key, api);
	    }
		return api;		
	}
	
	private GitLabApi makeGitLabApi(Map<String, Object> configs) {
		GitLabApi gitLabApi = new GitLabApi((String)configs.get("host"), (String)configs.get("token"));
        gitLabApi.setDefaultPerPage(Constants.LIST_HUGE_SIZE);
		return gitLabApi;
	}	
}
