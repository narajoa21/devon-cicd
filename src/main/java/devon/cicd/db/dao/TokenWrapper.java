package devon.cicd.db.dao;

import java.util.Map;

public interface TokenWrapper {
	public Map<String, Object> selectToken(String repositoryId, String userName);
	public int updateToken(Map<String, Object> input);
	public int insertToken(Map<String, Object> input);
}
