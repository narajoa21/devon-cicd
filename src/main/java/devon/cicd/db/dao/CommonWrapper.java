package devon.cicd.db.dao;

import java.util.List;
import java.util.Map;

public interface CommonWrapper {
	public List<Map<String, Object>> selectCodeLIst(String division);
	public Map<String, Object> selectCode(Map<String, Object> input);
}
