package devon.cicd.db.dao;

import java.util.List;
import java.util.Map;

public interface ConfigWrapper {
	public Map<String, Object> selectCicd(Map<String, Object> input);
	public List<Map<String, Object>> selectCicdConfigBySystemId(Map<String, Object> input);
	public List<Map<String, Object>> selectCicdConfigBySubSystemId(Map<String, Object> input);
	public List<Map<String, Object>> selectAuthConfig(Map<String, Object> input);
	public List<Map<String, Object>> selectAuthConfigById(Map<String, Object> input);
	public List<Map<String, Object>> selectRepoConfig(Map<String, Object> input);
	public List<Map<String, Object>> selectGroupList(String groupDiv);
	public Map<String, Object> selectSystemGroup(String sourceRepository);
	public List<Map<String, Object>> selectCicdList(Map<String, Object> input);
	public List<Map<String, Object>> selectStageList(String subSystemId);
	public Map<String, Object> selectApprovalUsers(Map<String, Object> input);
	
	
	public List<Map<String, Object>> selectDevonGroupList();
	public List<Map<String, Object>> selectDevonGroupByGroupDiv(Map<String, Object> input);
	public List<Map<String, Object>> selectGroupDiv();
	public int insertDevonGroup(Map<String, Object> input);
	public int updateDevonGroup(Map<String, Object> input);
	public int deleteDevonGroup(Map<String, Object> input);
	public Map<String,Object> selectDevonGroup(String groupId);
	public List<Map<String, Object>> selectDevonSolution();
	public List<Map<String, Object>> selectStageKeyList();
	public int updateStage(Map<String, Object> input);
	public int deleteStage(Map<String, Object> input);
	public int insertStage(Map<String, Object> input);
	public List<Map<String,Object>> selectSolutionKeyList();
	public List<Map<String,Object>> selectCicdStepKeyList();
	public List<Map<String,Object>> selectAuthKeyList();
	public int insertCicd(Map<String, Object> input);
	public int updateCicd(Map<String, Object> input);
	public int deleteCicd(Map<String, Object> input);
	public List<Map<String,Object>> selectConfigList();
	public List<Map<String,Object>> selectConfigStage(Map<String, Object> params);
	public List<Map<String,Object>> selectConfigList(Map<String, Object> params);
    public int insertConfig(Map<String, Object> config);
    public int updateConfig(Map<String, Object> configs);
    public int deleteConfig(Map<String, Object> configs);
    public List<Map<String,Object>> selectAddConfigList(Map<String, Object> params);
    public List<Map<String,Object>> selectAddConfigListBysCode(String solutionCode);
    public List<Map<String,Object>> selectRegionList();
 
 }
