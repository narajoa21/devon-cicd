package devon.cicd.db;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TableInitializer {
    static private final Log logger = LogFactory.getLog(TableInitializer.class);
    
	static public void createTables(Connection conn) throws Exception{
		logger.info("Initialize Tables");
		String line;
		StringBuilder buffer = null;
		boolean isDML = false;
		BufferedReader isr = null;
		int lineNum = 0;
		
		try {
			isr = new BufferedReader(new InputStreamReader(TableInitializer.class.getResourceAsStream("createTable.sql"), "UTF-8"));
			while((line = isr.readLine()) != null) {
				lineNum++;
				line = line.trim();
				if(line.length() == 0) {
					continue;
				}
				if(lineNum == 1) {
					if(line.indexOf("<DDL>") == 1) {
						line = line.substring(1);
					}
				}
				if(line.startsWith("---") && buffer != null){
					System.out.println("==>" + buffer.toString());
					processSQL(conn, buffer.toString(), isDML);
					buffer = null;
					continue;
				}

				if(buffer == null) {
					buffer = new StringBuilder(300);
					if(line.equalsIgnoreCase("<DDL>")) {
						isDML = false;
					}else if(line.equalsIgnoreCase("<DML>")) {
						isDML = true;
					}else {
						buffer.append(line);
					}
				}else {
					buffer.append("\r\n");
					buffer.append(line);
				}
			}
			if(buffer != null) {
				System.out.println("==>" + buffer.toString());
				processSQL(conn, buffer.toString(), isDML);
			}
		}finally {
			if(isr != null) {
				try { isr.close();}catch(Exception ex) {}
			}
			if(conn != null) {
				try { conn.commit();}catch(Exception ex) {}
			}			
		}
	}
	
	static private void processSQL(Connection conn, String sql, boolean isDML) throws Exception{
		if(sql == null || sql.length() == 0) {
			return;
		}
		
		if(isDML) {
			updateData(conn, sql, null);
		}else {
			executeDDL(conn, sql);
		}
	}	
	
	static private void setParams(PreparedStatement stmt, List<Object> params) throws Exception {
		if(params != null) {
			int index = 0;
			for(Object param : params) {
				index++;
				if(param instanceof String) {
					stmt.setString(index, (String)param);
				}else if(param instanceof Integer) {
					stmt.setInt(index, (Integer)param);
				}else if(param instanceof Long) {
					stmt.setLong(index, (Long)param);
				}else if(param instanceof Float) {
					stmt.setFloat(index, (Float)param);
				}else if(param instanceof Double) {
					stmt.setDouble(index, (Double)param);
				}else if(param instanceof Date) {
					stmt.setDate(index, (Date)param);
				}else if(param instanceof BigDecimal) {
					stmt.setBigDecimal(index, (BigDecimal)param);
				}else {
					throw new RuntimeException("Data type was not declared! - " + param );
				}
			}
		}
	}

	static private Map<String, Object> readData(ResultSet rs, ResultSetMetaData metaData) throws Exception {
		int count = metaData.getColumnCount();
		Map<String, Object> data = new HashMap<String, Object>();
		for(int i = 1; i <= count; i++) {
			data.put(metaData.getColumnName(i).toUpperCase(), rs.getObject(i));		
		}
		return data;
	}
	
	
	static public Map<String, Object> selectData(Connection conn, String query, List<Object> params) throws Exception{
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Map<String, Object> data = null;
		try {
			stmt = conn.prepareStatement(query);
			if(params != null && params.size() > 0) {
				setParams(stmt, params);
			}
			rs = stmt.executeQuery();
			
			if(rs.next()) {
				ResultSetMetaData metaData = rs.getMetaData();
				data = readData(rs, metaData);
			}
		}finally {
			if(rs != null) {
				try { rs.close(); }catch(Exception ex) {}
			}
			if(stmt != null) {
				try { stmt.close(); }catch(Exception ex) {}
			}
		}

		return data;
	}
	
	static public List<Map<String, Object>> selectDatas(Connection conn, String query, List<Object> params) throws Exception{
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Map<String, Object>> datas = null;
		try {
			stmt = conn.prepareStatement(query);
			if(params != null && params.size() > 0) {
				setParams(stmt, params);
			}
			rs = stmt.executeQuery();
			
			ResultSetMetaData metaData = null;
			Map<String, Object> data;
			while(rs.next()) {
				if(metaData == null) {
					metaData = rs.getMetaData();
				}
				data = readData(rs, metaData);
				if(datas == null) {
					datas = new ArrayList<Map<String, Object>>();
				}
				datas.add(data);
			}
		}finally {
			if(rs != null) {
				try { rs.close(); }catch(Exception ex) {}
			}
			if(stmt != null) {
				try { stmt.close(); }catch(Exception ex) {}
			}
		}

		return datas;
	}

	static public int updateData(Connection conn, String query, List<Object> params) throws Exception{
		PreparedStatement stmt = null;
		int updatedRows = 0;
		try {
			stmt = conn.prepareStatement(query);
			if(params != null && params.size() > 0) {
				setParams(stmt, params);
			}
			updatedRows = stmt.executeUpdate();
		}finally {
			if(stmt != null) {
				try { stmt.close(); }catch(Exception ex) {}
			}
		}
		return updatedRows;
	}

	static public int [] updateDatas(Connection conn, String query, List<List<Object>> params) throws Exception{
		PreparedStatement stmt = null;
		int [] updatedRows;
		try {
			stmt = conn.prepareStatement(query);
			for(List<Object> param : params) {
				setParams(stmt, param);
				stmt.addBatch();
			}
			updatedRows = stmt.executeBatch();
		}finally {
			if(stmt != null) {
				try { stmt.close(); }catch(Exception ex) {}
			}
		}
		return updatedRows;
	}
	
	static public int executeDDL(Connection conn, String ddlSql) throws Exception{
		Statement stmt = null;
		int updatedRows = 0;
		try {
			stmt = conn.createStatement();
			updatedRows = stmt.executeUpdate(ddlSql);
		}finally {
			if(stmt != null) {
				try { stmt.close(); }catch(Exception ex) {}
			}
		}
		return updatedRows;
	}	
}