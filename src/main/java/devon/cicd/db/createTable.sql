﻿<DDL>
CREATE TABLE IF NOT EXISTS devonCode (
	division		CHAR(3)			NOT NULL,
	code			VARCHAR(50)		NOT NULL,
	name			VARCHAR(100)	NOT NULL,
	displayOrder	INTEGER,
	useYn			CHAR(1)			NOT NULL DEFAULT 'Y',
	modifyLoginId	VARCHAR(40)		NOT NULL,
	modifyDt 		DATETIME		NOT NULL DEFAULT SYSDATE(),
	CONSTRAINT devonCode_pk PRIMARY KEY (division, code)
)
---
<DML>
INSERT INTO devonCode VALUES('000', 'key', '1,23,111,121,34,112,3,56,53,73,91,123,105,123,34,55', null, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('001', 'cicd_admin', 'B44XEK3WKSUBAKFS2HEMBIFY2A======', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('002', '000', 'CICD Process', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('002', 'S', 'Source', 1,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('002', 'B', 'Build', 2,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('002', 'D', 'Deploy', 3,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('002', 'T', 'Test', 4,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('002', 'P', 'Pipeline', 5,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('002', 'R', 'Image Repository', 6,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('002', 'E', 'Run Envirnment', 7,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('002', 'A', 'Auth', 8,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('003', '000', 'Stage Name', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('003', 'ALL', 'All', 1,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('003', 'DEV', 'Development', 2,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('003', 'TST', 'Test', 3,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('003', 'PRD', 'Production', 4,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('003', 'UPD', 'Upgrade', 5,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('004', '000', 'Deploy Strategy', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('004', 'RS', 'Restart', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('004', 'RO', 'Rolling', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('004', 'BG', 'Blue/Green', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('004', 'CA', 'Canari', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('005', '000', 'Group Div', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('005', 'SN', 'System Name', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('005', 'SB', 'Sub System', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('005', 'SR', 'Source Repository', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('005', 'AU', 'Auth Repository', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('006', '000', 'Build Trigger', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('006', 'C', 'Commit', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('006', 'M', 'Merge', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('006', 'D', 'Manual', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('100', '000', 'Input Types', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('100', 'S', 'String', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('100', 'N', 'Number', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('100', 'D', 'Date', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('100', 'DT', 'DateTime', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('100', 'T', 'Time', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('100', 'C', 'CheckBox', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('100', 'L', 'List', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', '000', 'AWS Region', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'us-east-1','US East (N. Virginia)', 1,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'us-east-2','US East (Ohio)', 2,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'us-west-1','US West (N. California)', 3,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'us-west-2','US West (Oregon)', 4,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'ca-central-1','Canada (Central)', 5,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'eu-central-1','EU (Frankfurt)', 6,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'eu-west-1','EU (Ireland)', 7,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'eu-west-2','EU (London)', 8,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'eu-west-3','EU (Paris)', 9,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'eu-north-1','EU (Stockholm)', 10,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'ap-east-1','Asia Pacific (Hong Kong)', 11,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'ap-northeast-1','Asia Pacific (Tokyo)', 12,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'ap-northeast-2','Asia Pacific (Seoul)', 13,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'ap-northeast-3','Asia Pacific (Osaka-Local)', 14,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'ap-southeast-1','Asia Pacific (Singapore)', 15,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'ap-southeast-2','Asia Pacific (Sydney)', 16,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'ap-south-1','Asia Pacific (Mumbai)', 17,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'sa-east-1','South America (Sao Paulo)', 18,'Y','cicd_admin', sysdate())
---
INSERT INTO devonCode VALUES('201', 'me-south-1','Middle East (Bahrain)', 19,'Y','cicd_admin', sysdate())
---
<DDL>
CREATE TABLE IF NOT EXISTS devonGroup (
	groupId				VARCHAR(5)	NOT NULL,	-- systemId(SN), susSystemId(SB), sourceRepositoryID(SR), authCode(AU) -- SR�� �������� AU�̱⵵ ��
	groupDiv			CHAR(2)		NOT NULL,	-- DEVON_CODE : 005
	groupName			VARCHAR(60)	NOT NULL,
	parentGroupId		VARCHAR(5),				-- if grouDiv = SB
	sourceRepository	VARCHAR(80),			-- if groupDiv = SB
	solutionCode		VARCHAR(5),				-- if groupDiv = (AU or SR) and devonSolution:supportLevel = A
	managementTeam		VARCHAR(60),
	managerName			VARCHAR(40),
	managerTelno		VARCHAR(20),
	managerEmail		VARCHAR(40),
	CONSTRAINT devonGroup_pk PRIMARY KEY (groupId)
)
---
<DML>
INSERT INTO devonGroup VALUES('R0001','SR', 'Example GitLab', null, null,'GLA', null, null, null, null)
---
INSERT INTO devonGroup VALUES('R0002','SR', 'Example SVN', null, null,'GLA', null, null, null, null)
---
INSERT INTO devonGroup VALUES('10000', 'SN', 'Example System',  null, null, null,'sm management','Hong gildong', '010-2411-1234', 'honggildong@example.com')
---
INSERT INTO devonGroup VALUES('10001', 'SB', 'Sub System1',  '10000', 'lgcns/cicd', null, 'sm management1','Hong gildong1', '010-2411-1234', 'honggildong1@example.com')
---
INSERT INTO devonGroup VALUES('10002', 'SB', 'Sub System2',  '10000', 'msa/tecConfigServer', null, 'sm management2','Hong gildong2', '010-2411-1235', 'honggildong2@example.com')
---
INSERT INTO devonGroup VALUES('A0001', 'AU', 'Example AWS',  null, null, 'AWSA', 'sm management2','Hong gildong2', '010-2411-1235', 'honggildong2@example.com')
---
<DDL>
CREATE TABLE IF NOT EXISTS devonCicd (
	systemId		VARCHAR(5) 	NOT NULL,	-- devonGroup : groupDiv - SN
	cicdStep		CHAR(1) 	NOT NULL,	-- devonCode : 002
	solutionCode	VARCHAR(5)	NOT NULL,	-- devonSolution
	authId			VARCHAR(5),				-- devonGroup : groupDiv - (AU, SR)
	CONSTRAINT devonCicd_pk PRIMARY KEY (systemId, cicdStep, solutionCode)
)
---
<DML>
INSERT INTO devonCicd VALUES('10000', 'S', 'GLC', 'R0001')
---
INSERT INTO devonCicd VALUES('10000', 'B', 'GLR', 'R0001')
---
INSERT INTO devonCicd VALUES('10000', 'D', 'ACDP', 'A0001')
---
INSERT INTO devonCicd VALUES('10000', 'P', 'GLR', 'R0001')
---
INSERT INTO devonCicd VALUES('10000', 'E', 'AECS', 'A0001')
---
INSERT INTO devonCicd VALUES('10000', 'R', 'AECR', 'A0001')
---
<DDL>
CREATE TABLE IF NOT EXISTS devonStage (
	subSystemId		VARCHAR(5) 	NOT NULL,		-- devonGroup : groupDiv - SB
	stageCode		CHAR(3)		NOT NULL, 		-- devonCode : 003
	region			VARCHAR(20),
	deploy			CHAR(2),					-- deovnCode : 004
	branchName		VARCHAR(40),
	preBranchName	VARCHAR(40),
	approvalUsers	VARCHAR(200),
	pipeline		VARCHAR(100),
	buildTrigger	CHAR(1),					-- deovnCode : 006
	useYn			CHAR(1)		NOT NULL DEFAULT 'Y',
	modifyLoginId	VARCHAR(40)	NOT NULL,
	modifyDt 		DATETIME 	NOT NULL DEFAULT SYSDATE(),
	CONSTRAINT devonStage_pk PRIMARY KEY (subSystemId, stageCode)
)
---
<DML>
INSERT INTO devonStage VALUES('10001', 'DEV', 'ap-northeast-2', 'RS', 'dev', null, null, '.gitlab-ci.yml', null, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonStage VALUES('10001', 'PRD', 'ap-northeast-2', 'BG', 'master', 'dev', 'mskwon,root', '.gitlab-ci.yml', null, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonStage VALUES('10002', 'ALL', 'ap-northeast-2', null, 'master', null, 'mskwon', '.gitlab-ci.yml', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonStage VALUES('10002', 'DEV', 'ap-northeast-2', 'RS', null, null, null, '.gitlab-ci.yml', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonStage VALUES('10002', 'TST', 'ap-northeast-2', 'RO', null, null, null, '.gitlab-ci.yml', null,'Y','cicd_admin', sysdate())
---
INSERT INTO devonStage VALUES('10002', 'PRD', 'ap-northeast-2', 'CA', null, null, null, '.gitlab-ci.yml', null,'Y','cicd_admin', sysdate())
---
<DDL>
CREATE TABLE IF NOT EXISTS devonSolution (
	solutionCode	VARCHAR(5)		NOT NULL,
	solutionName	VARCHAR(100)	NOT NULL,
	supportLevel	VARCHAR(4)		NOT NULL,	-- devonCode: 002  (Multi)
	CONSTRAINT devonSolution_pk PRIMARY KEY (solutionCode)
)
---
<DML>
INSERT INTO devonSolution VALUES('GLC','GitLab CI', 'S');
---
INSERT INTO devonSolution VALUES('GLR','GitLab Runner', 'BD');
---
INSERT INTO devonSolution VALUES('AECS','AWS ECS', 'E');
---
INSERT INTO devonSolution VALUES('AECR','AWS Image Registry', 'R');
---
INSERT INTO devonSolution VALUES('AEKS','AWS EKS', 'E');
---
INSERT INTO devonSolution VALUES('GLA','GitLab Auth', 'A');
---
INSERT INTO devonSolution VALUES('AWSA','AWS Auth', 'A');
---
INSERT INTO devonSolution VALUES('ACDP','AWS CodeDeploy', 'D');
---
<DDL>
CREATE TABLE IF NOT EXISTS devonConfig (
	groupId			VARCHAR(5) 		NOT NULL,	-- devonGroup: groupId (SB : subSystemId, SR: repositoryId, AU, authId) - SB: selected subSytemId, AU: authId- devonCicd, SR: selected repositoryId
	stageCode		CHAR(3)			NOT NULL,	-- devonCode : 003 (SR: ALL, AU: ALL) - list target : DevonStage
	solutionCode	VARCHAR(5) 		NOT NULL,	-- devonConfItem  - List: DevonCicd
	item			VARCHAR(100)	NOT NULL,	-- devonConfItem
	value			VARCHAR(200),
	useYn			CHAR(1) 		NOT NULL DEFAULT 'Y',
	modifyLoginId	VARCHAR(40)		NOT NULL,
	modifyDt 		DATETIME 		NOT NULL DEFAULT SYSDATE(),
	CONSTRAINT devonConfig_pk PRIMARY KEY (groupId, stageCode, solutionCode, item)
)
---
<DML>
INSERT INTO devonConfig VALUES('R0001', 'ALL', 'GLA', 'host', 'http://ec2-15-164-169-168.ap-northeast-2.compute.amazonaws.com','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('R0001', 'ALL', 'GLA', 'token', 'm4d_L5_2ADWhFaS8ESEr','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('R0001', 'ALL', 'GLA', 'apiKey', 'fcb663b6bb338cdfcf39bff561f1704c4328dd74826ee8cdb9db1cefa3f81bc9','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('R0001', 'ALL', 'GLA', 'apiSecret', '56432b5cbe147fbbf7b99f958ccfcfafdf9549b75679e781656a1c707a7dbbaa','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('R0001', 'ALL', 'GLA', 'jwtKey', 'jwt-key-length-must-be-at-least-256-bits','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('R0001', 'ALL', 'GLA', 'jwtIssuer', 'cicd-manager','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'ALL', 'GLR', 'triggerName', 'cicdTrigger','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'ALL', 'GLR', 'triggerToken', '29ecc533f1bb1b67412c06108ed471','Y','cicd_admin', sysdate()) 
---
INSERT INTO devonConfig VALUES('10001', 'ALL', 'GLR', 'runnerName', 'cicd docker runner:shared-socket','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'ALL', 'GLR', 'useShortCommitId', 'true','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('A0001', 'ALL', 'AWSA', 'host', 'https://console.aws.amazon.com','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('A0001', 'ALL', 'AWSA', 'region', 'ap-northeast-2','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('A0001', 'ALL', 'AWSA', 'accessKeyId', 'AKIAI5MPVPQUR2242WFQ','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('A0001', 'ALL', 'AWSA', 'secretAccessKey', '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','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'ALL', 'AECR', 'region', 'ap-northeast-2','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'ALL', 'AECR', 'imageRepository', 'lgcns/app','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'DEV', 'AECS', 'region', 'ap-northeast-2','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'DEV', 'AECS', 'clusterName', 'LgCnsDevCluster','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'DEV', 'AECS', 'serviceName', 'dev-app-svc','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'DEV', 'AECS', 'loadBalancerName', 'LgCnsAlb','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'DEV', 'AECS', 'loadBalancerServicePort', '80','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'PRD', 'AECS', 'region', 'ap-northeast-2','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'PRD', 'AECS', 'clusterName', 'LgCnsProdCluster','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'PRD', 'AECS', 'serviceName', 'prod-lgcns-app-svc','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'PRD', 'AECS', 'loadBalancerName', 'LgcnsProdAlb','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'PRD', 'AECS', 'loadBalancerServicePort', '80','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'PRD', 'AECS', 'loadBalancerTestPort', '8080','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'PRD', 'ACDP', 'application', 'AppECS-LgCnsProdCluster-prod-lgcns-app-svc','Y','cicd_admin', sysdate())
---
INSERT INTO devonConfig VALUES('10001', 'PRD', 'ACDP', 'deploymentGroup', 'DgpECS-LgCnsProdCluster-prod-lgcns-app-svc','Y','cicd_admin', sysdate())
---
<DDL>
CREATE TABLE IF NOT EXISTS devonConfItem (
	solutionCode	VARCHAR(5)		NOT NULL, -- devonSolution: solutionCode
	item			VARCHAR(100)	NOT NULL,
	description		VARCHAR(1000),
	displayOrder	INTEGER,
	inputType		VARCHAR(2),		-- devonCode: 100
	codeValue		VARCHAR(3),		-- devonCode: division
	useYn			CHAR(1)			NOT NULL DEFAULT 'Y',
	modifyLoginId	VARCHAR(40)		NOT NULL,
	modifyDt 		DATETIME		NOT NULL DEFAULT SYSDATE(),
	CONSTRAINT devonConfItem_pk PRIMARY KEY (solutionCode, item)
)
---
<DML>
INSERT INTO devonConfItem VALUES('GLA', 'host', 'Gitlab - Host', 1, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('GLA', 'token', 'Gitlab - [User dropdown menu] -> Settings -> Access Tokens', 2, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('GLA', 'apiKey', 'Gitlab - [User dropdown menu] -> Settings -> Applications\r\n[Redirect URI]\r\n  https://{your.gitlab.domain}/import/gitlab/callback\r\n  https://{your.gitlab.domain}/users/auth/gitlab/callback\r\n[scope]\r\n  check all', 3, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('GLA', 'apiSecret', 'Gitlab - [User dropdown menu] -> Settings -> Applications\r\n[Redirect URI]\r\n  https://{your.gitlab.domain}/import/gitlab/callback\r\n  https://{your.gitlab.domain}/users/auth/gitlab/callback\r\n[scope]\r\n  check all', 4, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('GLA', 'jwtKey', 'Gitlab - JwtKey', 5, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('GLA', 'jwtIssuer', 'Gitlab - JwtIssuer', 6, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('GLR', 'triggerName', 'Gitlab - [Project navigation menu] -> Settings -> CICD -> Pipeline Trigger', 7, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('GLR', 'triggerToken', 'Gitlab - [Project navigation menu] -> Settings -> CICD -> Pipeline Trigger', 7, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('GLR', 'runnerName', 'Gitlab Runner - Runner name', 8, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('GLR', 'useShortCommitId', 'Gitlab Runner - Use short commit id in image tag', 8, 'C', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AWSA', 'host', 'AWS - Host(ex: https://console.aws.amazon.com)', 1, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AWSA', 'region', 'AWS - Region id', 2, 'L', '201', 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AWSA', 'accessKeyId', 'AWS - [User dropdown menu] -> My Security Credentials -> Access Key(Access key ID and secret access key)', 3, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AWSA', 'secretAccessKey', 'AWS - [User dropdown menu] -> My Security Credentials -> Access Key(Access key ID and secret access key)', 4, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AECR', 'region', 'AWS ECR - Region', 1, 'L', '201', 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AECR', 'imageRepository', 'AWS ECR - Repository name', 2, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AECR', 'tagName', 'AWS ECR - Name of tag that separates stage steps', 3, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AECS', 'region', 'AWS ECS - Region', 1, 'L', '201', 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AECS', 'clusterName', 'AWS ECS - Cluster name', 2, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AECS', 'serviceName', 'AWS ECS - Service name', 3, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AECS', 'loadBalancerName', 'AWS ELB, ALP, NLB - Load-balancer Name', 4, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AECS', 'loadBalancerServicePort', 'AWS ELB, ALP, NLB - Load-balancer service Port', 5, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('AECS', 'loadBalancerTestPort', 'AWS ELB, ALP, NLB - Load-balancer Test Port', 6, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('ACDP', 'application', 'AWS CodeDeploy - Application', 1, 'S', NULL, 'Y','cicd_admin', sysdate())
---
INSERT INTO devonConfItem VALUES('ACDP', 'deploymentGroup', 'AWS CodeDeploy - deploymentGroup', 2, 'S', NULL, 'Y','cicd_admin', sysdate())
---
<DDL>
CREATE TABLE IF NOT EXISTS devonPersonToken (
	repositoryId	VARCHAR(5)		NOT NULL, -- devonGroup: groupId
	userName		VARCHAR(40)	NOT NULL,
	tokenId			VARCHAR(100)		NOT NULL,
	token			VARCHAR(300)	NOT NULL,
	modifyDt 		DATETIME 		NOT NULL DEFAULT SYSDATE(),
	CONSTRAINT devonPersonToken_pk PRIMARY KEY (repositoryId, userName)
)